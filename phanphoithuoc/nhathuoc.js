var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Công ty TNHH Dược Phẩm Tây Nam",
   "address": "Số 529A Hà Hoàng Hổ, phường Mỹ Xuyên, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3755669,
   "Latitude": 105.4175208
 },
 {
   "STT": 2,
   "Name": "Công ty TNHH Dược Phẩm Khả Thy",
   "address": "Số 215, Quốc lộ 91, ấp Hòa Phú 3, thị trấn An Châu, huyện ChâuThành, AN GIANG",
   "Longtitude": 10.4404055,
   "Latitude": 105.3921284
 },
 {
   "STT": 3,
   "Name": "Công ty TNHH Dược Phẩm PhúHải",
   "address": "Số 38 Phan Chu Trinh, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3833812,
   "Latitude": 105.4414515
 },
 {
   "STT": 4,
   "Name": "Công Ty Cổ Phần Dược Phẩm Agimexphar M",
   "address": "Số 27 Nguyễn Thái Học, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3877808,
   "Latitude": 105.4364323
 },
 {
   "STT": 5,
   "Name": "Công ty TNHH Dược Phẩm Liên Thành",
   "address": "Số 1/4 Lý Thường Kiệt, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3906208,
   "Latitude": 105.4364843
 },
 {
   "STT": 6,
   "Name": "Công ty TNHH Dược Phẩm Trúc Chi",
   "address": "Số 17 Lý Thái Tổ, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3775535,
   "Latitude": 105.4418518
 },
 {
   "STT": 7,
   "Name": "Công ty TNHH Dược Phẩm Minh Hạnh",
   "address": "Số 2-3C5 Nguyễn Tri Phương, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3907088,
   "Latitude": 105.4240215
 },
 {
   "STT": 8,
   "Name": "Chi nhánh Công ty CP Dược phẩm Cửu Longtại tỉnh An",
   "address": "Lô 3G1 Lý Thái Tổ, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3778032,
   "Latitude": 105.4420386
 },
 {
   "STT": 9,
   "Name": "Chi nhánh Công ty CP Dược Hậu Giang tại Tp.Long Xuyên",
   "address": "Số 288 Phạm Cự Lượng, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3652405,
   "Latitude": 105.4459285
 },
 {
   "STT": 10,
   "Name": "Công Ty TNHH Một Thành Viên Dược Phẩm Huy Thảo",
   "address": "Số 81 Lê Thị Nhiên, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.384029,
   "Latitude": 105.442482
 },
 {
   "STT": 11,
   "Name": "Công ty CP Pymepharco - chi nhánh AN GIANG",
   "address": "Số 21-23-25 Mai Hắc Đế - Số 26 Thục Phán, phường Bình Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3986126,
   "Latitude": 105.416332
 },
 {
   "STT": 12,
   "Name": "Công ty TNHH DP Long Xuyên",
   "address": "Số 253G Nguyễn Trường Tộ, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3924633,
   "Latitude": 105.4141354
 },
 {
   "STT": 13,
   "Name": "Chi nhánh Công ty CP DP TV.Pharm tạiAN GIANG",
   "address": "Số 11B Bùi Thị Xuân, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3793929,
   "Latitude": 105.4375185
 },
 {
   "STT": 14,
   "Name": "Công ty TNHH DP Hữu Thành",
   "address": "Số 86 Nguyễn Văn Cừ, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5857046,
   "Latitude": 105.3554266
 },
 {
   "STT": 15,
   "Name": "Công Ty TNHH Dược Phẩm Minh Quang",
   "address": "Số 169C/9, khóm Bình Thới 1, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.4001112,
   "Latitude": 105.4269719
 },
 {
   "STT": 16,
   "Name": "Công Ty TNHH MTV Dược Phẩm Phương Huy",
   "address": "Số 568 Nguyễn Văn Thoại, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7045946,
   "Latitude": 105.1118
 },
 {
   "STT": 17,
   "Name": "Chi Nhánh Công Ty Cổ Phần Dược Phẩm Imexpharm - AN GIANG",
   "address": "Số 16-18 Hùng Vương, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3794455,
   "Latitude": 105.4416736
 },
 {
   "STT": 18,
   "Name": "Công Ty TNHH Dược Phẩm Tiền Hồ",
   "address": "Số 19-20 E2 Sư Vạn Hạnh, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3990373,
   "Latitude": 105.4146765
 },
 {
   "STT": 19,
   "Name": "Công Ty TNHH Dược Phẩm & Dụng Cụ Y Tế Thái Bình",
   "address": "Số 209/2C Lương Thế Vinh, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3832333,
   "Latitude": 105.4397734
 },
 {
   "STT": 20,
   "Name": "Công Ty TNHH Dược Phẩm Phước Hưng",
   "address": "Số 42-44 Chu Văn An, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3821975,
   "Latitude": 105.4453215
 },
 {
   "STT": 21,
   "Name": "Nhà thuốc Số 130",
   "address": "Số 130 Trần Hưng Đạo, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3384194,
   "Latitude": 105.4737011
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc Lê Minh",
   "address": "Số 34, 36 Hai Bà Trưng, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3825697,
   "Latitude": 105.4415739
 },
 {
   "STT": 23,
   "Name": "Nhà thuốc Thu Hằng",
   "address": "Số 42C2 Thành Thái, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3916262,
   "Latitude": 105.4225195
 },
 {
   "STT": 24,
   "Name": "Nhà thuốc Út Cấn",
   "address": "Số 368, đường Ven Bãi, tổ 1, khóm Mỹ Chánh, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6869563,
   "Latitude": 105.1398205
 },
 {
   "STT": 25,
   "Name": "Nhà thuốc Hải Linh",
   "address": "Số 267 Nguyễn Tri Phương, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6973105,
   "Latitude": 105.1254259
 },
 {
   "STT": 26,
   "Name": "Nhà thuốcBệnh viện Đa Khoa Khu Vực Tỉnh AN GIANG",
   "address": "Số 917 Tôn Đức Thắng, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.690769,
   "Latitude": 105.1448433
 },
 {
   "STT": 27,
   "Name": "Nhà thuốc Kiến Quốc",
   "address": "Số 1500 Trần Hưng Đạo, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3740904,
   "Latitude": 105.4425703
 },
 {
   "STT": 28,
   "Name": "Nhà thuốc Hoàng Đạo",
   "address": "Số 812A Hà Hoàng Hổ, phường Đông Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3776904,
   "Latitude": 105.4314038
 },
 {
   "STT": 29,
   "Name": "Nhà thuốc Agimexphar M 8",
   "address": "Lô B11 đường số 4, Chợ Cái Sao, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3466449,
   "Latitude": 105.4685482
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Bảo Ngọc",
   "address": "Số 942 Hà Hoàng Hổ, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3753038,
   "Latitude": 105.4167899
 },
 {
   "STT": 31,
   "Name": "Nhà thuốc Minh Châu",
   "address": "Kios A2, Chợ Mỹ Quý, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3647792,
   "Latitude": 105.4481828
 },
 {
   "STT": 32,
   "Name": "Nhà thuốc Kim Hoàng",
   "address": "Số 485B Võ Thị Sáu, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3757411,
   "Latitude": 105.4361369
 },
 {
   "STT": 33,
   "Name": "Nhà thuốc Uy Phát",
   "address": "Số 488 Võ Thị Sáu, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3757297,
   "Latitude": 105.4362064
 },
 {
   "STT": 34,
   "Name": "Nhà thuốc Mai Lý",
   "address": "Số 1698 Trần Hưng Đạo, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3695949,
   "Latitude": 105.4462213
 },
 {
   "STT": 35,
   "Name": "Nhà thuốc Gia Mỹ",
   "address": "Số 98 Trần Phú, tổ 161, khóm Đông Thịnh 9, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.375602,
   "Latitude": 105.44761
 },
 {
   "STT": 36,
   "Name": "Nhà thuốc Luân Mai",
   "address": "Số 8H2 Hồ Quý Ly, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3647898,
   "Latitude": 105.4475283
 },
 {
   "STT": 37,
   "Name": "Nhà thuốc Đăng Tùng",
   "address": "Số 22/15 Trần Hưng Đạo, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3524598,
   "Latitude": 105.460362
 },
 {
   "STT": 38,
   "Name": "Nhà thuốc Phúc Khôi",
   "address": "Số 7/7 Lê Triệu Kiết, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.38682,
   "Latitude": 105.4378778
 },
 {
   "STT": 39,
   "Name": "Nhà thuốc Phương Liên",
   "address": "Số 59/2A, khóm Hưng Thạnh, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3247736,
   "Latitude": 105.471249
 },
 {
   "STT": 40,
   "Name": "Nhà thuốc Thùy Trang",
   "address": "Số 54/3A Trần Hưng Đạo, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3504723,
   "Latitude": 105.4615016
 },
 {
   "STT": 41,
   "Name": "Nhà thuốc Số 40",
   "address": "Số J19 Hùng Vương, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3763055,
   "Latitude": 105.4440797
 },
 {
   "STT": 42,
   "Name": "Nhà thuốc Lâm Phan",
   "address": "Số 11A Trần Nhật Duật, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3779944,
   "Latitude": 105.4411604
 },
 {
   "STT": 43,
   "Name": "Nhà thuốc Thụy Loan",
   "address": "Số 02 Trần Bình Trọng, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.377358,
   "Latitude": 105.4360526
 },
 {
   "STT": 44,
   "Name": "Nhà thuốc Sáu Vân",
   "address": "Kios số 01, ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 45,
   "Name": "Nhà thuốc Trí Hồng 1",
   "address": "Số 498, tổ 21, khóm Vĩnh Phú, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6820814,
   "Latitude": 105.0823967
 },
 {
   "STT": 46,
   "Name": "Nhà thuốc Đạt Sanh",
   "address": "Số 9 Hoàng Diệu, phường Châu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7018414,
   "Latitude": 105.1098636
 },
 {
   "STT": 47,
   "Name": "Nhà thuốc Hoàng Anh",
   "address": "Số 309 Trần Hưng Đạo, khóm 5, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.713841,
   "Latitude": 105.1185566
 },
 {
   "STT": 48,
   "Name": "Nhà thuốc Huỳnh Chí Thành",
   "address": "Tổ 8, ấp Vĩnh Khánh 2, xã Vĩnh Tế, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.668943,
   "Latitude": 105.0623171
 },
 {
   "STT": 49,
   "Name": "Nhà thuốc Thăng Long",
   "address": "Số 112 Châu Thị Tế, khóm Vĩnh Tây 1, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6831912,
   "Latitude": 105.0817394
 },
 {
   "STT": 50,
   "Name": "Nhà thuốc Thái Bình",
   "address": "Số 11 Huỳnh Thị Hưởng, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3837514,
   "Latitude": 105.4425162
 },
 {
   "STT": 51,
   "Name": "Nhà thuốc Mỹ Ý",
   "address": "Lô 01 kios Chợ Mỹ Hòa, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3762262,
   "Latitude": 105.4137532
 },
 {
   "STT": 52,
   "Name": "Nhà thuốc Giáo Hinh",
   "address": "Số 536, đường Vòng Núi Sam, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6812134,
   "Latitude": 105.0809523
 },
 {
   "STT": 53,
   "Name": "Nhà thuốc Cẩm Lìl",
   "address": "Số 9 Nguyễn Tri Phương, khóm Long Thạnh D, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.800328,
   "Latitude": 105.2422939
 },
 {
   "STT": 54,
   "Name": "Nhà thuốc Phước Nguyên",
   "address": "Số 44/4 Trần HƯng Đạo, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3732175,
   "Latitude": 105.4431774
 },
 {
   "STT": 55,
   "Name": "Nhà thuốc Gia Uyên",
   "address": "Số 7/9 Châu Văn Liêm, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3865182,
   "Latitude": 105.4367086
 },
 {
   "STT": 56,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "Số 4/4 Lê Quý Đôn, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3906929,
   "Latitude": 105.4353297
 },
 {
   "STT": 57,
   "Name": "Nhà thuốc Bích Châu",
   "address": "Số 36 Phạm Hồng Thái, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.383443,
   "Latitude": 105.4393476
 },
 {
   "STT": 58,
   "Name": "Nhà thuốc Trưng Vương",
   "address": "Số 17A Nguyễn Trãi, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3816818,
   "Latitude": 105.4435861
 },
 {
   "STT": 59,
   "Name": "Nhà thuốc Thảo Nguyên",
   "address": "Số 16/1B Chưởng Binh Lễ, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3324586,
   "Latitude": 105.4838578
 },
 {
   "STT": 60,
   "Name": "Nhà thuốc Lý Huệ",
   "address": "Số 26 Nguyễn Văn  Linh, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7989498,
   "Latitude": 105.2484829
 },
 {
   "STT": 61,
   "Name": "Nhà thuốc Phúc Thiện",
   "address": "Số 35 Trần Phú, khóm Long Thạnh C, phường Long Hưng, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7972915,
   "Latitude": 105.2364278
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Thế Hùng",
   "address": "Số 59A Trần Phú, khóm Long Thạnh C, phường Long Hưng, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7958942,
   "Latitude": 105.237668
 },
 {
   "STT": 63,
   "Name": "Nhà thuốc Số 301",
   "address": "Số 177 Nguyễn TriPhương, khóm Long Thạnh D, phường Long Thạnh, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.7708956,
   "Latitude": 105.2838511
 },
 {
   "STT": 64,
   "Name": "Nhà thuốc Triều Nguyên",
   "address": "Số 46 Nguyễn Văn  Linh, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7984707,
   "Latitude": 105.2483075
 },
 {
   "STT": 65,
   "Name": "Nhà thuốc Song Anh",
   "address": "Số 544, tổ 1, đường Nguyễn Tri Phương, khóm Long Thạnh B, phường Long Thạnh,Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7982508,
   "Latitude": 105.311655
 },
 {
   "STT": 66,
   "Name": "Nhà thuốc Tịnh Đức",
   "address": "Số 150 Lê Lợi, khóm Châu Long 7, phường Châu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7046782,
   "Latitude": 105.1304708
 },
 {
   "STT": 67,
   "Name": "Nhà thuốc Lan Phương",
   "address": "Số 1018-1020 Tôn Đức Thắng, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.690769,
   "Latitude": 105.1448433
 },
 {
   "STT": 68,
   "Name": "Nhà thuốc Vân An",
   "address": "Kios số 1, 2, 3, 4, Lô I, Chợ Vĩnh Đông, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6823267,
   "Latitude": 105.073886
 },
 {
   "STT": 69,
   "Name": "Nhà thuốc Dung",
   "address": "Số 172 Châu Thị Tế, khóm Vĩnh Tây 1, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6831937,
   "Latitude": 105.0806978
 },
 {
   "STT": 70,
   "Name": "Nhà thuốc Lan Anh",
   "address": "Số 467 Thủ Khoa Huân, phường Châu Phú B,  thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6999303,
   "Latitude": 105.1253314
 },
 {
   "STT": 72,
   "Name": "Nhà thuốc Thanh Trúc",
   "address": "Số 7/5 Nguyễn Thái Học, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3871655,
   "Latitude": 105.4355647
 },
 {
   "STT": 73,
   "Name": "Nhà thuốc Thái Bình 3",
   "address": "Lô 1 I 10 Võ Văn Tần, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3751824,
   "Latitude": 105.4346457
 },
 {
   "STT": 74,
   "Name": "Nhà thuốc Phước Lợi",
   "address": "Số 9/2 Phan Liêm, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.378937,
   "Latitude": 105.438669
 },
 {
   "STT": 75,
   "Name": "Nhà thuốc Số 0152",
   "address": "Số 50/17 Tăng Bạt Hổ, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.33675,
   "Latitude": 105.474078
 },
 {
   "STT": 76,
   "Name": "Nhà thuốc Quốc Thảo",
   "address": "Số 339/40, khóm Bình Đức 4, phường Bình Đức, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.4031695,
   "Latitude": 105.419113
 },
 {
   "STT": 77,
   "Name": "Nhà thuốc Mỹ Phương",
   "address": "Số 9 Trần Phú, khóm Long Thạnh C, phường Long Hưng, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.794961,
   "Latitude": 105.2385435
 },
 {
   "STT": 78,
   "Name": "Nhà thuốc Hồng Phước",
   "address": "Nền 03 L12 Hàm Nghi, khóm Bình Khánh 5, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3931428,
   "Latitude": 105.4218372
 },
 {
   "STT": 79,
   "Name": "Nhà thuốc Khánh Nguyên",
   "address": "Số 41/4 Chưởng Binh Lễ, khóm Thới An, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3324586,
   "Latitude": 105.4838578
 },
 {
   "STT": 80,
   "Name": "Nhà thuốc 102",
   "address": "Tổ 9, khóm Tây Huề 1, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.368451,
   "Latitude": 105.397467
 },
 {
   "STT": 81,
   "Name": "Nhà thuốc Thành Đạt",
   "address": "Số H17 Tô Hiến Thành, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3729346,
   "Latitude": 105.4451466
 },
 {
   "STT": 85,
   "Name": "Nhà thuốc Thiện Phúc",
   "address": "Số 162 Châu Thị Tế, khóm Vĩnh Tây 1, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6831937,
   "Latitude": 105.0806978
 },
 {
   "STT": 86,
   "Name": "Nhà thuốc Hồng Trí",
   "address": "Số 68 Châu Thị Tế, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6831912,
   "Latitude": 105.0817394
 },
 {
   "STT": 87,
   "Name": "Nhà thuốc Song Thanh",
   "address": "Số 43C1 Nguyễn Thiện Thuật, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3920394,
   "Latitude": 105.4229141
 },
 {
   "STT": 88,
   "Name": "Nhà thuốc Phước Đặng",
   "address": "Số 1010 Tôn Đức Thắng, khóm Mỹ Thành, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.690769,
   "Latitude": 105.1448433
 },
 {
   "STT": 89,
   "Name": "Nhà thuốc 0177",
   "address": "Số 2/8, khóm Hòa Thạnh, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3495679,
   "Latitude": 105.4622698
 },
 {
   "STT": 90,
   "Name": "Nhà thuốc Hưng Phú",
   "address": "Số 544A Võ Thị Sáu, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3753208,
   "Latitude": 105.4358203
 },
 {
   "STT": 91,
   "Name": "Nhà thuốc Tân Thành",
   "address": "Số 38 Châu Văn Liêm, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3872698,
   "Latitude": 105.4374862
 },
 {
   "STT": 92,
   "Name": "Nhà thuốc Ngọc Hưng",
   "address": "Tổ 17, khóm Long Hưng 2, phường Long Sơn, Tx. Tân Châu, AnGiang",
   "Longtitude": 10.7726552,
   "Latitude": 105.248737
 },
 {
   "STT": 93,
   "Name": "Nhà thuốc Trí Hải",
   "address": "Số 224 Thủ Khoa Nghĩa, khóm 4, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7107944,
   "Latitude": 105.11625
 },
 {
   "STT": 94,
   "Name": "Nhà thuốc Ánh Mai",
   "address": "Tổ 8, khóm Vĩnh Tân, phường Vĩnh Ngươn, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7222677,
   "Latitude": 105.1054308
 },
 {
   "STT": 95,
   "Name": "Nhà thuốc Tây Nam",
   "address": "Số 17 Nguyễn Thái Học, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3882544,
   "Latitude": 105.4368976
 },
 {
   "STT": 96,
   "Name": "Nhà thuốc Nhật Bình",
   "address": "Số 87/B3 Tôn Thất Thuyết, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3929223,
   "Latitude": 105.4261552
 },
 {
   "STT": 97,
   "Name": "Nhà thuốc Bình Quyên",
   "address": "Số 926/10F Phạm Cự Lượng, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3664713,
   "Latitude": 105.4478638
 },
 {
   "STT": 98,
   "Name": "Nhà thuốc Vẹn Toàn",
   "address": "Số 1222/65, khóm Bình Đức 1, phường Bình Đức, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.40149,
   "Latitude": 105.4179164
 },
 {
   "STT": 99,
   "Name": "Nhà thuốc Liên Hương",
   "address": "Số 3C1 Nguyễn Thiện Thuật, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3920394,
   "Latitude": 105.4229141
 },
 {
   "STT": 100,
   "Name": "Nhà thuốc Thanh In",
   "address": "Số 2 Kênh Đào, khóm Mỹ Thành, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.690276,
   "Latitude": 105.1434387
 },
 {
   "STT": 101,
   "Name": "Nhà thuốc Hiệp Hòa",
   "address": "Tổ 28, đường Tân Lộ Kiều Lương, khóm Vĩnh Tây 1, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6794225,
   "Latitude": 105.0725341
 },
 {
   "STT": 102,
   "Name": "Nhà thuốc Khánh An",
   "address": "Số 124C/62 Trần Hưng Đạo, phường Bình Đức, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3893938,
   "Latitude": 105.430112
 },
 {
   "STT": 103,
   "Name": "Nhà thuốc Anh Thư 2",
   "address": "Số 11/4, khóm Đông Thạnh A, phường Mỹ Thạnh, thành phố  Long  Xuyên, AN GIANG",
   "Longtitude": 10.3357128,
   "Latitude": 105.4731317
 },
 {
   "STT": 104,
   "Name": "Nhà thuốc Mỹ Hạnh",
   "address": "Số 1/4 Lê Lợi, phườngMỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.389323,
   "Latitude": 105.4385385
 },
 {
   "STT": 105,
   "Name": "Nhà thuốc Bệnh viện Đa Khoa Nhật Tân",
   "address": "Số 32 Phạm Ngọc Thạch, tổ 14, khóm Châu Long 7, phường Châu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7026975,
   "Latitude": 105.1254365
 },
 {
   "STT": 106,
   "Name": "Nhà thuốc Thu Hiền",
   "address": "Số 202 Lý Thái Tổ, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.377888,
   "Latitude": 105.4422827
 },
 {
   "STT": 107,
   "Name": "Nhà thuốc Thái Phương 2",
   "address": "Số 841A Hà Hoàng Hổ, phường Đông Xuyên, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3753568,
   "Latitude": 105.4169373
 },
 {
   "STT": 108,
   "Name": "Nhà thuốc Bệnh viện Đa Khoa Hạnh Phúc",
   "address": "Số 234 Trần Hưng Đạo, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.354811,
   "Latitude": 105.4583055
 },
 {
   "STT": 109,
   "Name": "Nhà thuốc Hải Anh",
   "address": "Số 194/4A Bùi Văn Danh, phường Mỹ Hòa, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3825012,
   "Latitude": 105.4386611
 },
 {
   "STT": 110,
   "Name": "Nhà thuốc Tú Trân",
   "address": "Số 29E1 Trường Chinh, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.364018,
   "Latitude": 105.4429956
 },
 {
   "STT": 111,
   "Name": "Nhà thuốc Duy Nguyên",
   "address": "Số 31/4B Lê Chân, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3636341,
   "Latitude": 105.4489039
 },
 {
   "STT": 112,
   "Name": "Nhà thuốc Tú Phương",
   "address": "Số 38 Nguyễn Thị Minh Khai, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.79944,
   "Latitude": 105.2412834
 },
 {
   "STT": 113,
   "Name": "Nhà thuốc Kim Phương",
   "address": "Số 122 Nguyễn Văn Linh, phường MỹPhước, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3682584,
   "Latitude": 105.4330097
 },
 {
   "STT": 114,
   "Name": "Nhà thuốc Thủy Phạm",
   "address": "Số 50 Nguyễn Trường Tộ, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3951111,
   "Latitude": 105.4207737
 },
 {
   "STT": 115,
   "Name": "Nhà thuốc Khánh Như",
   "address": "Số 24/73 Trần Hưng Đạo, phường Mỹ Thạnh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.350578,
   "Latitude": 105.4616417
 },
 {
   "STT": 116,
   "Name": "Nhà thuốc Tâm Phúc",
   "address": "Số 88 Nguyễn Văn Thoại, phường Châu Phú A, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7090977,
   "Latitude": 105.117989
 },
 {
   "STT": 117,
   "Name": "Nhà thuốc Ngân Thủy",
   "address": "Tổ 13, khóm Châu Long 6, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7032388,
   "Latitude": 105.1222649
 },
 {
   "STT": 118,
   "Name": "Nhà thuốc Thảo Minh",
   "address": "Số 150B Lê Văn Nhung, phường Mỹ Bình, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3816831,
   "Latitude": 105.4355261
 },
 {
   "STT": 119,
   "Name": "Nhà thuốc Bệnh viện Mắt Long Xuyên",
   "address": "Số 17I1 Lý Thái Tổ- Ung Văn Khiêm, phường Đông Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3748814,
   "Latitude": 105.4321613
 },
 {
   "STT": 120,
   "Name": "Nhà thuốc Sao Mai",
   "address": "Lô N2 Chợ Sao Mai, khóm Bình Khánh 5, Đường số 6, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3891364,
   "Latitude": 105.4158568
 },
 {
   "STT": 121,
   "Name": "Nhà thuốc Duy Thanh",
   "address": "Số 2 Lê Lợi, phườngChâu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7063487,
   "Latitude": 105.1283497
 },
 {
   "STT": 122,
   "Name": "Nhà thuốc Trí Hiếu 2",
   "address": "Số 95/97 Điện Biên Phủ, phường Mỹ Long, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3820709,
   "Latitude": 105.4449137
 },
 {
   "STT": 123,
   "Name": "Nhà thuốc Tiền Hồ 13",
   "address": "Lô N14 chợ Sao Mai, khóm Bình Khánh 5, , phường Bình Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3891364,
   "Latitude": 105.4158568
 },
 {
   "STT": 124,
   "Name": "Nhà thuốc Tấn Phát",
   "address": "Số 469A/24 Quản Cơ Thành, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3932711,
   "Latitude": 105.4281328
 },
 {
   "STT": 125,
   "Name": "Nhà thuốc Duy Châu Lx",
   "address": "Số 1, số 3 Nguyễn Huệ B, phường Mỹ Long, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3830854,
   "Latitude": 105.4427462
 },
 {
   "STT": 126,
   "Name": "Nhà thuốc Ánh Linh",
   "address": "Số 413/12, tỉnh lộ 943, khóm Tây Khánh 4, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 127,
   "Name": "Nhà thuốc Minh Đức",
   "address": "Số 696 Trần Hưng Đạo, khóm Bình Đức 1, phường Bình Đức, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.399945,
   "Latitude": 105.4202345
 },
 {
   "STT": 128,
   "Name": "Nhà thuốc Số 8",
   "address": "Số 275/14 Trần Hưng Đạo, khóm Bình Thới 2, phường Bình Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3574073,
   "Latitude": 105.4561007
 },
 {
   "STT": 129,
   "Name": "Nhà thuốc Thanh Sang 1",
   "address": "Số 51/6 Trần Hưng Đạo, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3294861,
   "Latitude": 105.4843082
 },
 {
   "STT": 130,
   "Name": "Nhà thuốc Thành Sang",
   "address": "Số 512 Lois Pasteur, khóm Vĩnh Phú, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6820814,
   "Latitude": 105.0823967
 },
 {
   "STT": 131,
   "Name": "Nhà thuốc Ái Linh",
   "address": "Số 780 Ung Văn Khiêm, khóm Đông Thành, phường Đông Xuyên, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3726838,
   "Latitude": 105.4338911
 },
 {
   "STT": 132,
   "Name": "Nhà thuốc Hải Đăng",
   "address": "Số 571, khóm Bình Khánh 4, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3953269,
   "Latitude": 105.4208821
 },
 {
   "STT": 133,
   "Name": "Nhà thuốc Agimexphar M 7",
   "address": "Lô N39 Chợ Sao Mai, khóm Bình Khánh 5, phường Bình Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3891364,
   "Latitude": 105.4158568
 },
 {
   "STT": 134,
   "Name": "Nhà thuốc Agimexphar M 9",
   "address": "Tổ 23, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4426246,
   "Latitude": 105.3888335
 },
 {
   "STT": 135,
   "Name": "Nhà thuốc Tiền Hồ 2",
   "address": "Lô 29, khu 1 Chợ Mỹ Xuyên, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3776064,
   "Latitude": 105.4358658
 },
 {
   "STT": 136,
   "Name": "Nhà thuốc Bá Tòng",
   "address": "Số 201 Ung Văn Khiêm, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3671924,
   "Latitude": 105.4396794
 },
 {
   "STT": 137,
   "Name": "Nhà thuốcTrung Tâm Csskss AN GIANG",
   "address": "Số 79 Tôn Đức Thắng, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.385608,
   "Latitude": 105.4365098
 },
 {
   "STT": 138,
   "Name": "Nhà thuốc Đức Duy",
   "address": "Số 176, khóm Vĩnh Chánh 2, phường Vĩnh Ngươn, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7241412,
   "Latitude": 105.109085
 },
 {
   "STT": 139,
   "Name": "Nhà thuốc Thanh Sang",
   "address": "Số 220 Nguyễn Thái Học, P.Mỹ Bình, TPLX, AN GIANG",
   "Longtitude": 10.3845554,
   "Latitude": 105.4327259
 },
 {
   "STT": 140,
   "Name": "Nhà thuốc Minh Huy",
   "address": "Số 1/1B Lê Triệu Kiết, P.Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3874691,
   "Latitude": 105.4372716
 },
 {
   "STT": 141,
   "Name": "Nhà thuốc Tiến Thuận",
   "address": "Số 537/27 Trần Hưng Đạo, P.Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3899461,
   "Latitude": 105.4298104
 },
 {
   "STT": 142,
   "Name": "Nhà thuốc Hồng Hà",
   "address": "Số 119 Châu Văn Liêm, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3870728,
   "Latitude": 105.4372573
 },
 {
   "STT": 143,
   "Name": "Nhà thuốc Trí Thức",
   "address": "Số 5 khu 2, Hà Hoàng Hổ, P.Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3766995,
   "Latitude": 105.4200947
 },
 {
   "STT": 144,
   "Name": "Nhà thuốc Huệ Chi",
   "address": "Số 60/2 Thoại Ngọc Hầu, P.Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3804133,
   "Latitude": 105.4447655
 },
 {
   "STT": 145,
   "Name": "Nhà thuốc Triệu Tín",
   "address": "Số 39 Ung Văn Khiêm, P.Đông Xuyên, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3748814,
   "Latitude": 105.4321613
 },
 {
   "STT": 146,
   "Name": "Nhà thuốc Hương Ngân",
   "address": "Số 07 Huỳnh Văn Hây, P.Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3838137,
   "Latitude": 105.4403297
 },
 {
   "STT": 147,
   "Name": "Nhà thuốc Minh Bửu",
   "address": "Số 10/1 Đào Duy Từ,P.Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3648437,
   "Latitude": 105.4484695
 },
 {
   "STT": 148,
   "Name": "Nhà thuốc Thùy Quyên",
   "address": "Số 2 Nguyễn Văn Thoại, P.Châu Phú B,TX.Châu Đốc, AN GIANG",
   "Longtitude": 10.7106651,
   "Latitude": 105.1200735
 },
 {
   "STT": 149,
   "Name": "Nhà thuốc Nguyễn Thanh",
   "address": "Số 91 Phan Đình Phùng nối dài, phường Châu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6810551,
   "Latitude": 105.1083542
 },
 {
   "STT": 150,
   "Name": "Nhà thuốc Hiệp Thành",
   "address": "Số 89 Lê Minh Ngươn, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3835176,
   "Latitude": 105.4420852
 },
 {
   "STT": 151,
   "Name": "Nhà thuốc Đại Dương",
   "address": "Số 9/5 Lê Triệu Kiết, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3922099,
   "Latitude": 105.435097
 },
 {
   "STT": 152,
   "Name": "Nhà thuốc Quân Y",
   "address": "Thửa số 1, đường Trần Hưng Đạo (Bệnh xá Quân y), phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3939023,
   "Latitude": 105.4258132
 },
 {
   "STT": 153,
   "Name": "Nhà thuốc Tâm Phương",
   "address": "Số 766A, tổ 39B, khóm Bình Đức 4, phường Bình Đức, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.4031695,
   "Latitude": 105.419113
 },
 {
   "STT": 154,
   "Name": "Nhà thuốc Mỹ Thạnh",
   "address": "Số 53/3, Đông Thạnh B, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3357128,
   "Latitude": 105.4731317
 },
 {
   "STT": 155,
   "Name": "Nhà thuốc Trọng Nhân",
   "address": "Số 2/3A Nguyễn Thái Học, phường Mỹ Bình, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3864325,
   "Latitude": 105.434865
 },
 {
   "STT": 156,
   "Name": "Nhà thuốc Quốc Thái",
   "address": "Số 22 Nguyễn Văn  Linh, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7990091,
   "Latitude": 105.2485046
 },
 {
   "STT": 157,
   "Name": "Nhà thuốc Đăng Khoa",
   "address": "Số 6/7 Lê Triệu Kiết,phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.38682,
   "Latitude": 105.4378778
 },
 {
   "STT": 158,
   "Name": "Nhà thuốc Khánh Thi",
   "address": "Số 140/5 Nguyễn Thái Học, phường Mỹ Bình, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3887404,
   "Latitude": 105.4375196
 },
 {
   "STT": 159,
   "Name": "Nhà thuốc Trí Hiếu",
   "address": "Số 97A Lê Thị Nhiên, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.383807,
   "Latitude": 105.4426702
 },
 {
   "STT": 160,
   "Name": "Nhà thuốc Kim Thoa",
   "address": "Số 638 Nguyễn Văn Thoại, phường Châu Phú A, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7046025,
   "Latitude": 105.1118106
 },
 {
   "STT": 161,
   "Name": "Nhà thuốc Khải Hoàng",
   "address": "Số 1048 Tôn Đức Thắng, khóm Mỹ Thành, phường Vĩnh Mỹ, thành phố  Châu Đốc, AnGiang",
   "Longtitude": 10.690769,
   "Latitude": 105.1448433
 },
 {
   "STT": 162,
   "Name": "Nhà thuốc Kiều Nga",
   "address": "Lô 85, chợ Mỹ Xuyên, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3776064,
   "Latitude": 105.4358658
 },
 {
   "STT": 163,
   "Name": "Nhà thuốc 689",
   "address": "Số 769 Hà Hoàng Hổ, phường Đông Xuyên, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.375979,
   "Latitude": 105.4185437
 },
 {
   "STT": 164,
   "Name": "Nhà thuốc 63",
   "address": "Số 181/9Q Ung Văn Khiêm, phường Mỹ Phước, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3648881,
   "Latitude": 105.4403492
 },
 {
   "STT": 165,
   "Name": "Nhà thuốc Trung Hậu",
   "address": "Số 807 Hà Hoàng Hổ, phường Đông Xuyên, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3777848,
   "Latitude": 105.4312175
 },
 {
   "STT": 166,
   "Name": "Nhà thuốc Tân Bình",
   "address": "Số 851 Hà Hoàng Hổ, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3753516,
   "Latitude": 105.4169227
 },
 {
   "STT": 167,
   "Name": "Nhà thuốc Phương Khanh",
   "address": "Số 50 Ngô Gia Tự, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3792399,
   "Latitude": 105.4434192
 },
 {
   "STT": 168,
   "Name": "Nhà thuốc Agimedic",
   "address": "Số 2/7 Châu Văn Liêm, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3865182,
   "Latitude": 105.4367086
 },
 {
   "STT": 169,
   "Name": "Nhà thuốc Mỹ Châu VT",
   "address": "Số 327/1 Hùng Vương, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3794455,
   "Latitude": 105.4416736
 },
 {
   "STT": 170,
   "Name": "Nhà thuốc Giang Thanh",
   "address": "Số 262/3 Hùng Vương, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3794455,
   "Latitude": 105.4416736
 },
 {
   "STT": 171,
   "Name": "Nhà thuốc Thảo Vy",
   "address": "Thửa 171 Trần Hưng Đạo, phường Bình Đức, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3864616,
   "Latitude": 105.4346895
 },
 {
   "STT": 172,
   "Name": "Nhà thuốc Lam Anh",
   "address": "Số 26/7A, khóm Trung An, phường Mỹ Thới, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3368079,
   "Latitude": 105.4478131
 },
 {
   "STT": 173,
   "Name": "Nhà thuốc An Toàn",
   "address": "Số 283 Trần Hưng Đạo, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3848234,
   "Latitude": 105.4366874
 },
 {
   "STT": 174,
   "Name": "Nhà thuốc Hồng Vân",
   "address": "Số 12-14 Nguyễn Văn Thoại, phường Châu Phú A, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7093576,
   "Latitude": 105.1183578
 },
 {
   "STT": 175,
   "Name": "Nhà thuốc Bệnh viện TIM MẠCH",
   "address": "Số 8 Nguyễn Du, phường Mỹ Bình, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3867375,
   "Latitude": 105.4409278
 },
 {
   "STT": 176,
   "Name": "Nhà thuốc VÂN TRANG",
   "address": "Số 16 Lý Thái Tổ, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3776967,
   "Latitude": 105.4417777
 },
 {
   "STT": 177,
   "Name": "Nhà thuốc Dược Thảo",
   "address": "Tổ 8, ấp Phú Hữu, thị trấn Phú Hòa, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 178,
   "Name": "Nhà thuốc NGỌC DUNG",
   "address": "Số 41-43, lô B, đường Tân Lộ Kiều Lương, phường Núi Sam, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.6827399,
   "Latitude": 105.0823904
 },
 {
   "STT": 179,
   "Name": "Nhà thuốc HẢI ĐẾN",
   "address": "Số 14 Nguyễn Hữu Cảnh, khóm 5, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7097467,
   "Latitude": 105.1181107
 },
 {
   "STT": 180,
   "Name": "Nhà thuốc Bệnh viện đa khoa Trung tâm AnGiang",
   "address": "Số 60 Ung Văn Khiêm, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3647016,
   "Latitude": 105.4404358
 },
 {
   "STT": 181,
   "Name": "Nhà thuốc Quỳnh Mai",
   "address": "Lô 29M8 Lưu Hữu Phước, phường Mỹ Phước, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3632641,
   "Latitude": 105.4373136
 },
 {
   "STT": 182,
   "Name": "Nhà thuốc Kim Minh",
   "address": "Số 524A/28 Trần Hưng Đạo, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.350578,
   "Latitude": 105.4616417
 },
 {
   "STT": 183,
   "Name": "Nhà thuốc Phương Ngân",
   "address": "Số 21/6C Trần Hưng Đạo, phường Mỹ Quý, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3789915,
   "Latitude": 105.4394272
 },
 {
   "STT": 184,
   "Name": "Nhà thuốc Hoàng Thanh",
   "address": "Số 597 Tôn Đức Thắng, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6980767,
   "Latitude": 105.1372851
 },
 {
   "STT": 185,
   "Name": "Nhà thuốc Hiếu Nghĩa",
   "address": "Số 30/5B Lê Triệu Kiết, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3922099,
   "Latitude": 105.435097
 },
 {
   "STT": 186,
   "Name": "Nhà thuốc Bảo  Nghi",
   "address": "Số 89B3 Tôn Thất Thuyết, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3929218,
   "Latitude": 105.4261546
 },
 {
   "STT": 187,
   "Name": "Nhà thuốc Thủy Tiên 2",
   "address": "Lô 1 & 2, khu D, TTTM khóm Vĩnh Đông,phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6823267,
   "Latitude": 105.073886
 },
 {
   "STT": 188,
   "Name": "Nhà thuốc Tuyết Minh",
   "address": "Số 11/3 Thoại Ngọc Hầu, phường Mỹ Phước, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3801849,
   "Latitude": 105.4446693
 },
 {
   "STT": 189,
   "Name": "Nhà thuốc Tiền Hồ 1",
   "address": "Số 150/4 Nguyễn Thái Học, phường Mỹ Bình, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3892347,
   "Latitude": 105.4378275
 },
 {
   "STT": 190,
   "Name": "Nhà thuốc Bệnh viện đa khoa Bình Dân",
   "address": "Số 39 Trần Hưng Đạo, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.389747,
   "Latitude": 105.4301344
 },
 {
   "STT": 191,
   "Name": "Nhà thuốc Phương Khánh",
   "address": "Số 24 Hai Bà Trưng, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3830753,
   "Latitude": 105.4409761
 },
 {
   "STT": 192,
   "Name": "Nhà thuốc Phú Thành",
   "address": "Số 24 Ngô Thời Nhậm, phường Mỹ Long, thành phố Long Xuyên, AnGiang",
   "Longtitude": 10.3827571,
   "Latitude": 105.4450339
 },
 {
   "STT": 193,
   "Name": "Nhà thuốc Châu Đốc",
   "address": "Số 272 Trương Định, phường Châu Phú B,  thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7013779,
   "Latitude": 105.1261401
 },
 {
   "STT": 194,
   "Name": "Nhà thuốc Lê Mỹ",
   "address": "Số 283 Tôn Đức Thắng, khóm Long Thạnh D, phường Long Thạnh, TX.Tân Châu, AN GIANG",
   "Longtitude": 10.7991208,
   "Latitude": 105.2451032
 },
 {
   "STT": 195,
   "Name": "Nhà thuốc 91",
   "address": "Số 91 Lê Lợi, phường Châu Phú B, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7046782,
   "Latitude": 105.1304708
 },
 {
   "STT": 196,
   "Name": "Nhà thuốc Cẩm Tú",
   "address": "Số 69A Nguyễn Văn Linh, phường MỹPhước, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3640656,
   "Latitude": 105.4282251
 },
 {
   "STT": 197,
   "Name": "Nhà thuốc Bệnh viện thành phố Long Xuyên",
   "address": "Số 9 Hải Thượng Lãn Ông, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3752187,
   "Latitude": 105.4411361
 },
 {
   "STT": 198,
   "Name": "Nhà thuốc Trí Đức",
   "address": "Số 11/1 Thoại Ngọc Hầu, phường Mỹ Long, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3800553,
   "Latitude": 105.4446482
 },
 {
   "STT": 199,
   "Name": "Nhà thuốc Huy Chương",
   "address": "Số 326 Ung Văn Khiêm, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3666071,
   "Latitude": 105.4397758
 },
 {
   "STT": 200,
   "Name": "Nhà thuốc Phúc Khang",
   "address": "Số 42 Tôn Đức Thắng, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3867805,
   "Latitude": 105.4358511
 },
 {
   "STT": 201,
   "Name": "Nhà thuốc Đại Sơn",
   "address": "Số 5H Hồ Quý Ly, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3648011,
   "Latitude": 105.4475443
 },
 {
   "STT": 202,
   "Name": "Nhà thuốc Tiền Hồ 3",
   "address": "Số 836 Hà Hoàng Hổ, phường Đông Xuyên, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3753595,
   "Latitude": 105.4169445
 },
 {
   "STT": 203,
   "Name": "Nhà thuốc Huy Anh",
   "address": "Số 54/8 Trần Hưng Đạo, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3586748,
   "Latitude": 105.4552536
 },
 {
   "STT": 204,
   "Name": "Nhà thuốc Sơn Thảo",
   "address": "Tổ 9, khóm Mỹ Thành, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.1396322
 },
 {
   "STT": 205,
   "Name": "Nhà thuốc Bạch Đằng",
   "address": "Số 96 Bạch Đằng, khóm 5, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7058379,
   "Latitude": 105.1054308
 },
 {
   "STT": 206,
   "Name": "Nhà thuốc Trường Phú",
   "address": "Số 164/1B Thoại Ngọc Hầu, phường Mỹ Long, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3800553,
   "Latitude": 105.4446482
 },
 {
   "STT": 207,
   "Name": "Nhà thuốc Trường Phúc",
   "address": "Số 22/12C Trần Hưng Đạo, phường Mỹ Quý, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3578687,
   "Latitude": 105.4559049
 },
 {
   "STT": 208,
   "Name": "Nhà thuốc Agimexpharm 2",
   "address": "Số 11/1 Châu Văn Liêm, phường Mỹ Bình, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3865182,
   "Latitude": 105.4367086
 },
 {
   "STT": 209,
   "Name": "Nhà thuốc Agimexpharm 3",
   "address": "Số 79 Tôn Đức Thắng, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.385608,
   "Latitude": 105.4365098
 },
 {
   "STT": 210,
   "Name": "Nhà thuốc Agimexpharm 6",
   "address": "Số 1 Phan Đình Phùng, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3835823,
   "Latitude": 105.4411165
 },
 {
   "STT": 211,
   "Name": "Nhà thuốc Bệnh viện Mắt-TMH- RHM",
   "address": "Số 12B Lê Lợi, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3871257,
   "Latitude": 105.4395485
 },
 {
   "STT": 212,
   "Name": "Nhà thuốc Gia Bảo Ngọc",
   "address": "Số 42/21C, Quốc lộ 91, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.350578,
   "Latitude": 105.4616417
 },
 {
   "STT": 213,
   "Name": "Nhà thuốc Gia Hòa",
   "address": "Số 132/2A Trần Hưng Đạo, phường Mỹ Phước, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3788658,
   "Latitude": 105.4394464
 },
 {
   "STT": 214,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "Tổ 17, khóm Mỹ Chánh, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6872304,
   "Latitude": 105.1402067
 },
 {
   "STT": 215,
   "Name": "Nhà thuốc Pha No",
   "address": "Số 28 Phan Đình Phùng, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3841146,
   "Latitude": 105.4413588
 },
 {
   "STT": 216,
   "Name": "Nhà thuốc Vĩnh Huy",
   "address": "Số 113 Thủ Khoa Nghĩa, phường Châu Phú A, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7156329,
   "Latitude": 105.1118173
 },
 {
   "STT": 217,
   "Name": "Nhà thuốc Vạn Phước",
   "address": "Số 68 Bạch Đằng, khóm 5, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7110793,
   "Latitude": 105.1186884
 },
 {
   "STT": 218,
   "Name": "Nhà thuốc Thân Thiện",
   "address": "Số 327 Ung Văn Khiêm, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3671924,
   "Latitude": 105.4396794
 },
 {
   "STT": 219,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "Số 4C2 Thành Thái, phường Bình Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3925543,
   "Latitude": 105.4224984
 },
 {
   "STT": 220,
   "Name": "Nhà thuốc Tiền Hồ 4",
   "address": "Lô 1A sân C, chợ Bình Khánh, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3916532,
   "Latitude": 105.4232597
 },
 {
   "STT": 221,
   "Name": "Nhà thuốc Thu An",
   "address": "Số 578/8, tổ 4, Tây Khánh 4, phường Mỹ Hòa, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 222,
   "Name": "Nhà thuốc 87",
   "address": "Số 189 Lê Lợi, phường Châu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6810551,
   "Latitude": 105.1083542
 },
 {
   "STT": 223,
   "Name": "Nhà thuốc Phước Thới",
   "address": "Số 62 La Thành Thân, phường Châu Phú B,  thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6810551,
   "Latitude": 105.1083542
 },
 {
   "STT": 224,
   "Name": "Nhà thuốc Mười Một",
   "address": "Số 2 Lê Minh Ngươn, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3818495,
   "Latitude": 105.4433495
 },
 {
   "STT": 225,
   "Name": "Nhà thuốc Hải Mai",
   "address": "Số 119, tổ 4, tỉnh lộ 954, khóm Long Hưng 1, phường Long Sơn, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7985252,
   "Latitude": 105.2523859
 },
 {
   "STT": 226,
   "Name": "Nhà thuốc Như Ý",
   "address": "Số 38/1 Lê Chân, phường Mỹ Quý, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3636341,
   "Latitude": 105.4489039
 },
 {
   "STT": 227,
   "Name": "Nhà thuốc Cát Tường",
   "address": "Số 45/12, Trung An, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3562421,
   "Latitude": 105.4582046
 },
 {
   "STT": 228,
   "Name": "Nhà thuốc Nhật Tân",
   "address": "Số 42 Lý Phật Mã, phường Bình Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3932172,
   "Latitude": 105.4151241
 },
 {
   "STT": 229,
   "Name": "Nhà thuốc Kim Ngọc",
   "address": "Số 16/5, khóm An Hưng, phường Mỹ Thới, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3562421,
   "Latitude": 105.4582046
 },
 {
   "STT": 230,
   "Name": "Nhà thuốc Tú Quân",
   "address": "Số 11 Nguyễn Thị Minh Khai, phường Mỹ Long, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3812352,
   "Latitude": 105.4420887
 },
 {
   "STT": 231,
   "Name": "Nhà thuốc Bạch Đằng",
   "address": "Số 29 Đoàn Văn Phối, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.384183,
   "Latitude": 105.4406337
 },
 {
   "STT": 232,
   "Name": "Nhà thuốc Phú Thịnh",
   "address": "Khóm An Hòa B, thị trấn Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.489575,
   "Latitude": 104.903946
 },
 {
   "STT": 233,
   "Name": "Nhà thuốc Huỳnh Trang",
   "address": "Số 107 Phạm Cự Lượng, phường MỹPhước, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3660122,
   "Latitude": 105.4471464
 },
 {
   "STT": 234,
   "Name": "Nhà thuốc Hưng Thạnh",
   "address": "Số 90 Lê Minh Ngươn, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3835176,
   "Latitude": 105.4420852
 },
 {
   "STT": 235,
   "Name": "Nhà thuốc Thanh Trinh",
   "address": "Tổ 11, khóm Mỹ Chánh, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6869563,
   "Latitude": 105.1398205
 },
 {
   "STT": 236,
   "Name": "Nhà thuốc Quốc Khang",
   "address": "Số 65 Lê Duẫn, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.797223,
   "Latitude": 105.2503162
 },
 {
   "STT": 237,
   "Name": "Nhà thuốc Phương Huỳnh",
   "address": "Số 165, tỉnh lộ 954, khóm Long Hưng 1, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8005402,
   "Latitude": 105.2473292
 },
 {
   "STT": 238,
   "Name": "Nhà thuốc Tiền Hồ 5",
   "address": "Số 68B Nguyễn Thái Học, phường Mỹ Bình, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3869223,
   "Latitude": 105.4351219
 },
 {
   "STT": 239,
   "Name": "Nhà thuốc Ngọc Thủy",
   "address": "Số 19 Nguyễn Chí Thanh, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.7993413,
   "Latitude": 105.2474087
 },
 {
   "STT": 240,
   "Name": "Nhà thuốc Ngọc Thạch",
   "address": "Số 15A Lê Văn Nhung, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3834577,
   "Latitude": 105.4362721
 },
 {
   "STT": 241,
   "Name": "Nhà thuốc Minh Thy",
   "address": "Số 48B2 Tôn Thất Thuyết, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3929327,
   "Latitude": 105.4261663
 },
 {
   "STT": 242,
   "Name": "Nhà thuốc Nhật Hồng",
   "address": "Số 120 Châu Văn Liêm, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3870728,
   "Latitude": 105.4372573
 },
 {
   "STT": 243,
   "Name": "Nhà thuốc Vạn An",
   "address": "Số 23A Tôn Đức Thắng, phường Mỹ Bình, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3886451,
   "Latitude": 105.4353099
 },
 {
   "STT": 244,
   "Name": "Nhà thuốc Chung Thiên Phú",
   "address": "Số 311/2A Trần Hưng Đạo, phường Mỹ Long, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3809505,
   "Latitude": 105.439126
 },
 {
   "STT": 245,
   "Name": "Nhà thuốc Quốc Minh",
   "address": "Số 01 Phan Châu Trinh, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3792399,
   "Latitude": 105.4434192
 },
 {
   "STT": 246,
   "Name": "Nhà thuốc Ngọc Mỹ",
   "address": "Số 21 Ngô Thì Nhậm, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3838471,
   "Latitude": 105.442632
 },
 {
   "STT": 247,
   "Name": "Nhà thuốc Ung Văn Khương",
   "address": "Số 47 Tôn Đức Thắng, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3876953,
   "Latitude": 105.4356865
 },
 {
   "STT": 248,
   "Name": "Nhà thuốc Hòa Phát",
   "address": "Số 04 Lê Công Thành, phường Châu Phú A,  thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.710839,
   "Latitude": 105.1197352
 },
 {
   "STT": 249,
   "Name": "Nhà thuốc Bảo Linh",
   "address": "Tổ 18, khóm Long Hưng, phường Long Châu, Tx. Tân Châu, AnGiang",
   "Longtitude": 10.7958634,
   "Latitude": 105.2308219
 },
 {
   "STT": 250,
   "Name": "Nhà thuốc Hà Tiên",
   "address": "Số 36B2 Tôn Thất Thuyết, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3929358,
   "Latitude": 105.4261698
 },
 {
   "STT": 251,
   "Name": "Nhà thuốc Kim Quỳnh",
   "address": "Số 244G Đề Thám, phường Bình Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3951357,
   "Latitude": 105.4209906
 },
 {
   "STT": 252,
   "Name": "Nhà thuốc Bệnh viện Sản NhiAN GIANG",
   "address": "Số 02 Lê Lợi, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3922955,
   "Latitude": 105.4352612
 },
 {
   "STT": 253,
   "Name": "Nhà thuốc Duy Thảo",
   "address": "Số 90 Lê Lợi, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3897209,
   "Latitude": 105.4377594
 },
 {
   "STT": 254,
   "Name": "Nhà thuốc AN GIANG",
   "address": "Số 34 Nguyễn Văn Thoại, phường Châu Phú A, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7100822,
   "Latitude": 105.1193315
 },
 {
   "STT": 255,
   "Name": "Nhà thuốc Thủy Tiên",
   "address": "Lô số 7, 8, 9, 10 - lô G,Chợ Vĩnh Đông, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6823267,
   "Latitude": 105.073886
 },
 {
   "STT": 256,
   "Name": "Nhà thuốcPhương Khanh 1",
   "address": "Số 24B Nguyễn Trãi, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.381583,
   "Latitude": 105.4433894
 },
 {
   "STT": 257,
   "Name": "Nhà thuốc Pha No 3",
   "address": "Số 39 Lê Minh Ngươn, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3846488,
   "Latitude": 105.441043
 },
 {
   "STT": 258,
   "Name": "Nhà thuốc Nguyễn Mai",
   "address": "Số 130 Nguyễn Văn Thoại, khóm 6, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7090859,
   "Latitude": 105.1179722
 },
 {
   "STT": 259,
   "Name": "Nhà thuốc Phú Khánh",
   "address": "Số 91 Lê Thị Nhiên, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3839415,
   "Latitude": 105.4425542
 },
 {
   "STT": 260,
   "Name": "Nhà thuốc Tiến Hùng",
   "address": "Số 136A Ung Văn Khiêm, phường Đông Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3746611,
   "Latitude": 105.4323626
 },
 {
   "STT": 261,
   "Name": "Nhà thuốc An Mỹ",
   "address": "Số 14, 15, 16 F1 PhạmCự Lượng, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3664713,
   "Latitude": 105.4478638
 },
 {
   "STT": 262,
   "Name": "Nhà thuốc Bảo Thanh 2",
   "address": "Số 922/10C Phạm Cự Lượng, phường Mỹ Quý, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3664713,
   "Latitude": 105.4478638
 },
 {
   "STT": 263,
   "Name": "Nhà thuốc Bình An",
   "address": "Số 4L19, khóm Bình Khánh 5, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3891364,
   "Latitude": 105.4158568
 },
 {
   "STT": 264,
   "Name": "Nhà thuốc Mỹ Yến",
   "address": "Số 7/9 Đinh Tiên Hoàng, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.386202,
   "Latitude": 105.4392412
 },
 {
   "STT": 265,
   "Name": "Nhà thuốc Phương Thảo",
   "address": "Số 1325A Trần Hưng Đạo, phường Bình Đức, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3793579,
   "Latitude": 105.4394133
 },
 {
   "STT": 266,
   "Name": "Nhà thuốc Bình Thạnh",
   "address": "Số 1188 Tôn Đức Thắng, khóm Mỹ Thành, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.690769,
   "Latitude": 105.1448433
 },
 {
   "STT": 267,
   "Name": "Nhà thuốc Tiền Hồ 8",
   "address": "Lô K1, khóm An Hưng, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3368079,
   "Latitude": 105.4478131
 },
 {
   "STT": 268,
   "Name": "Nhà thuốc Thủy Tiên",
   "address": "Kios A1O3 Đào Duy Từ, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.364799,
   "Latitude": 105.448498
 },
 {
   "STT": 269,
   "Name": "Nhà thuốc Tân Tiến",
   "address": "Lô A1, Chợ Cái Sao, khóm An Hưng, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3466449,
   "Latitude": 105.4685482
 },
 {
   "STT": 270,
   "Name": "Nhà thuốc Phương Thúy",
   "address": "Số 666 Trần Hưng Đạo, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3685559,
   "Latitude": 105.4473385
 },
 {
   "STT": 271,
   "Name": "Nhà thuốc Xuân Mai",
   "address": "Số 73 Nguyễn Văn Thoại, phường Châu Phú B, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7095461,
   "Latitude": 105.1186115
 },
 {
   "STT": 272,
   "Name": "Nhà thuốc Anh Kiệt",
   "address": "Số 784/9, tổ 35, khóm Tây Khánh 5, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 273,
   "Name": "Nhà thuốc số 25",
   "address": "Tổ 19, khóm Tây Khánh 3, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3774788,
   "Latitude": 105.4232138
 },
 {
   "STT": 274,
   "Name": "Nhà thuốc Thanh Long",
   "address": "Số 24/3A, khóm Tây Huề 1, phường Mỹ Hòa, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 275,
   "Name": "Nhà thuốc Thành Trí",
   "address": "Số 1232/62, khóm Bình Đức 1, phường Bình Đức, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.40149,
   "Latitude": 105.4179164
 },
 {
   "STT": 276,
   "Name": "Nhà thuốc 39",
   "address": "Số 34C6 Trần Phú, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3657848,
   "Latitude": 105.4416911
 },
 {
   "STT": 277,
   "Name": "Nhà thuốc Minh Điển",
   "address": "Số 22/19 Trần Quý Cáp, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3461458,
   "Latitude": 105.4673068
 },
 {
   "STT": 278,
   "Name": "Nhà thuốc Phi Hải",
   "address": "Số 36A2 Tôn Thất Thuyết, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3929358,
   "Latitude": 105.4261698
 },
 {
   "STT": 279,
   "Name": "Nhà thuốc Song Toàn",
   "address": "Số 61 Trần Hưng Đạo, phường Mỹ Bình, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3894082,
   "Latitude": 105.4300895
 },
 {
   "STT": 280,
   "Name": "Nhà thuốc Huỳnh Tiên",
   "address": "Đường Tân Lộ Kiều Lương, tổ 19, khóm Vĩnh Đông, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6823267,
   "Latitude": 105.073886
 },
 {
   "STT": 281,
   "Name": "Nhà thuốc Khôi Nguyên",
   "address": "Số 330 Nguyễn Văn Thoại, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7036537,
   "Latitude": 105.1105382
 },
 {
   "STT": 282,
   "Name": "Nhà thuốc Vân Võ",
   "address": "Lô 19.17 đường số 7, KDC Bắc Hà Hoàng Hổ, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3869981,
   "Latitude": 105.4267131
 },
 {
   "STT": 283,
   "Name": "Nhà thuốc Bích Ngọc",
   "address": "Số 9/53, khóm Thới Hòa, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 284,
   "Name": "Nhà thuốc Phương Loan",
   "address": "Số 154 Lê Lợi, phường Châu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7046782,
   "Latitude": 105.1304708
 },
 {
   "STT": 285,
   "Name": "Nhà thuốc MINH PHÚC",
   "address": "Số 97 Nguyễn Huệ B, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.381373,
   "Latitude": 105.4406611
 },
 {
   "STT": 286,
   "Name": "Nhà thuốc Võ Tấn Phát",
   "address": "Lô 7C1, khóm Hòa Thạnh, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3495679,
   "Latitude": 105.4622698
 },
 {
   "STT": 287,
   "Name": "Nhà thuốc Tuyết Vân",
   "address": "Số 15/6 Trần Hưng Đạo, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3789915,
   "Latitude": 105.4394272
 },
 {
   "STT": 288,
   "Name": "Nhà thuốc Kim Nguyên",
   "address": "Số 02 Nguyễn Huệ, khóm Long Thị C,phường Long Hưng, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7958634,
   "Latitude": 105.2308219
 },
 {
   "STT": 289,
   "Name": "Nhà thuốc Cẩm Xém",
   "address": "Số 06 Nguyễn Văn  Linh, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7994077,
   "Latitude": 105.2486551
 },
 {
   "STT": 290,
   "Name": "Nhà thuốc Kim Tỷ",
   "address": "Số 01, tổ 01, khóm Long An B, phường Long Phú, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7883758,
   "Latitude": 105.2276171
 },
 {
   "STT": 291,
   "Name": "Nhà thuốc Mỹ Duyên",
   "address": "Số 114 Tôn Đức Thắng, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7987827,
   "Latitude": 105.2463251
 },
 {
   "STT": 292,
   "Name": "Nhà thuốc Lý Hên",
   "address": "Số 9 Trường Chinh, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.798846,
   "Latitude": 105.2505255
 },
 {
   "STT": 293,
   "Name": "Nhà thuốc Chi Lan",
   "address": "Tổ 37, khóm Vĩnh Tây 1, phường Núi Sam, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6732681,
   "Latitude": 105.0718158
 },
 {
   "STT": 294,
   "Name": "Nhà thuốc 265",
   "address": "Số 265 Thủ Khoa Nghĩa, khóm 4, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7107133,
   "Latitude": 105.1163236
 },
 {
   "STT": 295,
   "Name": "Nhà thuốc Ngọc Ánh",
   "address": "Số 29, khóm Vĩnh Tây 1, đường Tâm Lộ Kiều Lương, phường Núi Sam, thành phố  Châu Đốc, AnGiang",
   "Longtitude": 10.6794225,
   "Latitude": 105.0725341
 },
 {
   "STT": 296,
   "Name": "Nhà thuốc Yên Bình",
   "address": "Số 184, đường Tân Lộ Kiều Lương, khóm 8, phường Châu Phú A,  thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6951485,
   "Latitude": 105.099008
 },
 {
   "STT": 297,
   "Name": "Nhà thuốc Cẩm Tú",
   "address": "Số 62 Trương Định, phường Châu Phú B,  thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7045023,
   "Latitude": 105.1285846
 },
 {
   "STT": 298,
   "Name": "Nhà thuốc Đào Phương",
   "address": "Số 58/5B, khóm Đông Thạnh B, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3247736,
   "Latitude": 105.471249
 },
 {
   "STT": 299,
   "Name": "Nhà thuốc Bảo Thanh 1",
   "address": "Số 09 Bùi Văn Danh, phường Đông Xuyên, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3804011,
   "Latitude": 105.4386009
 },
 {
   "STT": 300,
   "Name": "Nhà thuốc 47",
   "address": "Số 903 Trần Hưng Đạo, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3942434,
   "Latitude": 105.4256419
 },
 {
   "STT": 301,
   "Name": "Nhà thuốc Mỹ Phụng",
   "address": "Số 2275 Trần Hưng Đạo, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3596224,
   "Latitude": 105.454564
 },
 {
   "STT": 302,
   "Name": "Nhà thuốc 0151",
   "address": "Số 57/26A, khóm Mỹ Phú, phường Mỹ Quý, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3563003,
   "Latitude": 105.4390254
 },
 {
   "STT": 303,
   "Name": "Nhà thuốc Tấn Phước",
   "address": "Số 577/8A, khóm Tây Khánh 4, phường Mỹ Hòa, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 304,
   "Name": "Nhà thuốc Tâm Đức AN GIANG",
   "address": "Số 513B Ung Văn Khiêm, phường Đông Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3732093,
   "Latitude": 105.4338958
 },
 {
   "STT": 305,
   "Name": "Quầy thuốc Tiền Hồ 11",
   "address": "Lô 154-155, Chợ Cần Đăng, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4593829,
   "Latitude": 105.2950544
 },
 {
   "STT": 307,
   "Name": "Quầy thuốc Mỹ Hường",
   "address": "Số 167, tổ 02, ấp Mỹ Hòa B, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3772782,
   "Latitude": 105.4221943
 },
 {
   "STT": 308,
   "Name": "Quầy thuốc Phúc Lợi",
   "address": "Số 456B, ấp Long Định, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 311,
   "Name": "Quầy thuốc Thiên Di",
   "address": "Số 38, tổ 04, ấp Long Thành, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4957171,
   "Latitude": 105.4511467
 },
 {
   "STT": 312,
   "Name": "Quầy thuốc Phương Đông",
   "address": "Số 300, tổ 12, ấp Long Định, xã Long Kiến, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4607668,
   "Latitude": 105.4504064
 },
 {
   "STT": 313,
   "Name": "Quầy thuốc Mỹ Anh",
   "address": "Số 94, tổ 04, ấp An Long, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 314,
   "Name": "Quầy thuốc Ngọc Nga",
   "address": "Tổ 19, ấp An Thái, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 315,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Tổ 13, ấp Hòa Phú, xã Định Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 316,
   "Name": "Quầy thuốc Trung Quân",
   "address": "Số 30 Lê Hồng Phong, ấp Đông Sơn 2, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 317,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Số 137, tổ 10, ấp Phú Thuận, xã Tây Phú, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3570986,
   "Latitude": 105.1530449
 },
 {
   "STT": 318,
   "Name": "Quầy thuốc 087",
   "address": "Tổ 16, ấp Trung Bình Nhất, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 319,
   "Name": "Quầy thuốc Bích Loan",
   "address": "Số 099/6, tổ 6, ấp Tây Bình A, xã Vĩnh Chánh, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 320,
   "Name": "Quầy thuốc Bảo Châu",
   "address": "Tổ 10, ấp Hòa Đông, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3479739,
   "Latitude": 105.386309
 },
 {
   "STT": 321,
   "Name": "Quầy thuốc Thanh Hằng",
   "address": "Số 377, tổ 9, ấp Trung Phú 1, xã Vĩnh Phú, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3527541,
   "Latitude": 105.2311827
 },
 {
   "STT": 322,
   "Name": "Quầy thuốc ThanhTuyền",
   "address": "Số 26/2, TL 943, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 323,
   "Name": "Quầy thuốc Kim Thảo",
   "address": "Ấp Vĩnh Lạc, xã Vĩnh Gia, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.5073282,
   "Latitude": 104.8330368
 },
 {
   "STT": 324,
   "Name": "Quầy thuốc Thái Học",
   "address": "Tổ 15, ấp Mỹ Phó, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 325,
   "Name": "Quầy thuốc Số 0134",
   "address": "Đường Nguyễn Trãi, khóm 6, thị trấn Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4216114,
   "Latitude": 104.9980491
 },
 {
   "STT": 327,
   "Name": "Quầy thuốc Đức Hạnh",
   "address": "Tổ 4, ấp 1, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 328,
   "Name": "Quầy thuốc Bảo Trân",
   "address": "Tổ 9, ấp Mỹ Quý, xã Mỹ Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.36208,
   "Latitude": 105.4524494
 },
 {
   "STT": 329,
   "Name": "Quầy thuốc Triệu Lộc",
   "address": "Số 85, tổ 2, ấp Bình Thới, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5285357,
   "Latitude": 105.3189726
 },
 {
   "STT": 330,
   "Name": "Quầy thuốc Kim Hoàng",
   "address": "Số 63, tổ 3, ấp Hưng Thới 1, xã Phú Hưng, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6666154,
   "Latitude": 105.2897042
 },
 {
   "STT": 331,
   "Name": "Quầy thuốc Huyền Linh",
   "address": "Số 470, ấp Phú Hòa, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 332,
   "Name": "Quầy thuốc An Nhiên",
   "address": "Tổ 9, ấp Bình Hòa, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3798212,
   "Latitude": 105.3875148
 },
 {
   "STT": 333,
   "Name": "Quầy thuốc Toản Linh",
   "address": "Tổ 14, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 334,
   "Name": "Quầy thuốc Hồng Anh",
   "address": "Số 804, tổ 44, ấp Bình Phú 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 335,
   "Name": "Quầy thuốc Như Phượng",
   "address": "Tổ 1, ấp Vĩnh Thành, xã Vĩnh An, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.441856,
   "Latitude": 105.171806
 },
 {
   "STT": 336,
   "Name": "Quầy thuốc Trà Mi",
   "address": "Số 258, ấp Mỹ An, xã Mỹ An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 337,
   "Name": "Quầy thuốc Tiền Hồ 12",
   "address": "Tổ 24, ấp Mỹ Hòa, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4663644,
   "Latitude": 105.3788738
 },
 {
   "STT": 338,
   "Name": "Quầy thuốc Như Ý",
   "address": "Số 21, tổ 1, ấp Long Tân, xã Long Điền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4957171,
   "Latitude": 105.4511467
 },
 {
   "STT": 339,
   "Name": "Quầy thuốc Ngọc Viễn",
   "address": "Số 77, ấp Mỹ Hòa, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 340,
   "Name": "Quày thuốc Kim Loan",
   "address": "Tổ 21, ấp Tấn Phước, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 341,
   "Name": "Quầy thuốc Minh Minh Khôi",
   "address": "Số 78, Quốc lộ 91, tổ20, ấp Hòa Phú 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4331811,
   "Latitude": 105.398926
 },
 {
   "STT": 342,
   "Name": "Quầy thuốc PhúcTruyền",
   "address": "Tổ 6, ấp Long Thuận 2, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5505156,
   "Latitude": 105.3960289
 },
 {
   "STT": 343,
   "Name": "Quầy thuốc Bích Tuyền",
   "address": "Kios số 67, Chợ Cái Tàu Thượng, ấp Thị 1, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4340278,
   "Latitude": 105.5500278
 },
 {
   "STT": 344,
   "Name": "Quầy thuốc Hương Thủy",
   "address": "Số 61, tổ 3, ấp Hòa Phú 2, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 345,
   "Name": "Quầy thuốc Bảo Tâm",
   "address": "Tổ 25, ấp An Phú, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 346,
   "Name": "Quầy thuốc Phương Nghi",
   "address": "Số 103/51, ấp Mỹ Hòa, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 347,
   "Name": "Quầy thuốc Khỏe Đẹp",
   "address": "Tổ 18, ấp Bình Thạnh 1, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3672222,
   "Latitude": 105.4936111
 },
 {
   "STT": 348,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "Tổ 24, ấp Long Hòa 1, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 349,
   "Name": "Quầy thuốc Tuấn Kiệt",
   "address": "Số 106, tổ 4, ấp Long Thành, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 350,
   "Name": "Quầy thuốc Hoàng Khang",
   "address": "Số 747, ấp An Mỹ, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3671932,
   "Latitude": 105.494688
 },
 {
   "STT": 351,
   "Name": "Quầy thuốc Số 440",
   "address": "Số 440, tổ 14, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 353,
   "Name": "Quầy thuốc Thành Long",
   "address": "Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 354,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Ấp Tân Khánh, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 355,
   "Name": "Quầy thuốc Mỹ Hương",
   "address": "Ấp Tân Khánh, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 356,
   "Name": "Quầy thuốc Mỹ Dung",
   "address": "Ấp Hà Bao 1, xã Đa Phước, huyện An Phú, AN GIANG",
   "Longtitude": 10.7465743,
   "Latitude": 105.1199409
 },
 {
   "STT": 357,
   "Name": "Quầy thuốc Thảo Nguyên",
   "address": "Ấp An Thịnh, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 358,
   "Name": "Quầy thuốc Gia Khang",
   "address": "Ấp Phước Thọ, xã Đa Phước, huyện An Phú, AN GIANG",
   "Longtitude": 10.7173664,
   "Latitude": 105.12151
 },
 {
   "STT": 359,
   "Name": "Quầy thuốc Hạnh Phương",
   "address": "Lô số 01, 02 đường phố 3, Chợ Chi Lăng, thị trấn Chi Lăng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5309982,
   "Latitude": 105.0271211
 },
 {
   "STT": 360,
   "Name": "Quầy thuốc Văn Nam",
   "address": "Tổ 4, ấp Vĩnh Tường 1, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 361,
   "Name": "Quầy thuốc Vạn Lý",
   "address": "Số 167, tổ 9, ấp Mỹ Thành, xã Định Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3300097,
   "Latitude": 105.2932799
 },
 {
   "STT": 362,
   "Name": "Quầy thuốc Thái Long",
   "address": "Số 366, tổ 2, ấp Tân Thành, xã Vọng Thê, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.275753,
   "Latitude": 105.1317431
 },
 {
   "STT": 363,
   "Name": "Quầy thuốc Khoa",
   "address": "Tổ 35, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.46257,
   "Latitude": 105.34064
 },
 {
   "STT": 364,
   "Name": "Quầy thuốc Thu Nguyệt",
   "address": "Số 330/11, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 365,
   "Name": "Quầy thuốc Khoa Văn",
   "address": "Số 095, tổ 10, ấp Mỹ Thới, xã Định Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3562421,
   "Latitude": 105.4582046
 },
 {
   "STT": 366,
   "Name": "Quầy thuốc Thùy Trinh",
   "address": "Tổ 11, ấp Hòa Thành, xã Định Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3057775,
   "Latitude": 105.3014109
 },
 {
   "STT": 367,
   "Name": "Quầy thuốc Như Quỳnh",
   "address": "Số 467, tổ 1, ấp Hòa Tây B, xã Phú Thuận, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.278342,
   "Latitude": 105.4185227
 },
 {
   "STT": 368,
   "Name": "Quầy thuốc Việt Trinh",
   "address": "Tổ 4, ấp Mỹ Thới, xã Định Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3300097,
   "Latitude": 105.2932799
 },
 {
   "STT": 369,
   "Name": "Quầy thuốc Đồng Kiều",
   "address": "Tổ 2, ấp Vĩnh Thạnh 1, xã Lê Chánh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.733889,
   "Latitude": 105.143889
 },
 {
   "STT": 370,
   "Name": "Quầy thuốc Kim Liễu",
   "address": "Số 41, tổ 2A, đường Đinh Tiên Hoàng, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 371,
   "Name": "Quầy thuốc Ngọc Trang",
   "address": "Tổ 18, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 372,
   "Name": "Quầy thuốc Trần Thị Ngọc Hằng",
   "address": "Tổ 13, ấp Cầu Dây, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 373,
   "Name": "Quầy thuốc Ái Khoa",
   "address": "Tổ 16, ấp Vĩnh Lộc, thị trấn Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5688889,
   "Latitude": 105.2427778
 },
 {
   "STT": 374,
   "Name": "Quầy thuốc Kiều Diễm",
   "address": "Tổ 12, ấp Bình Chánh, xã Bình Long, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5671715,
   "Latitude": 105.249289
 },
 {
   "STT": 375,
   "Name": "Quầy thuốc Thọ Bích",
   "address": "Tổ 7, ấp Khánh Mỹ, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6770184,
   "Latitude": 105.1902297
 },
 {
   "STT": 376,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Tổ 28, ấp Khánh Bình, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6442088,
   "Latitude": 105.2125434
 },
 {
   "STT": 377,
   "Name": "Quầy thuốc Vịnh Tre",
   "address": "Tổ 21, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5764843,
   "Latitude": 105.2162554
 },
 {
   "STT": 378,
   "Name": "Quầy thuốc Thu Vân 2",
   "address": "Tổ 1, ấp Vĩnh Khánh 1, xã Vĩnh Tế, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.668943,
   "Latitude": 105.0623171
 },
 {
   "STT": 379,
   "Name": "Quầy thuốc Cao Khiết",
   "address": "Số 537, tổ 11, ấp Hòa An, xã Hòa Lạc, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6752813,
   "Latitude": 105.2253316
 },
 {
   "STT": 380,
   "Name": "Quầy thuốc Dương Khòn",
   "address": "Tổ 5, ấp Bình Quới 1, xã Bình Thạnh Đông, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 381,
   "Name": "Quầy thuốc Ngọc Giàu",
   "address": "Tổ 5, ấp 1, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 382,
   "Name": "Quầy thuốc Nga Tùng",
   "address": "Tổ 4, ấp 5, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 383,
   "Name": "Quầy thuốc Nam Nhung",
   "address": "Số 414, tổ 8, ấp Tân Hòa C, xã Tân An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8129361,
   "Latitude": 105.191593
 },
 {
   "STT": 384,
   "Name": "Quầy thuốc Hồng Điểm",
   "address": "Số 522, ấp Tân Hòa B, xã Tân An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7986632,
   "Latitude": 105.1668326
 },
 {
   "STT": 385,
   "Name": "Quầy thuốc Kiều Trang",
   "address": "Tổ 1, ấp Ba Xoài, xã An Cư, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5223894,
   "Latitude": 104.956956
 },
 {
   "STT": 386,
   "Name": "Quầy thuốc Thu Hồng",
   "address": "Tổ 5, ấp Ba Xoài, xã An Cư, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5223894,
   "Latitude": 104.956956
 },
 {
   "STT": 387,
   "Name": "Quầy thuốc Thùy Dung",
   "address": "Tổ 11, ấp Đây Cà Hom, xã Văn Giáo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5748743,
   "Latitude": 105.0133713
 },
 {
   "STT": 388,
   "Name": "Quầy thuốc Hồng Quyên",
   "address": "Tổ 10, khóm Xuân Phú, thị trấn Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 389,
   "Name": "Quầy thuốc Ngọc Yến",
   "address": "Tổ 7, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 390,
   "Name": "Quầy thuốc Kim Phượng",
   "address": "Ấp An Thành, xã Lương Phi, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4498527,
   "Latitude": 104.9262294
 },
 {
   "STT": 391,
   "Name": "Quầy thuốc Nga",
   "address": "Ấp Bình Quới 1, xã Bình Thạnh Đông, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5710424,
   "Latitude": 105.258784
 },
 {
   "STT": 392,
   "Name": "Quầy thuốc Hồng Sương",
   "address": "Tổ 7, ấp Hưng Lợi, xã Đào Hữu Cảnh, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4855694,
   "Latitude": 105.1245632
 },
 {
   "STT": 393,
   "Name": "Quầy thuốc Thắng Loan",
   "address": "Tổ 1, ấp Bình An, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 394,
   "Name": "Quầy thuốc Minh Minh",
   "address": "Tổ 2, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 395,
   "Name": "Quầy thuốc Trường Phát",
   "address": "Tổ 12, ấp 2, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 396,
   "Name": "Quầy thuốc Phước Nguyệt",
   "address": "Tổ 13, ấp 2, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 397,
   "Name": "Quầy thuốc Kim Si Pha",
   "address": "Tổ 1, ấp Rò Leng, xã Châu Lăng, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4430515,
   "Latitude": 104.9695519
 },
 {
   "STT": 398,
   "Name": "Quầy thuốc Phương Tùng",
   "address": "Số 718, tổ 30, ấp Vĩnh Trung, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3310367,
   "Latitude": 105.3423909
 },
 {
   "STT": 399,
   "Name": "Quầy thuốc Như Sáng",
   "address": "Số 49 Thoại Ngọc Hầu, ấp Nam Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 400,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Số 26, tổ 1, ấp Trung Bình, xã Thoại Giang, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2655716,
   "Latitude": 105.2311827
 },
 {
   "STT": 401,
   "Name": "Quầy thuốc Chanh Thone",
   "address": "Số 7, ấp An Lộc, xã Châu Lăng, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4430515,
   "Latitude": 104.9695519
 },
 {
   "STT": 402,
   "Name": "Quầy thuốc Chúc Mai",
   "address": "Tổ 34, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 403,
   "Name": "Quầy thuốc Tuấn Tín",
   "address": "Tổ 9, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 404,
   "Name": "Quầy thuốc Hoàng Huy",
   "address": "Ấp Sóc Triết, xã Cô Tô, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3814537,
   "Latitude": 105.0176239
 },
 {
   "STT": 405,
   "Name": "Quầy thuốc Dương Quyền",
   "address": "Ấp Vĩnh Hội, xã Vĩnh Hội Đông, huyện An Phú, AN GIANG",
   "Longtitude": 10.7923518,
   "Latitude": 105.0732771
 },
 {
   "STT": 406,
   "Name": "Quầy thuốc Mai",
   "address": "Ấp Búng Lớn, xã Nhơn Hội, Huyện An Phú, AN GIANG",
   "Longtitude": 10.9117665,
   "Latitude": 105.0504589
 },
 {
   "STT": 407,
   "Name": "Quầy thuốc Bé Sáu",
   "address": "Ấp Búng Lớn, xã Nhơn Hội, Huyện An Phú, AN GIANG",
   "Longtitude": 10.9117665,
   "Latitude": 105.0504589
 },
 {
   "STT": 408,
   "Name": "Quầy thuốc Thái Gia",
   "address": "Ấp Phước Thọ, xã Đa Phước, huyện An Phú, AN GIANG",
   "Longtitude": 10.7173664,
   "Latitude": 105.12151
 },
 {
   "STT": 409,
   "Name": "Quầy thuốc Thanh Hảo",
   "address": "Tổ 9, ấp Tấn Hưng, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 410,
   "Name": "Quầy thuốc Phú Thịnh",
   "address": "Chợ Mỹ Khánh, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3792765,
   "Latitude": 105.3905147
 },
 {
   "STT": 411,
   "Name": "Quầy thuốc Kim Oanh",
   "address": "Số 438, ấp An Quới, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 412,
   "Name": "Quầy thuốc Mỹ Loan",
   "address": "Tổ 2, ấp Mỹ Thuận, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756645,
   "Latitude": 105.3950939
 },
 {
   "STT": 413,
   "Name": "Quầy thuốc Kim Yến",
   "address": "Tổ 20, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 414,
   "Name": "Quầy thuốc Diễm Thúy",
   "address": "Số 558, ấp Phú Hạ 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 415,
   "Name": "Quầy thuốc Kim Thoa 9",
   "address": "Số 222, tỉnh lộ 942, ấp Hòa Trung, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5151814,
   "Latitude": 105.4868975
 },
 {
   "STT": 416,
   "Name": "Quầy thuốc Xuân Yên",
   "address": "Tổ 16, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5702578,
   "Latitude": 105.2387827
 },
 {
   "STT": 417,
   "Name": "Quầy thuốc Lê Hồ",
   "address": "Số 98, ấp Thị 1, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 418,
   "Name": "Quầy thuốc Quốc Minh",
   "address": "Số 59, ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 419,
   "Name": "Quầy thuốc Bích Trâm",
   "address": "Tổ 11, ấp An Khương, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4330203,
   "Latitude": 105.5508873
 },
 {
   "STT": 420,
   "Name": "Quầy thuốc Phương Anh",
   "address": "Ấp Hưng Tân, xã Phú Hưng, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5965525,
   "Latitude": 105.3378651
 },
 {
   "STT": 421,
   "Name": "Quầy thuốc Trọng Tường",
   "address": "Tổ 12, ấp Phú Hữu, thị trấn Chợ Vàm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.702865,
   "Latitude": 105.3306814
 },
 {
   "STT": 422,
   "Name": "Quầy thuốc Mỹ Loan",
   "address": "Ấp Bình Trung 1, xã Bình Thạnh Đông, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5796614,
   "Latitude": 105.2325225
 },
 {
   "STT": 423,
   "Name": "Quầy thuốc Mỹ Phượng",
   "address": "Ấp Phú Đức A, xã Phú Thạnh, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6972977,
   "Latitude": 105.283441
 },
 {
   "STT": 424,
   "Name": "Quầy thuốc Nguyễn Ly",
   "address": "Tổ 2, ấp Phú Trung, xã Phú Thọ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.63104,
   "Latitude": 105.3402166
 },
 {
   "STT": 425,
   "Name": "Quầy thuốc Huy Thành",
   "address": "Số 595 Chu Văn An, ấp Trung 1, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5889666,
   "Latitude": 105.3594999
 },
 {
   "STT": 426,
   "Name": "Quầy thuốc Phúc Hân",
   "address": "Chợ Mỹ Đức, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 427,
   "Name": "Quầy thuốc Hữu Tâm",
   "address": "Tổ 11, ấp Hòa Thành, xã Định Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3057775,
   "Latitude": 105.3014109
 },
 {
   "STT": 428,
   "Name": "Quầy thuốc Huy Kiều",
   "address": "Tổ 6, ấp Tân Mỹ, xã Mỹ Phú Đông, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3253502,
   "Latitude": 105.2114696
 },
 {
   "STT": 430,
   "Name": "Quầy thuốc Thu Hồng",
   "address": "Số 216, TL 943, tổ 7, ấpSơn Lập, xã Vọng Đông, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2678029,
   "Latitude": 105.1843801
 },
 {
   "STT": 431,
   "Name": "Quầy thuốc Quang Trang",
   "address": "Số 115, tổ 6, ấp Tân Mỹ, xã Mỹ Phú Đông, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3253502,
   "Latitude": 105.2114696
 },
 {
   "STT": 432,
   "Name": "Quầy thuốc Thu Dung",
   "address": "Tổ 11, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3600281,
   "Latitude": 105.3808105
 },
 {
   "STT": 433,
   "Name": "Quầy thuốc Kim Hồng",
   "address": "Ấp Tân Mỹ, xã Mỹ Phú Đông, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3253502,
   "Latitude": 105.2114696
 },
 {
   "STT": 434,
   "Name": "Quầy thuốc Linh Chi",
   "address": "Tổ 5, ấp Vĩnh Tây, xã Vĩnh Trung, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5474535,
   "Latitude": 105.0225377
 },
 {
   "STT": 435,
   "Name": "Quầy thuốc Nguyễn Phương Bình",
   "address": "Số 45 Châu Thị Tế, tổ 2, khóm Xuân Biên, thị trấn Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.3779007,
   "Latitude": 105.4455232
 },
 {
   "STT": 436,
   "Name": "Quầy thuốc Hải Đăng",
   "address": "Kios số 01-02, Chợ Núi Voi, xã Núi Voi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5385053,
   "Latitude": 105.0586639
 },
 {
   "STT": 437,
   "Name": "Quầy thuốc Trường Thanh",
   "address": "Số 5 Võ Văn Hoài, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3741487,
   "Latitude": 105.3927685
 },
 {
   "STT": 438,
   "Name": "Quầy thuốc Hoài Nam",
   "address": "Tổ 6, ấp An Hưng, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 439,
   "Name": "Quầy thuốc Thanh Tiến",
   "address": "Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 440,
   "Name": "Quầy thuốc Kim Cương",
   "address": "Ấp Tắc Trúc, xã Nhơn Hội, huyện An Phú, AN GIANG",
   "Longtitude": 10.9105865,
   "Latitude": 105.0538921
 },
 {
   "STT": 441,
   "Name": "Quầy thuốc Kim Mỹ",
   "address": "Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 442,
   "Name": "Quầy thuốc Hoa Phượng",
   "address": "Tổ 4, ấp Hòa Long 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4400607,
   "Latitude": 105.3934684
 },
 {
   "STT": 443,
   "Name": "Quầy thuốc Ngọc Phương",
   "address": "Số 361, tổ 5, ấp Hòa Long 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 444,
   "Name": "Quầy thuốc 131",
   "address": "Số 300, ấp Hà Bao 2, xã Đa Phước, huyện An Phú, AN GIANG",
   "Longtitude": 10.7461948,
   "Latitude": 105.1200482
 },
 {
   "STT": 445,
   "Name": "Quầy thuốc Phúc Duy",
   "address": "Ấp Tắc Trúc, xã Nhơn Hội, huyện An Phú, AN GIANG",
   "Longtitude": 10.9105865,
   "Latitude": 105.0538921
 },
 {
   "STT": 446,
   "Name": "Quầy thuốc Yến Thu",
   "address": "Chợ kênh 3, tổ 10, ấp Mỹ Hòa, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 447,
   "Name": "Quầy thuốc Nhã Vy",
   "address": "Tổ 16, ấp Mỹ Phó, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 448,
   "Name": "Quầy thuốc Thúy Nhi",
   "address": "Tổ 16, ấp Long An, xã Ô Long Vỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5883214,
   "Latitude": 105.1025075
 },
 {
   "STT": 449,
   "Name": "Quầy thuốc Cẩm Nhiên",
   "address": "Tổ 12, ấp Phú Hưng, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 450,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "Số 285, ấp Thượng 1, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 451,
   "Name": "Quầy thuốc Nam Sang",
   "address": "Số 245, tổ 4, ấp Long Thạnh 2, xã Long Hòa, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 452,
   "Name": "Quầy thuốc Việt Hùng",
   "address": "Tổ 6, ấp Bình Chánh 2, xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 453,
   "Name": "Quầy thuốc Phúc Nhàn",
   "address": "Số 16, tổ 1, khóm An Hòa B, thị trấn Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4942823,
   "Latitude": 104.9095979
 },
 {
   "STT": 454,
   "Name": "Quầy thuốc Mai Thi",
   "address": "Ấp Giồng Cát, xã Lương An Trà, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3916667,
   "Latitude": 104.88
 },
 {
   "STT": 455,
   "Name": "Quầy thuốc Sô Che Ta",
   "address": "Số 271, ấp An Hòa, xã Châu Lăng, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4430515,
   "Latitude": 104.9695519
 },
 {
   "STT": 456,
   "Name": "Quầy thuốc Hoàng Phú",
   "address": "Ấp Tân Bình, xã Tà Đảnh, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4027778,
   "Latitude": 105.0905556
 },
 {
   "STT": 457,
   "Name": "Quầy thuốc Châu Loan",
   "address": "Tổ 1, khóm Thới Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 458,
   "Name": "Quầy thuốc Hồng Vân",
   "address": "Tổ 8, ấp Bình Hòa 2, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3829665,
   "Latitude": 105.376542
 },
 {
   "STT": 459,
   "Name": "Quầy thuốc Phương Dung",
   "address": "Tổ 14, ấp Mỹ An 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4017004,
   "Latitude": 105.4447566
 },
 {
   "STT": 460,
   "Name": "Quầy thuốc Mỹ Huyền",
   "address": "Tổ 2, ấp Mỹ Long 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4066274,
   "Latitude": 105.427371
 },
 {
   "STT": 461,
   "Name": "Quầy thuốc Khánh Giang",
   "address": "Số 707, tổ 33, ấp Long Hòa 1, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 462,
   "Name": "Quầy thuốc Tấn Đạt",
   "address": "Số 274, ấp Thị 1, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 463,
   "Name": "Quầy thuốc Anh Thy",
   "address": "Tổ 18, ấp Mỹ Quý, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.36208,
   "Latitude": 105.4524494
 },
 {
   "STT": 464,
   "Name": "Quầy thuốc Quỳnh Khôi",
   "address": "Tổ 6, ấp Mỹ Thành, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 465,
   "Name": "Quầy thuốc 8 Sơn",
   "address": "Số 58, tổ 3, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 466,
   "Name": "Quầy thuốc Tuyết Nhung",
   "address": "Số 14, ấp Long Thạnh, 2, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4957171,
   "Latitude": 105.4511467
 },
 {
   "STT": 467,
   "Name": "Quầy thuốc Thái Bình 1",
   "address": "Tổ 21, ấp Long Mỹ 1, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 468,
   "Name": "Quầy thuốc Đức Huy",
   "address": "Tổ 15, ấp Long Bình, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 469,
   "Name": "Quầy thuốc Thái Ngân",
   "address": "Ấp Sóc Triết, xã Cô Tô, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3814537,
   "Latitude": 105.0176239
 },
 {
   "STT": 470,
   "Name": "Quầy thuốc Thanh Sơn",
   "address": "Số 1 Nguyễn Huệ, ấp Tây Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 471,
   "Name": "Quầy thuốc Ái Nhi",
   "address": "Số 641 Nguyễn Huệ, ấp Nam Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 472,
   "Name": "Quầy thuốc Lê Phước",
   "address": "Số 264, tổ 13, ấp Vĩnh Trung, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3310367,
   "Latitude": 105.3423909
 },
 {
   "STT": 474,
   "Name": "Quầy thuốc Thùy Dung",
   "address": "Chợ Tây Bình C, tổ 5, ấp Tây Bình C, xã Vĩnh Chánh, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 475,
   "Name": "Quầy thuốc Phước Sang",
   "address": "Tổ 5, ấp Tây Bình, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 476,
   "Name": "Quầy thuốc Ngọc Hạnh",
   "address": "Số 202/8, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 477,
   "Name": "Quầy thuốc Hoài Phong",
   "address": "Số 237/12, ấp Tây Bình, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 478,
   "Name": "Quầy thuốc Tuyết Anh",
   "address": "Số 93, tổ 4, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 479,
   "Name": "Quầy thuốc Siêu",
   "address": "Số 43, tổ 2, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 480,
   "Name": "Quầy thuốc Thanh Phương",
   "address": "Tổ 9, ấp Bình Hòa, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3798212,
   "Latitude": 105.3875148
 },
 {
   "STT": 481,
   "Name": "Quầy thuốc Hoàng Hiệp",
   "address": "Tổ 17, ấp Vĩnh Lợi, xã Vĩnh Hanh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3929765,
   "Latitude": 105.3014109
 },
 {
   "STT": 482,
   "Name": "Quầy thuốc Bệnh Viện Huyện Châu Thành",
   "address": "Ấp Phú An 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4464544,
   "Latitude": 105.3703234
 },
 {
   "STT": 483,
   "Name": "Quầy thuốc Thanh Phong",
   "address": "Tổ 11, ấp Bình An 2, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 484,
   "Name": "Quầy thuốc Bạch Tuyết",
   "address": "Tổ 5, ấp Thạnh Phú, xã Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4693515,
   "Latitude": 105.357029
 },
 {
   "STT": 485,
   "Name": "Quầy thuốc Lê Thanh",
   "address": "Tổ 41, ấp Bình Phú 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463034,
   "Latitude": 105.342453
 },
 {
   "STT": 486,
   "Name": "Quầy thuốc Hồng Phượng",
   "address": "Tổ 26, ấp Hòa Phú 2, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 487,
   "Name": "Quầy thuốc Mỹ Khôi",
   "address": "Số 301, tổ 14, ấp Hòa Phú 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 488,
   "Name": "Quầy thuốc Thảo Lan",
   "address": "Số 243, ấp Kiến Thuận 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 489,
   "Name": "Quầy thuốc Phú Khang",
   "address": "Số 340, ấp Long Hòa 2, xã Long Kiến, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4664526,
   "Latitude": 105.471249
 },
 {
   "STT": 491,
   "Name": "Quầy thuốc Anh Duy",
   "address": "Số 295, ấp Mỹ Hòa, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 492,
   "Name": "Quầy thuốc Mỹ Mỹ",
   "address": "Kios số 03, Chợ Mỹ Luông, ấp Thị 1, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4993584,
   "Latitude": 105.4830539
 },
 {
   "STT": 493,
   "Name": "Quầy thuốc Thu Ly",
   "address": "Số 115, tổ 5, ấp An Long, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 494,
   "Name": "Quầy thuốc Kim Anh",
   "address": "Tổ 2, ấp Long Hòa, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5508498,
   "Latitude": 105.4040296
 },
 {
   "STT": 495,
   "Name": "Quầy thuốc Diễm Bình",
   "address": "Tổ 8, ấp Bình Thắng, xã Bình Long, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5440566,
   "Latitude": 105.2253316
 },
 {
   "STT": 496,
   "Name": "Quầy thuốc HuệAnh",
   "address": "Chợ Tri Tôn, huyện TriTôn, AN GIANG",
   "Longtitude": 10.420975,
   "Latitude": 105.0026937
 },
 {
   "STT": 497,
   "Name": "Quầy thuốc Nguyễn Thị Kim Ngân",
   "address": "Tổ 1, ấp Bình Thới, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5285357,
   "Latitude": 105.3189726
 },
 {
   "STT": 498,
   "Name": "Quầy thuốc Nhất Hạnh",
   "address": "Tổ 16, ấp Long An, xã Ô Long Vỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5883214,
   "Latitude": 105.1025075
 },
 {
   "STT": 499,
   "Name": "Quầy thuốc Thảo Huỳnh",
   "address": "KDC Vĩnh Lợi 2, ấp Vĩnh Lợi 2, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.733889,
   "Latitude": 105.143889
 },
 {
   "STT": 500,
   "Name": "Quầy thuốc Thu Tươi",
   "address": "Số 267, tổ 1, ấp Hòa Long, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 502,
   "Name": "Quầy thuốc Huyền Vinh",
   "address": "Tổ 10, ấp Bình Đức, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 503,
   "Name": "Quầy thuốc Thúy Phượng",
   "address": "Số 514, tổ 1, ấp Long Mỹ 1, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 504,
   "Name": "Quầy thuốc Thanh Hiếu",
   "address": "Ấp Long Định, xã Long Kiến, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4498434,
   "Latitude": 105.4464267
 },
 {
   "STT": 505,
   "Name": "Quầy thuốc Nhẫn",
   "address": "Số 259, tổ 10, ấp Đông Châu, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 506,
   "Name": "Quầy thuốc Kim Tươi",
   "address": "Số 328, tổ 13, ấp Phú Hạ 2, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 507,
   "Name": "Quầy thuốc Nguyễn Duy",
   "address": "Số 823, tổ 18, ấp An Lương, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 508,
   "Name": "Quầy thuốc Trúc Quỳnh",
   "address": "Số 26, tổ 1, ấp Mỹ Thạnh, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5062781,
   "Latitude": 105.3464191
 },
 {
   "STT": 509,
   "Name": "Quầy thuốc An Khang",
   "address": "Tổ 11, ấp Long Hòa 1, xã Long Kiến, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 510,
   "Name": "Quầy thuốc Trọng Ni",
   "address": "Tổ 13, ấp Vĩnh Thành, xã Vĩnh An, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.5833273,
   "Latitude": 105.181027
 },
 {
   "STT": 511,
   "Name": "Quầy thuốc Tú Anh",
   "address": "Tổ 16, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4572188,
   "Latitude": 105.3324998
 },
 {
   "STT": 512,
   "Name": "Quầy thuốc Ngọc Hảo",
   "address": "Ấp Phước Thạnh, xã Phước Hưng, huyện An Phú, AN GIANG",
   "Longtitude": 10.8689629,
   "Latitude": 105.0844858
 },
 {
   "STT": 513,
   "Name": "Quầy thuốc Mạnh Hoài",
   "address": "Tổ 7, ấp Hưng Thới 2, xã Phú Hưng, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6666154,
   "Latitude": 105.2897042
 },
 {
   "STT": 514,
   "Name": "Quầy thuốc Ngân Hà",
   "address": "Số 48, tổ 2, ấp Khánh Phát, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6770184,
   "Latitude": 105.1902297
 },
 {
   "STT": 515,
   "Name": "Quầy thuốc Phú Lộc",
   "address": "Ấp Phước Thọ, xã Đa Phước, huyện An Phú, AN GIANG",
   "Longtitude": 10.7173664,
   "Latitude": 105.12151
 },
 {
   "STT": 516,
   "Name": "Quầy thuốc Út Diệu",
   "address": "Ấp Hà Bao 1, xã Đa Phước, huyện An Phú, AN GIANG",
   "Longtitude": 10.7465743,
   "Latitude": 105.1199409
 },
 {
   "STT": 517,
   "Name": "Quầy thuốc Thi Tâm",
   "address": "Ấp Phú Mỹ Hạ, xã Phú Thọ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6278414,
   "Latitude": 105.3381353
 },
 {
   "STT": 518,
   "Name": "Quầy thuốc Thanh Quyên 2",
   "address": "Số 465, tổ 8, ấp Bình Thới, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5285357,
   "Latitude": 105.3189726
 },
 {
   "STT": 519,
   "Name": "Quầy thuốc Long Duyên",
   "address": "Ấp Bình Phú 1, xã Phú Bình, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6200037,
   "Latitude": 105.2370339
 },
 {
   "STT": 520,
   "Name": "Quầy thuốc Minh Châu 1",
   "address": "Số 56 Tôn Đức Thắng, ấp Bắc Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2684671,
   "Latitude": 105.2682608
 },
 {
   "STT": 521,
   "Name": "Quầy thuốc Bình Nhi",
   "address": "Tổ 9, ấp Thạnh Hòa, xã Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4693515,
   "Latitude": 105.357029
 },
 {
   "STT": 522,
   "Name": "Quầy thuốc Chấn Hưng",
   "address": "Tổ 3, ấp Khánh Thuận, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6442088,
   "Latitude": 105.2125434
 },
 {
   "STT": 523,
   "Name": "Quầy thuốc Tú Anh 2",
   "address": "Tổ 17, ấp Phú An 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 524,
   "Name": "Quầy thuốc Anh Minh",
   "address": "Tổ 8, ấp Bình An, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5816487,
   "Latitude": 105.2320603
 },
 {
   "STT": 525,
   "Name": "Quầy thuốc Thanh Trúc",
   "address": "Tổ 9, ấp Bình Thạnh, xã Bình Chánh, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.506439,
   "Latitude": 105.222734
 },
 {
   "STT": 527,
   "Name": "Quầy thuốc Nhựt Thanh",
   "address": "Tổ 14, ấp Vĩnh Thới, xã Vĩnh Hanh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4446287,
   "Latitude": 105.248737
 },
 {
   "STT": 528,
   "Name": "Quầy thuốc Bá Đạt",
   "address": "Số 309A, tổ 14, ấp Vĩnh Trung, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3310367,
   "Latitude": 105.3423909
 },
 {
   "STT": 529,
   "Name": "Quầy thuốc Thanh Sang",
   "address": "Ấp Tân Trung, xã Tà Đảnh, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.401378,
   "Latitude": 105.0963975
 },
 {
   "STT": 530,
   "Name": "Quầy thuốc Thiện Tâm",
   "address": "Tổ 4, ấp Tân Biên, xã An Nông, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5639847,
   "Latitude": 104.9272044
 },
 {
   "STT": 531,
   "Name": "Quầy thuốc Lam Nguyễn",
   "address": "Số 171 Lê Lợi, ấp Đông Sơn 1, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 532,
   "Name": "Quầy thuốc Nhã Nhung",
   "address": "Số 367/11, khóm Xuân Hiệp, thị trấn Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 533,
   "Name": "Quầy thuốc Thu An 2",
   "address": "Kênh Sẻo Sâu Ba Dong, ấp Trung Phú 3, xã Vĩnh Phú, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3527541,
   "Latitude": 105.2311827
 },
 {
   "STT": 534,
   "Name": "Quầy thuốc Thiên Phước",
   "address": "Số 413 Nguyễn Huệ, ấp Bắc Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 535,
   "Name": "Quầy thuốc Hồng Huệ",
   "address": "Số 117, tổ 17, ấp Tấn Lợi, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 536,
   "Name": "Quầy thuốc Tiến Hùng",
   "address": "Số 87, tổ 3, ấp Vĩnh Lợi 1, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 537,
   "Name": "Quầy thuốc Ngọc Thanh",
   "address": "Số 145, tổ 6, ấp Long Quới 2, xã Long Điền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5104005,
   "Latitude": 105.4320465
 },
 {
   "STT": 538,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Ấp Bình Đông 2, xã Bình Thạnh Đông, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5867106,
   "Latitude": 105.2600331
 },
 {
   "STT": 540,
   "Name": "Quầy thuốc Dì Biển",
   "address": "Ấp Tân Bình, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 541,
   "Name": "Quầy thuốc Hồng Thúy",
   "address": "Chợ Vĩnh Nhuận, ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3730355,
   "Latitude": 105.2210788
 },
 {
   "STT": 542,
   "Name": "Quầy thuốc Thanh Trúc",
   "address": "Số 142, tổ 1, ấp Bình Thành, xã Bình Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 543,
   "Name": "Quầy thuốc Bùi Thị Phấn",
   "address": "Tổ 18, ấp Hòa Lợi 2, xã Vĩnh Lợi, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3747313,
   "Latitude": 105.3132033
 },
 {
   "STT": 544,
   "Name": "Quầy thuốc Ngọc Kim",
   "address": "Ấp Ninh Phước, xã Lương An Trà, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.38787,
   "Latitude": 104.8988342
 },
 {
   "STT": 545,
   "Name": "Quầy thuốc Nhựt Linh",
   "address": "Lô kios A8, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 546,
   "Name": "Quầy thuốc Yến Phương 2",
   "address": "Số 481, tổ 6, ấp Trung Sơn, thị trấn Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2294719,
   "Latitude": 105.1592462
 },
 {
   "STT": 547,
   "Name": "Quầy thuốc Khanh Loan 2",
   "address": "Số 349/14, ấp Phú Nhất, xã An Phú, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6400127,
   "Latitude": 104.9819662
 },
 {
   "STT": 548,
   "Name": "Quầy thuốc Phương Anh",
   "address": "Tổ 7, ấp Vĩnh An, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5833273,
   "Latitude": 105.181027
 },
 {
   "STT": 549,
   "Name": "Quầy thuốc Huỳnh Như",
   "address": "Ấp Ninh Phước, xã Lương An Trà, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.38787,
   "Latitude": 104.8988342
 },
 {
   "STT": 550,
   "Name": "Quầy thuốc 2 Trong",
   "address": "Số 149, tổ 8, ấp Hòa Phú 2, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 551,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Tổ 36, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 552,
   "Name": "Quầy thuốc Xuân Lộc 2",
   "address": "Số 235, tổ 14, ấp Đông Bình Trạch, xã Vĩnh Thành, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.35,
   "Latitude": 105.35
 },
 {
   "STT": 553,
   "Name": "Quầy thuốc Dung",
   "address": "Lô 34, chợ Bình Hòa, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463213,
   "Latitude": 105.3405155
 },
 {
   "STT": 554,
   "Name": "Quầy thuốc Ngọc Thúy",
   "address": "Số 18, tổ 1A, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 555,
   "Name": "Quầy thuốc Phước Sang",
   "address": "Tổ 3, ấp Mỹ Thuận, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4017004,
   "Latitude": 105.4447566
 },
 {
   "STT": 556,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "Số 426, tổ 9, ấp Long Hòa 1, xã Long Hòa, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 557,
   "Name": "Quầy thuốc Duy Khang",
   "address": "Chợ Châu Phú, ấp Vĩnh Hưng, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.587597,
   "Latitude": 105.22915
 },
 {
   "STT": 558,
   "Name": "Quầy thuốc Mai Anh",
   "address": "Tổ 20, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5764843,
   "Latitude": 105.2162554
 },
 {
   "STT": 560,
   "Name": "Quầy thuốc Kim Phụng",
   "address": "Tổ 13, ấp Phú Trung, xã Phú Thành, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5968965,
   "Latitude": 105.3513766
 },
 {
   "STT": 561,
   "Name": "Quầy thuốc Quốc Duy",
   "address": "Số 33, ấp Phú Đức A, xã Phú Thạnh, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6574189,
   "Latitude": 105.2545888
 },
 {
   "STT": 562,
   "Name": "Quầy thuốc Tính Em",
   "address": "Số 143, tổ 5, ấp Phú Bình, xã Phú An, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6596897,
   "Latitude": 105.3189726
 },
 {
   "STT": 563,
   "Name": "Quầy thuốc Thành Thơ",
   "address": "Số 174 Trần Hưng Đạo, ấp Thị 1, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 564,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Kios số 64, ấp Thị 1, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 565,
   "Name": "Quầy thuốc Mỹ Tuyền",
   "address": "Số 342, tổ 11, ấp Hòa Trung, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5423179,
   "Latitude": 105.3716684
 },
 {
   "STT": 566,
   "Name": "Quầy thuốc Ngọc Chum",
   "address": "Tổ 9, ấp Hiệp Hòa, xã Hiệp Xương, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5932012,
   "Latitude": 105.2799187
 },
 {
   "STT": 567,
   "Name": "Quầy thuốc Hồng Quốc",
   "address": "Tổ 5, ấp Phú Trung, xã Phú Thành, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6574189,
   "Latitude": 105.2545888
 },
 {
   "STT": 569,
   "Name": "Quầy thuốc Tú Linh",
   "address": "Tổ 6, ấp Vĩnh Tường 1, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 570,
   "Name": "Quầy thuốc Nguyên Khôi",
   "address": "Số 556, tổ 11, ấp Vĩnh Khánh 1, xã Vĩnh Tế, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.668943,
   "Latitude": 105.0623171
 },
 {
   "STT": 571,
   "Name": "Quầy thuốc Thoại An",
   "address": "Số 15 Nguyễn Trãi, khóm II, TT.Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4216114,
   "Latitude": 104.9980491
 },
 {
   "STT": 572,
   "Name": "Quầy thuốc Trí Nhân",
   "address": "Ấp Vĩnh Thành, xã Vĩnh Trường, huyện An Phú, AN GIANG",
   "Longtitude": 10.7790702,
   "Latitude": 105.1200482
 },
 {
   "STT": 573,
   "Name": "Quầy thuốc Bích Dương",
   "address": "Số 484, tổ 14, ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5285357,
   "Latitude": 105.3189726
 },
 {
   "STT": 574,
   "Name": "Quầy thuốc Hoa Ngọc",
   "address": "Tổ 10, ấp Bình Thành, xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 575,
   "Name": "Quầy thuốc Thùy Dung",
   "address": "Số 49, tổ 2, ấp Vĩnh Phú, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5833273,
   "Latitude": 105.181027
 },
 {
   "STT": 576,
   "Name": "Quầy thuốc Huỳnh Tuấn",
   "address": "Tổ 1, ấp Khánh Bình, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.677646,
   "Latitude": 105.190959
 },
 {
   "STT": 577,
   "Name": "Quầy thuốc Thanh Trí",
   "address": "Số 343, tổ 10, ấp Bình Đức, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 578,
   "Name": "Quầy thuốc Thanh Hằng",
   "address": "Số 448, tổ 7, ấp Hòa Bình 3, xã Hòa Lạc, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.667617,
   "Latitude": 105.219466
 },
 {
   "STT": 579,
   "Name": "Quầy thuốc Kim Hương",
   "address": "Tổ 4, ấp Bình Khánh, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3953269,
   "Latitude": 105.4208821
 },
 {
   "STT": 580,
   "Name": "Quầy thuốc Nga Hiệp",
   "address": "Số 397/23, ấp Bình Hòa 1, xã Mỹ Khánh, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3861185,
   "Latitude": 105.386309
 },
 {
   "STT": 581,
   "Name": "Quầy thuốc Thủy Mến",
   "address": "Tổ 9, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.477841,
   "Latitude": 105.284556
 },
 {
   "STT": 582,
   "Name": "Quầy thuốc Bích Vân",
   "address": "Tổ 25, ấp Vĩnh Quới, xã Vĩnh An, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4227169,
   "Latitude": 105.1375908
 },
 {
   "STT": 583,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Tổ 10, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 584,
   "Name": "Quầy thuốc Tân Linh",
   "address": "Chợ Bình Hòa, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463213,
   "Latitude": 105.3405155
 },
 {
   "STT": 585,
   "Name": "Quầy thuốc Quốc Khang",
   "address": "Tổ 6, ấp Thạnh Nhơn, xã Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4693515,
   "Latitude": 105.357029
 },
 {
   "STT": 586,
   "Name": "Quầy thuốc Hòa Phương",
   "address": "Số 307, tổ 12, ấp Bình Thạnh 2, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3671932,
   "Latitude": 105.494688
 },
 {
   "STT": 587,
   "Name": "Quầy thuốc Phúc Thịnh",
   "address": "Tổ 3, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 588,
   "Name": "Quầy thuốc Trần Huỳnh",
   "address": "Tổ 2, ấp Mỹ Đức, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 589,
   "Name": "Quầy thuốc Xuân Thảo",
   "address": "Số 236, ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4755556,
   "Latitude": 105.395
 },
 {
   "STT": 590,
   "Name": "Quầy thuốc Minh Sang",
   "address": "Tổ 14, ấp Vĩnh Thạnh B, xã Vĩnh Hòa, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8528973,
   "Latitude": 105.1785307
 },
 {
   "STT": 591,
   "Name": "Quầy thuốc Kim Yến",
   "address": "Số 81, tổ 3, ấp Bình Trung 2, xã Bình Thạnh Đông, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 592,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Số 441, tổ 5, ấp Tân Phú, xã Phú Lâm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.7282651,
   "Latitude": 105.2604409
 },
 {
   "STT": 593,
   "Name": "Quầy thuốc Khoa Nguyên 1",
   "address": "KDC Chợ Cầu, ấp Mỹ Tân, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4863353,
   "Latitude": 105.5036159
 },
 {
   "STT": 594,
   "Name": "Quầy thuốc Lam Thanh",
   "address": "Số 277, tổ 12, ấp An Bình, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 595,
   "Name": "Quầy thuốc Hữu Tâm 1",
   "address": "Tổ 24, ấp Tấn Phước, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 596,
   "Name": "Quầy thuốc Phương Mai",
   "address": "Số 532 Nguyễn Huệ, ấp Đông Sơn 2, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2644019,
   "Latitude": 105.2650614
 },
 {
   "STT": 597,
   "Name": "Quầy thuốc Ngọc Danh",
   "address": "Số 427, tổ 14, ấp Trung Phú 2, xã Vĩnh Phú, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3527541,
   "Latitude": 105.2311827
 },
 {
   "STT": 598,
   "Name": "Quầy thuốc Phúc Hưng",
   "address": "Tổ 8, ấp Nam Huề, xã Bình Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 599,
   "Name": "Quầy thuốc Bình An",
   "address": "Tổ 23, ấp Vĩnh Trung, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 600,
   "Name": "Quầy thuốc Gia Khang",
   "address": "Đường Liên Hoa Sơn, tổ 9, khóm An Định A, thị trấn Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4833465,
   "Latitude": 104.8980058
 },
 {
   "STT": 601,
   "Name": "Quầy thuốc Thanh Huy",
   "address": "Số 390, tổ 12, ấp Đây Cà Hôm, xã Văn Giáo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5748743,
   "Latitude": 105.0133713
 },
 {
   "STT": 602,
   "Name": "Quầy thuốc Kiều Nương",
   "address": "Tổ 8, ấp Mỹ Long 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4100841,
   "Latitude": 105.4434738
 },
 {
   "STT": 603,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "Chợ Cần Đăng, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4595756,
   "Latitude": 105.2779983
 },
 {
   "STT": 604,
   "Name": "Quầy thuốc Hoàng Anh",
   "address": "Tổ 36, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 605,
   "Name": "Quầy thuốc Thùy Dung",
   "address": "Tổ 30, ấp Trung An, xã Lê Trì, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3972321,
   "Latitude": 104.9856176
 },
 {
   "STT": 606,
   "Name": "Quầy thuốc Bích Ngân",
   "address": "Tổ 1, ấp Mỹ An 2, xã Mỹ Hòa Hưng, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.4066274,
   "Latitude": 105.427371
 },
 {
   "STT": 607,
   "Name": "Quầy thuốc Kim Chung",
   "address": "Đường Ngô Tự Lợi, khóm An Hòa A, thị trấn Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4943645,
   "Latitude": 104.9089042
 },
 {
   "STT": 608,
   "Name": "Quầy thuốc Kim Lượng",
   "address": "Tổ 1, ấp Vĩnh Hòa A, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4535251,
   "Latitude": 105.2853566
 },
 {
   "STT": 609,
   "Name": "Quầy thuốc Mỹ Châu",
   "address": "Tổ 17, ấp Bình An, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 611,
   "Name": "Quầy thuốc Nguyễn Thùy",
   "address": "Tổ 3, ấp Vĩnh Tường 2, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 612,
   "Name": "Quầy thuốc Kỳ Loan",
   "address": "Tổ 13, ấp Giồng Trà Dên, xã Tân Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8284639,
   "Latitude": 105.1686188
 },
 {
   "STT": 613,
   "Name": "Quầy thuốc Phương Trinh",
   "address": "Tổ 8, ấp Mỹ Thành, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 614,
   "Name": "Quầy thuốc Nhật Tân",
   "address": "Ấp An Hưng, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 615,
   "Name": "Quầy thuốc Hiếu Oanh 2",
   "address": "Tổ 7, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3479739,
   "Latitude": 105.386309
 },
 {
   "STT": 617,
   "Name": "Quầy thuốc Số 107",
   "address": "Tổ 10, ấp Hòa Thành, xã Định Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3066279,
   "Latitude": 105.3027873
 },
 {
   "STT": 618,
   "Name": "Quầy thuốc Phát Huy",
   "address": "Ấp Tân Khánh, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 619,
   "Name": "Quầy thuốc An Vinh",
   "address": "Số 628, tổ 22, ấp Tây Thượng, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 620,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "Số 633, tổ 19, ấp An Mỹ, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3671932,
   "Latitude": 105.494688
 },
 {
   "STT": 621,
   "Name": "Quầy thuốc Lệ Phương",
   "address": "Tổ 30, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5682157,
   "Latitude": 105.2341082
 },
 {
   "STT": 622,
   "Name": "Quầy thuốc Tú Quyên",
   "address": "Đường Thất Sơn, khóm An Hòa A, thị trấn Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4833465,
   "Latitude": 104.8980058
 },
 {
   "STT": 623,
   "Name": "Quầy thuốc Đông Phương",
   "address": "Số 790, tổ 30, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 624,
   "Name": "Quầy thuốc Quý Lộc",
   "address": "Tổ 1, khóm Xuân Biên, thị trấn Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6040828,
   "Latitude": 104.9453179
 },
 {
   "STT": 625,
   "Name": "Quầy thuốc Lan Anh",
   "address": "Số 17, ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 626,
   "Name": "Quầy thuốc Trường Ẩn",
   "address": "Số 626, tổ 25, ấp Long Định, xã Long Kiến, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4607668,
   "Latitude": 105.4504064
 },
 {
   "STT": 627,
   "Name": "Quầy thuốc Phúc Mến",
   "address": "Tổ 7, ấp Bình Hòa, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3861185,
   "Latitude": 105.386309
 },
 {
   "STT": 628,
   "Name": "Quầy thuốc Nguyên Khoa",
   "address": "Ấp Phước Lộc, xã Ô Lâm, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.355998,
   "Latitude": 104.977193
 },
 {
   "STT": 629,
   "Name": "Quầy thuốc Cẩm Loan",
   "address": "Ấp Búng Lớn, xã Nhơn Hội, huyện An Phú, AN GIANG",
   "Longtitude": 10.9117665,
   "Latitude": 105.0504589
 },
 {
   "STT": 630,
   "Name": "Quầy thuốc Hà Nương",
   "address": "Tổ 1, ấp Hòa Thạnh, xã Hòa Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4125083,
   "Latitude": 105.348246
 },
 {
   "STT": 631,
   "Name": "Quầy thuốc Ngọc Điệp",
   "address": "Ấp Tân Khánh, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 632,
   "Name": "Quầy thuốc Thúy Oanh",
   "address": "Tổ 13, ấp Đông Bình Trạch, xã Vĩnh Thành, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.35,
   "Latitude": 105.35
 },
 {
   "STT": 633,
   "Name": "Quầy thuốc Ngọc Như Ý",
   "address": "Tổ 12, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 634,
   "Name": "Quầy thuốc Thu Sương",
   "address": "Số 908, tổ 6, ấp Kinh Đào, xã Phú Thuận, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.304159,
   "Latitude": 105.4057097
 },
 {
   "STT": 635,
   "Name": "Quầy thuốc Tân",
   "address": "Số 253, tổ 5, ấp Bình Phú 1, xã Phú Bình, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5933054,
   "Latitude": 105.2493604
 },
 {
   "STT": 636,
   "Name": "Quầy thuốc Huỳnh Nga",
   "address": "Số 293, tổ 20, ấp Long Hòa 1, xã Long Kiến, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 637,
   "Name": "Quầy thuốc Mai Xuân",
   "address": "Số 22, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 638,
   "Name": "Quầy thuốc Tô Châu",
   "address": "Số 93 Trần Hưng Đạo, khóm II, TT.Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4208836,
   "Latitude": 105.0004525
 },
 {
   "STT": 639,
   "Name": "Quầy thuốc Huỳnh Phương",
   "address": "Số 315, tổ 13, ấp Vĩnh Tường, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 640,
   "Name": "Quầy thuốc Năm Thiện",
   "address": "tổ 17, ấp Mỹ Chánh, xãMỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 641,
   "Name": "Quầy thuốc Y Minh",
   "address": "Tổ 4, ấp Mỹ Lợi, xã Mỹ Phú, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 642,
   "Name": "Quầy thuốc Nhật Minh",
   "address": "Số 36 Hai Bà Trưng, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.5682157,
   "Latitude": 105.2341082
 },
 {
   "STT": 643,
   "Name": "Quầy thuốc Kiều Tiên",
   "address": "Số 31, tổ 01, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 644,
   "Name": "Quầy thuốc Ngọc Thặng",
   "address": "tổ 01, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 645,
   "Name": "Quầy thuốc Thinh Linh",
   "address": "tổ 01, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 646,
   "Name": "Quầy thuốc Ngân Hà",
   "address": "Số 31 Nguyễn Hữu Cảnh, ấp Thị II, TT.Chợ Mới, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.5443842,
   "Latitude": 105.4029715
 },
 {
   "STT": 647,
   "Name": "Quầy thuốc Hoàn Thắng",
   "address": "ấp Thị I, TT.Mỹ Luông, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4993584,
   "Latitude": 105.4830539
 },
 {
   "STT": 648,
   "Name": "Quầy thuốc Cẩm Giang",
   "address": "Số 125 Trần Hưng Đạo, TT.Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4207989,
   "Latitude": 105.0006938
 },
 {
   "STT": 649,
   "Name": "Quầy thuốc Chấn Thái",
   "address": "Số 19 Nguyễn Trãi, khóm II, Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4207863,
   "Latitude": 105.0002097
 },
 {
   "STT": 650,
   "Name": "Quầy thuốc Ngọc Khuyên",
   "address": "Số 430/08, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AnGiang",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 651,
   "Name": "Quầy thuốc Hữu Thiện",
   "address": "Số 222, tổ 01, ấp Bình Hòa 1, TT.Cái Dầu, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5682157,
   "Latitude": 105.2341082
 },
 {
   "STT": 652,
   "Name": "Quầy thuốc Kim Hương",
   "address": "Số 74, tổ 03, ấp Bình Hòa, TT.Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5796614,
   "Latitude": 105.2325225
 },
 {
   "STT": 653,
   "Name": "Quầy thuốc Hoa Phượng",
   "address": "Số 12 Lê Lợi, ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5503587,
   "Latitude": 105.4015307
 },
 {
   "STT": 654,
   "Name": "Quầy thuốc Thanh Ngọc",
   "address": "Số 804, ấp Thị 1, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4993584,
   "Latitude": 105.4830539
 },
 {
   "STT": 655,
   "Name": "Quầy thuốc Dung Cường",
   "address": "Kios 5B, đường Nguyễn Thái Học, ấp Thị, TT.Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5505245,
   "Latitude": 105.4002133
 },
 {
   "STT": 656,
   "Name": "Quầy thuốc Thanh Nguyên",
   "address": "Số 49, ấp Thị 1, thị trấnChợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 657,
   "Name": "Quầy thuốc Thanh Triền",
   "address": "Kios 5, đường Nguyễn Huệ, ấp Thị, TT.Chợ Mới, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.551219,
   "Latitude": 105.4024297
 },
 {
   "STT": 658,
   "Name": "Quầy thuốc Minh Châu",
   "address": "ấp Trung Thạnh, TT.Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5979221,
   "Latitude": 105.3541875
 },
 {
   "STT": 659,
   "Name": "Quầy thuốc Tấn",
   "address": "Ấp Tân Phú, xã Phú Lâm, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.7294198,
   "Latitude": 105.2677315
 },
 {
   "STT": 660,
   "Name": "Quầy thuốc Lâm Phương",
   "address": "Đường Nguyễn Hữu Cảnh, TTTM An Phú, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.8133892,
   "Latitude": 105.0900194
 },
 {
   "STT": 661,
   "Name": "Quầy thuốc Mỹ Ý",
   "address": "Số 543, tổ 24, ấp Hòa Long 4, TT.An Châu, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 662,
   "Name": "Quầy thuốc Nguyệt",
   "address": "Tổ 35, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.46257,
   "Latitude": 105.34064
 },
 {
   "STT": 663,
   "Name": "Quầy thuốc Phước Định",
   "address": "tổ I, ấp Hòa Phú 2, TT.An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4436218,
   "Latitude": 105.3896355
 },
 {
   "STT": 664,
   "Name": "Quầy thuốc Lý Linh",
   "address": "Chợ Cần Đăng, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4506751,
   "Latitude": 105.2981186
 },
 {
   "STT": 665,
   "Name": "Quầy thuốc Lê Trang",
   "address": "tổ 24, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 666,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "tổ 04, ấp Hòa Hưng, xã Hòa Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.407341,
   "Latitude": 105.3423909
 },
 {
   "STT": 667,
   "Name": "Quầy thuốc Bình Dân",
   "address": "ấp An Hưng, TT.An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 668,
   "Name": "Quầy thuốc Phương Mai",
   "address": "Ấp An Hòa, xã Khánh An, huyện An Phú, AN GIANG",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 669,
   "Name": "Quầy thuốc Ngọc Hà",
   "address": "Ấp An Hòa, xã Khánh An, huyện An Phú, AnGiang",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 670,
   "Name": "Quầy thuốc Sáu Kịch",
   "address": "Số 693 Nguyễn Hữu Cảnh, TT.An Phú,huyện An Phú, AN GIANG",
   "Longtitude": 10.8112501,
   "Latitude": 105.0880201
 },
 {
   "STT": 671,
   "Name": "Quầy thuốc Chín Sáng",
   "address": "ấp An Hưng, TT.AnPhú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 672,
   "Name": "Quầy thuốc Công Văn",
   "address": "ấp An Thịnh, TT.An Phú, huyện An Phú, AnGiang",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 673,
   "Name": "Quầy thuốc Sơn Trang",
   "address": "Số 301, tổ 14, ấp Hòa Phú 4, TT.An Châu, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4360562,
   "Latitude": 105.3877753
 },
 {
   "STT": 674,
   "Name": "Quầy thuốc Minh Châu",
   "address": "tổ 11, ấp Hòa Long 1, TT.An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4445504,
   "Latitude": 105.3824131
 },
 {
   "STT": 675,
   "Name": "Quầy thuốc Giang Thanh",
   "address": "Số 180 Hải Thượng Lản Ông, ấp Thượng 2, TT.Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5977244,
   "Latitude": 105.3511727
 },
 {
   "STT": 676,
   "Name": "Quầy thuốc Yến Thanh",
   "address": "ấp Thượng 2, TT.Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 677,
   "Name": "Quầy thuốc Thu Ba",
   "address": "Số 339, ấp Trung Thạnh, TT.Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5952249,
   "Latitude": 105.3511736
 },
 {
   "STT": 678,
   "Name": "Quầy thuốc Thanh Quang",
   "address": "ấp Mỹ Lương, TT.Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5974514,
   "Latitude": 105.3557475
 },
 {
   "STT": 679,
   "Name": "Quầy thuốc Thanh Hùng",
   "address": "ấp Mỹ Lương, TT.PhúMỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5974514,
   "Latitude": 105.3557475
 },
 {
   "STT": 680,
   "Name": "Quầy thuốc Phú Sĩ",
   "address": "ấp Mỹ Lương, TT.Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5974514,
   "Latitude": 105.3557475
 },
 {
   "STT": 681,
   "Name": "Quầy thuốc Trung Lâm",
   "address": "ấp Phú Xương, TT.Chợ Vàm, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.3413773
 },
 {
   "STT": 682,
   "Name": "Quầy thuốc Duyên Thanh",
   "address": "đường Tôn Đức Thắng, tổ 18, ấp Đông Sơn II, TT.Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2682486,
   "Latitude": 105.2690564
 },
 {
   "STT": 683,
   "Name": "Quầy thuốc Mai Thảo",
   "address": "Số 753 Nguyễn Huệ, tổ 6, ấp Nam Sơn, TT.Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 684,
   "Name": "Quầy thuốc Yến Phương",
   "address": "Kios số 1, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 685,
   "Name": "Quầy thuốc Sáu Hoàng",
   "address": "Số 809, tổ 5, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 686,
   "Name": "Quầy thuốc Kim Chi",
   "address": "Số 88, tổ 3, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 687,
   "Name": "Quầy thuốc Minh Quân",
   "address": "Số 545 Nguyễn Huệ, ấp Đông Sơn 2, thị trấn Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 688,
   "Name": "Quầy thuốc Thảo",
   "address": "Số 36-38 Lê Hồng Phong, thị trấn Núi Sập, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2685623,
   "Latitude": 105.2686663
 },
 {
   "STT": 689,
   "Name": "Quầy thuốc Trà Mi",
   "address": "Số 376, tổ 21, ấp Hòa Phú, xã Định Thành, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 690,
   "Name": "Quầy thuốc Gia An",
   "address": "tổ 23, ấp Bình Phú 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 691,
   "Name": "Quầy thuốc Thảo Hạnh",
   "address": "Tổ 13, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.457297,
   "Latitude": 105.2947414
 },
 {
   "STT": 692,
   "Name": "Quầy thuốc Thiện Toàn",
   "address": "Ấp Phú Mỹ Thượng, xã Phú Thọ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.63104,
   "Latitude": 105.3402166
 },
 {
   "STT": 693,
   "Name": "Quầy thuốc Ngọc Loan",
   "address": "tổ 11, ấp Vĩnh Thọ, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4204614,
   "Latitude": 105.1843801
 },
 {
   "STT": 694,
   "Name": "Quầy thuốc Bích Hạnh",
   "address": "ấp An Hòa,xã Khánh An, huyện An Phú, AnGiang",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 695,
   "Name": "Quầy thuốc Bế Dung",
   "address": "ấp Đồng Ky, xã Quốc Thái, huyện An Phú, AnGiang",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 698,
   "Name": "Quầy thuốc Thanh Hồng",
   "address": "tổ 15, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 699,
   "Name": "Quầy thuốc Kim Ngọc",
   "address": "tổ 11, ấp Khánh Hòa, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6770184,
   "Latitude": 105.1902297
 },
 {
   "STT": 700,
   "Name": "Quầy thuốc Út Thêm",
   "address": "ấp Hậu Giang 1, xã Tân Hòa, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5690667,
   "Latitude": 105.3277541
 },
 {
   "STT": 701,
   "Name": "Quầy thuốc Minh Thành",
   "address": "Số 26 Chu Văn An, ấp Mỹ Lương, thị trấn Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5974514,
   "Latitude": 105.3557475
 },
 {
   "STT": 702,
   "Name": "Quầy thuốc Nguyên Đán",
   "address": "Số 46, tổ 6, ấp Tân Phú, xã Phú Lâm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.7226201,
   "Latitude": 105.277371
 },
 {
   "STT": 703,
   "Name": "Quầy thuốc 101A",
   "address": "Số 101A, ấp Long Hòa, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5519586,
   "Latitude": 105.4048437
 },
 {
   "STT": 704,
   "Name": "Quầy thuốc Bình An",
   "address": "Số 159, ấp Mỹ An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4661042,
   "Latitude": 105.3777645
 },
 {
   "STT": 705,
   "Name": "Quầy thuốc Thanh Nhàn",
   "address": "Số 338, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 706,
   "Name": "Quầy thuốc Quốc Hằng",
   "address": "Số 370, tổ 03, ấp Mỹ Tân, xã Mỹ Hội Đông, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 707,
   "Name": "Quầy thuốc Nhơn Thành",
   "address": "Số 421, ấp Mỹ Hội, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 708,
   "Name": "Quầy thuốc Mỹ Thành",
   "address": "Số 426, ấp Mỹ Thành, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 709,
   "Name": "Quầy thuốc Huy Thoa",
   "address": "tổ 17, khóm Xuân Hòa, TT.Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6040223,
   "Latitude": 104.9459438
 },
 {
   "STT": 710,
   "Name": "Quầy thuốc Quế Phương",
   "address": "Số 218/9, khóm Thới Hòa, TT.Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 711,
   "Name": "Quầy thuốc Thanh Mai",
   "address": "Số 710B/16, khóm Xuân Hòa, TT.Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 712,
   "Name": "Quầy thuốc Thế Công",
   "address": "Số 178 Trà Sư (QL91),khóm Hòa Hưng, TT.Nhà Bàng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6206274,
   "Latitude": 105.0018163
 },
 {
   "STT": 713,
   "Name": "Quầy thuốc số 0216",
   "address": "Chợ Châu Lăng, xã Châu Lăng, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4370905,
   "Latitude": 105.0001949
 },
 {
   "STT": 714,
   "Name": "Quầy thuốc Hồng Đào",
   "address": "khóm An Định A, TT.Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4978942,
   "Latitude": 104.9082247
 },
 {
   "STT": 715,
   "Name": "Quầy thuốc Hoàng Diễm",
   "address": "Ấp An Lộc, xã Châu Lăng, huyện Tri Tôn,AN GIANG",
   "Longtitude": 10.4452778,
   "Latitude": 104.9916667
 },
 {
   "STT": 716,
   "Name": "Quầy thuốc Bạch Văn Tươi",
   "address": "ấp An Hòa, xã Châu Lăng, huyện Tri Tôn,AN GIANG",
   "Longtitude": 10.4430515,
   "Latitude": 104.9695519
 },
 {
   "STT": 718,
   "Name": "Quầy thuốc Phạm Hữu Khôi",
   "address": "Số 383 Nguyễn Huệ, ấp Bắc Sơn, TT.Núi Sập, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 719,
   "Name": "Quầy thuốc Thảo Vy",
   "address": "Tổ 2, ấp Hòa Thành, xã Định Thành, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3119397,
   "Latitude": 105.3167945
 },
 {
   "STT": 720,
   "Name": "Quầy thuốc Vĩnh An",
   "address": "Số 293/10, ấp Phú Hữu, TT.Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3479739,
   "Latitude": 105.386309
 },
 {
   "STT": 721,
   "Name": "Quầy thuốc Huy Hùng",
   "address": "Số 703 Nguyễn Huệ, ấp Nam Sơn, thị trấn Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 722,
   "Name": "Quầy thuốc Mỹ Linh",
   "address": "Số 09/1 Trần Phú, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 723,
   "Name": "Quầy thuốc Phúc Thúy",
   "address": "Ấp Tân Bình, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 724,
   "Name": "Quầy thuốc Quang Phục",
   "address": "Ấp An Hòa, xã Khánh An, huyện An Phú, AnGiang",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 725,
   "Name": "Quầy thuốc Thanh Khanh",
   "address": "ấp An Hưng, TT.An Phú, huyện An Phú, AnGiang",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 727,
   "Name": "Quầy thuốc Ba Nghìn",
   "address": "Ấp Bình Phú 1, xã Phú Bình, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6200037,
   "Latitude": 105.2370339
 },
 {
   "STT": 728,
   "Name": "Quầy thuốc Quốc Lộc",
   "address": "ấp Tân Bình, TT.Long Bình, huyện An Phú, AnGiang",
   "Longtitude": 10.9435856,
   "Latitude": 105.0794954
 },
 {
   "STT": 729,
   "Name": "Quầy thuốc Phước Thành 2",
   "address": "ấp Trung 3, TT.Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.577128,
   "Latitude": 105.35755
 },
 {
   "STT": 730,
   "Name": "Quầy thuốc Ca Tơ",
   "address": "ấp Phú Đông, xã Phú Xuân, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6334424,
   "Latitude": 105.281642
 },
 {
   "STT": 731,
   "Name": "Quầy thuốc Thanh Hải",
   "address": "ấp Thị I, TT.Mỹ Luông,huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 732,
   "Name": "Quầy thuốc Công Bằng",
   "address": "Số 262/9, ấp Phú Hữu, TT.Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3479739,
   "Latitude": 105.386309
 },
 {
   "STT": 734,
   "Name": "Quầy thuốc Hồng Hương",
   "address": "Số 170, tổ 6, ấp Tân Hưng, xã Phú Hưng, huyện Phú Tân, AnGiang",
   "Longtitude": 10.6666154,
   "Latitude": 105.2897042
 },
 {
   "STT": 735,
   "Name": "Quầy thuốc Hưng Cường",
   "address": "ấp Tân Hưng, xã PhúHưng, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5940885,
   "Latitude": 105.3189726
 },
 {
   "STT": 736,
   "Name": "Quầy thuốc Liêm Chính",
   "address": "ấp Phú Mỹ Thượng,xã Phú Thọ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.63104,
   "Latitude": 105.3402166
 },
 {
   "STT": 737,
   "Name": "Quầy thuốc Hữu Nghị",
   "address": "ấp Phú Xương, TT.Chợ Vàm, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.3413773
 },
 {
   "STT": 739,
   "Name": "Quầy thuốc Phương My",
   "address": "tổ 13, ấp An Quới, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 740,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Số 184, ấp An Bình, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 741,
   "Name": "Quầy thuốc Sáu Biên",
   "address": "Tổ 22, ấp Thị, thị trấn Chợ Mới, huyện ChợMới, AN GIANG",
   "Longtitude": 10.5499849,
   "Latitude": 105.4024994
 },
 {
   "STT": 742,
   "Name": "Quầy thuốc Diễm Trinh",
   "address": "Số 242, tổ 05, ấp Long Hòa, xã Long Giang, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 743,
   "Name": "Quầy thuốc Vinh Quang",
   "address": "ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4755556,
   "Latitude": 105.395
 },
 {
   "STT": 744,
   "Name": "Quầy thuốc Ngọc Huyền",
   "address": "Số 359, tổ 2, ấp Mỹ Phú, xã Mỹ An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 745,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "ấp Thị, xã Hội An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 746,
   "Name": "Quầy thuốc Hoàng Oanh",
   "address": "ấp An Lương, xã Hòa Bình, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.3893421,
   "Latitude": 105.4706604
 },
 {
   "STT": 747,
   "Name": "Quầy thuốc Ngoan Phúc",
   "address": "Số 303, ấp Mỹ An, xã Mỹ An, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 748,
   "Name": "Quầy thuốc Hữu Thành",
   "address": "TTTM TT.Phú Mỹ,đường Nguyễn Văn Cừ, TT.Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5860336,
   "Latitude": 105.3568533
 },
 {
   "STT": 749,
   "Name": "Quầy thuốc Ngọc Ngoan",
   "address": "tổ 01, ấp An Bình, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3897222,
   "Latitude": 105.4711111
 },
 {
   "STT": 750,
   "Name": "Quầy thuốc Xuân Đào",
   "address": "Số 146, ấp Long Phú, xãLong Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 751,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "tổ 01, ấp Hòa Thượng, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5423179,
   "Latitude": 105.3716684
 },
 {
   "STT": 752,
   "Name": "Quầy thuốc Ngọc phúc",
   "address": "ấp Hòa Thượng, xã Kiến An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5512579,
   "Latitude": 105.3917547
 },
 {
   "STT": 753,
   "Name": "Quầy thuốc Văn Bằng",
   "address": "Số 35, lô 3, ấp Thị 1, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 754,
   "Name": "Quầy thuốc Yến Nhi",
   "address": "ấp Mỹ Phú, xã Mỹ An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5083333,
   "Latitude": 105.5063889
 },
 {
   "STT": 755,
   "Name": "Quầy thuốc Thu Dung",
   "address": "Số 541, ấp An Thuận,xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 757,
   "Name": "Quầy thuốc Hạnh Phúc",
   "address": "Số 23 Nguyễn Trãi, khóm II, TT.Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4212875,
   "Latitude": 105.0000058
 },
 {
   "STT": 758,
   "Name": "Quầy thuốc Vĩnh Tâm",
   "address": "Số 104 Trần Hưng Đạo, khóm VI, TT.Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4224013,
   "Latitude": 105.0008098
 },
 {
   "STT": 759,
   "Name": "Quầy thuốc Minh Uyên",
   "address": "ấp An Hưng, TT.An Phú, huyện An Phú, AnGiang",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 760,
   "Name": "Quầy thuốc Hòa Nhung",
   "address": "Số 364, tổ 8, ấp Hòa Hạ, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5563083,
   "Latitude": 105.3950728
 },
 {
   "STT": 761,
   "Name": "Quầy thuốc Nhật Mỹ",
   "address": "Số 268, tổ 12, ấp Hòa Long 4, TT.An Châu, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 762,
   "Name": "Quầy thuốc Hồng Chen",
   "address": "ấp Phú Mỹ Hạ, xã Phú Thọ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.6278414,
   "Latitude": 105.3381353
 },
 {
   "STT": 763,
   "Name": "Quầy thuốc Kim Hòa",
   "address": "Số 8, ấp Mỹ Dức, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 764,
   "Name": "Quầy thuốc Thiên Vy",
   "address": "Số 1502, tổ 3, ấp Hòa Tây B, xã Phú Thuận, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.278342,
   "Latitude": 105.4185227
 },
 {
   "STT": 765,
   "Name": "Quầy thuốc Tấn Bửu",
   "address": "tổ 3, ấp An Hòa, xã AnHảo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.495795,
   "Latitude": 105.025549
 },
 {
   "STT": 766,
   "Name": "Quầy thuốc Đúng Hà",
   "address": "Số 17/1, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 768,
   "Name": "Quầy thuốc Tuyết Ngọc",
   "address": "ấp Sóc Triết, xã Cô Tô, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3814537,
   "Latitude": 105.0176239
 },
 {
   "STT": 769,
   "Name": "Quầy thuốc Chau Pháth",
   "address": "Số 136, ấp Sóc Triết, xã Cô Tô, huyện Tri Tôn,AN GIANG",
   "Longtitude": 10.3501846,
   "Latitude": 105.0384458
 },
 {
   "STT": 770,
   "Name": "Quầy thuốc Khánh Giang",
   "address": "ấp Giồng Cát, xã Lương An Trà, huyện Tri Tôn,AN GIANG",
   "Longtitude": 10.3916667,
   "Latitude": 104.88
 },
 {
   "STT": 771,
   "Name": "Quầy thuốc Hoàng Anh",
   "address": "Chợ Ba Chúc, TT.Ba Chúc, huyện Tri Tôn,AN GIANG",
   "Longtitude": 10.4932001,
   "Latitude": 104.9098447
 },
 {
   "STT": 772,
   "Name": "Quầy thuốc Hồ Phong",
   "address": "Tổ 6, ấp Vĩnh Phú, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.576247,
   "Latitude": 105.2140903
 },
 {
   "STT": 773,
   "Name": "Quầy thuốc Hoài Nam",
   "address": "Số 175/8, QL 91, ấpVĩnh Quới, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.584284,
   "Latitude": 105.2304821
 },
 {
   "STT": 774,
   "Name": "Quầy thuốc Ngọc Trinh",
   "address": "Số 306, ấp Bình Hưng 1, xã Bình Mỹ, huyệnChâu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 775,
   "Name": "Quầy thuốc Thu Trâm",
   "address": "tổ 14, ấp Bình Minh, xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 776,
   "Name": "Quầy thuốc Tám Xa",
   "address": "tổ 09, ấp Bình Hưng 1, xã Bình Mỹ, huyệnChâu Phú, AN GIANG",
   "Longtitude": 10.5213483,
   "Latitude": 105.3071753
 },
 {
   "STT": 777,
   "Name": "Quầy thuốc Tài Nguyên",
   "address": "Số 084, tổ 03, ấp Bình Hòa, TT.Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5682157,
   "Latitude": 105.2341082
 },
 {
   "STT": 778,
   "Name": "Quầy thuốc Thanh Thủy",
   "address": "Số 136, tổ 01, ấp Bình Hòa, TT.Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5682157,
   "Latitude": 105.2341082
 },
 {
   "STT": 779,
   "Name": "Quầy thuốc Kim Quan",
   "address": "Số 409, ấp Thị, xã MỹHiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 780,
   "Name": "Quầy thuốc Thanh Hải MỹHiệp",
   "address": "tổ 12, ấp Thị, xã Mỹ Hiệp, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.5083333,
   "Latitude": 105.5413889
 },
 {
   "STT": 781,
   "Name": "Quầy thuốc số 0160",
   "address": "ấp Trung, xã Mỹ Hiệp, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 782,
   "Name": "Quầy thuốc Mỹ Duyên",
   "address": "Số 264, tổ 10, ấp Đông Châu, xã Mỹ Hiệp, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 783,
   "Name": "Quầy thuốc Văn Khuyến",
   "address": "Số 438, ấp Trung Châu, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 784,
   "Name": "Quầy thuốc Huyền Trân 1",
   "address": "Số 459, ấp Tấn Lợi, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 785,
   "Name": "Quầy thuốc Vy Nhân",
   "address": "Số 237, tổ 15, ấp Bình Trung, xã Bình Phước Xuân, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.453799,
   "Latitude": 105.552789
 },
 {
   "STT": 786,
   "Name": "Quầy thuốc Diệu Hiền",
   "address": "Tổ 17, ấp Bình Trung, xã Bình Phước Xuân, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.453799,
   "Latitude": 105.552789
 },
 {
   "STT": 787,
   "Name": "Quầy thuốc Tần Hường",
   "address": "Số 412, tổ 14, ấp Thị, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 788,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "Số 454/5 Trà Sư, khóm Hòa Thuận, TT.Nhà Bàng, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.6194704,
   "Latitude": 105.0006289
 },
 {
   "STT": 789,
   "Name": "Quầy thuốc Thành Luân",
   "address": "tổ 8, khóm Thới Hòa, TT.Nhà Bàng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 790,
   "Name": "Quầy thuốc Hạnh An",
   "address": "tổ 1, ấp Núi Két, xãThới Sơn, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6091506,
   "Latitude": 105.0015736
 },
 {
   "STT": 791,
   "Name": "Quầy thuốc Tuấn Khanh",
   "address": "Số 96/3, ấp Voi I, xãNúi Voi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5421786,
   "Latitude": 105.0372692
 },
 {
   "STT": 792,
   "Name": "Quầy thuốc Thông Lệ",
   "address": "Số 343/7, khóm Xuân Hòa, TT.Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 793,
   "Name": "Quầy thuốc Sa Bone",
   "address": "Số 834/06, ấp An Hòa, xã An Hảo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.4869761,
   "Latitude": 105.0221367
 },
 {
   "STT": 794,
   "Name": "Quầy thuốc Khanh Loan",
   "address": "Số 473/21, ấp Tây Hưng, xã Nhơn Hưng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6223746,
   "Latitude": 104.9929208
 },
 {
   "STT": 795,
   "Name": "QUẦY THUỐC XUÂN NHÃ",
   "address": "Số 52/3 Trần Phú, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 796,
   "Name": "Quầy thuốc Lâm Hoàng Hận",
   "address": "tổ 7, ấp Tân Hiệp B,TT.Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 797,
   "Name": "Quầy thuốc Kim Loan",
   "address": "Số 513, tổ 17, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 798,
   "Name": "Quầy thuốc Ngọc Điệp",
   "address": "Số 346, tổ 17, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 799,
   "Name": "Quầy thuốc Đặng Phúc",
   "address": "Số 68, tổ 2, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 800,
   "Name": "Quầy thuốc Đức Hạnh",
   "address": "Số 307, tổ 06, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 801,
   "Name": "Quầy thuốc Xuân Mai",
   "address": "Số 157, tổ 01, ấp Tân Hiệp A, TT.Óc Eo, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 802,
   "Name": "Quầy thuốc Minh Đức",
   "address": "Số 550 Nguyễn Huệ, ấp Đông Sơn II, TT.Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 803,
   "Name": "Quầy thuốc Phúc Sang",
   "address": "Tổ 8, ấp Hòa Phú 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 804,
   "Name": "Quầy thuốc Ngọc Hà",
   "address": "Tổ 8, ấp Hòa Phú 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 805,
   "Name": "Quầy thuốc Thành Trang",
   "address": "Số 01, tổ 01, ấp Hòa Phú 2, TT.An Châu, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4360562,
   "Latitude": 105.3877753
 },
 {
   "STT": 806,
   "Name": "Quầy thuốc Tuấn Kiệt",
   "address": "tổ 11, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 807,
   "Name": "Quầy thuốc0328 Hồng Ngọc",
   "address": "Số 686, tổ 1, ấp Bình Phú 2, xã Bình Hòa, huyện Châu Thành, AnGiang",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 808,
   "Name": "Quầy thuốc Quang Vinh 1",
   "address": "Số 384, tổ 22, ấp Đông Bình Nhất, xã Vĩnh Thành, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3619086,
   "Latitude": 105.3696209
 },
 {
   "STT": 809,
   "Name": "Quầy thuốc Út Chức",
   "address": "Chợ An Châu, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4443713,
   "Latitude": 105.3901523
 },
 {
   "STT": 810,
   "Name": "Quầy thuốc Anh Thư",
   "address": "tổ 44, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4539693,
   "Latitude": 105.3430688
 },
 {
   "STT": 811,
   "Name": "Quầy thuốc Thanh Tùng 1",
   "address": "Số 534, tổ 9, ấp Mỹ Hòa, xã Nhơn Mỹ, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4756645,
   "Latitude": 105.3950939
 },
 {
   "STT": 812,
   "Name": "Quầy thuốc Hữu Phước",
   "address": "tổ 1, ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4779188,
   "Latitude": 105.3932834
 },
 {
   "STT": 813,
   "Name": "Quầy thuốc Quốc Tuấn",
   "address": "Số 11, tổ 01, ấp Mỹ Đức, xã Mỹ Hội Đông, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 814,
   "Name": "Quầy thuốc Trường Sơn",
   "address": "Số 98, ấp Mỹ Tân, xã Mỹ Hội Đông, huyệnChợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 815,
   "Name": "Quầy thuốc Phúc Khang",
   "address": "ấp Mỹ Hội, xã Mỹ Hội Đông, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4904783,
   "Latitude": 105.356108
 },
 {
   "STT": 817,
   "Name": "Quầy thuốc Sang",
   "address": "ấp Kiến Hưng I, xã Kiến Thành, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4825247,
   "Latitude": 105.4771084
 },
 {
   "STT": 818,
   "Name": "Quầy thuốc Thanh Long",
   "address": "Kios 8A1, ấp Thị I,TT.Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 819,
   "Name": "Quầy thuốc Hồng Yến",
   "address": "Số 84, ấp Long Định, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 820,
   "Name": "Quầy thuốc Đăng Dương",
   "address": "Số 510, ấp Long Bình, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 821,
   "Name": "Quầy thuốc số 144",
   "address": "Số 144, ấp Mỹ Quí, TT.Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5073465,
   "Latitude": 105.4912876
 },
 {
   "STT": 822,
   "Name": "Quầy thuốc Bảo Vy 1",
   "address": "Số 35, ấp Long Định, xã Long Điền A, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 823,
   "Name": "Quầy thuốc Quan Toàn",
   "address": "Kios 5, đường Nguyễn Thái Học, ấp Thị, TT.Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5505245,
   "Latitude": 105.4002133
 },
 {
   "STT": 825,
   "Name": "Quầy thuốc Ngọc Điệp",
   "address": "Số 200 Trần Phú, tổ 33, khóm Long Hưng, P.Long Châu, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7986834,
   "Latitude": 105.2352023
 },
 {
   "STT": 826,
   "Name": "Quầy thuốc Minh Cường",
   "address": "Số 55 Nguyễn Văn Cừ, khóm Long Thạnh A, P.Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7980766,
   "Latitude": 105.2492326
 },
 {
   "STT": 827,
   "Name": "Quầy thuốc Mỹ Dung",
   "address": "Tổ 17, ấp 2, xã Vĩnh Xương, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 828,
   "Name": "Quầy thuốc Quỳnh Trang",
   "address": "tổ 07, ấp 3, xã Vĩnh Xương, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 829,
   "Name": "Quầy thuốc Gia Khang",
   "address": "tổ 03, ấp 2, xã VĩnhXương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 830,
   "Name": "Quầy thuốc Minh Hồng",
   "address": "Tổ 14, ấp Vĩnh Tường 1, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7196256,
   "Latitude": 105.1319289
 },
 {
   "STT": 831,
   "Name": "Quầy thuốc Huyền Trâm",
   "address": "tổ 03, ấp 1, xã Vĩnh Xương, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 832,
   "Name": "Quầy thuốc Kim Phượng",
   "address": "Chợ Vĩnh Trung, ấp Vĩnh Tâm, xã Vĩnh Trung, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.5501325,
   "Latitude": 105.0224303
 },
 {
   "STT": 833,
   "Name": "Quầy thuốc Hai Vân",
   "address": "Số 171/5, khóm Hòa Thuận, TT.Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 834,
   "Name": "Quầy thuốc Thịnh Đạt",
   "address": "Số 500/10, khóm Xuân Hòa, TT.Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 835,
   "Name": "Quầy thuốc Đại Cát",
   "address": "Tổ 19, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6186933,
   "Latitude": 104.949899
 },
 {
   "STT": 836,
   "Name": "Quầy thuốc Huỳnh La Rưa",
   "address": "Số 505/10, khóm Xuân Hòa, TT.Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 837,
   "Name": "Quầy thuốc Võ Sang",
   "address": "tổ 15, khóm Xuân Biên, TT.Tịnh Biên, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6186839,
   "Latitude": 104.9495987
 },
 {
   "STT": 838,
   "Name": "Quầy thuốc Hoài Thu",
   "address": "tổ 9, khóm Xuân Hòa, TT.Tịnh Biên, huyệnTịnh Biên, AN GIANG",
   "Longtitude": 10.6182331,
   "Latitude": 104.9512973
 },
 {
   "STT": 839,
   "Name": "Quầy thuốc Trân Trang",
   "address": "Số 192/6, ấp Núi Voi, xã Núi Voi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5385053,
   "Latitude": 105.0586639
 },
 {
   "STT": 840,
   "Name": "Quầy thuốc Phương Anh",
   "address": "tổ 1, ấp Vĩnh Tâm, xã Vĩnh Trung, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5489517,
   "Latitude": 105.0163742
 },
 {
   "STT": 841,
   "Name": "Quầy thuốc Minh Phụng",
   "address": "Số 192/7, khóm Trà Sư, TT.Nhà Bàng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6248148,
   "Latitude": 105.0067314
 },
 {
   "STT": 842,
   "Name": "Quầy thuốc Hai Nếu",
   "address": "Số 162C, tổ 11, ấp Phú Thuận, xã Tây Phú, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3570986,
   "Latitude": 105.1530449
 },
 {
   "STT": 843,
   "Name": "Quầy thuốc Hồng Điệp",
   "address": "Chợ Tây Phú, ấp Phú Thuận, xã Tây Phú, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.37394,
   "Latitude": 105.155577
 },
 {
   "STT": 844,
   "Name": "Quầy thuốc Ngô Long Định",
   "address": "tổ 18, ấp Sơn Hiệp, xãAn Bình, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3125096,
   "Latitude": 105.1609838
 },
 {
   "STT": 845,
   "Name": "Quầy thuốc Công Nghĩa",
   "address": "Số 116, tổ 4, ấp Sơn Tân, xã Vọng Đông, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2678029,
   "Latitude": 105.1843801
 },
 {
   "STT": 846,
   "Name": "Quầy thuốc Phương Yến",
   "address": "Số 277, tổ 10, ấp Nam Huề, xã Bình Thành, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 847,
   "Name": "Quầy thuốc Thanh Ngân",
   "address": "Số 149, tổ 7, ấp Hòa Tân, xã Định Thành, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3057775,
   "Latitude": 105.3014109
 },
 {
   "STT": 848,
   "Name": "Quầy thuốc Ngọc Mai",
   "address": "Ấp Phú Thạnh, xã PhúHữu, huyện An Phú, AN GIANG",
   "Longtitude": 10.8840525,
   "Latitude": 105.1142011
 },
 {
   "STT": 849,
   "Name": "Quầy thuốc Thùy Dương",
   "address": "ấp Đồng Ky, xã QuốcThái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 850,
   "Name": "Quầy thuốc Minh Hiển",
   "address": "Ấp Quốc Hưng, xã Quốc Thái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8943447,
   "Latitude": 105.077809
 },
 {
   "STT": 851,
   "Name": "Quầy thuốc Vũ Sơn",
   "address": "ấp An Hưng, TT.AnPhú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 852,
   "Name": "Quầy thuốc Bảo Vy 2",
   "address": "Ấp Vĩnh Hội, xã Vĩnh Hội Đông, huyện An Phú, AN GIANG",
   "Longtitude": 10.7923518,
   "Latitude": 105.0732771
 },
 {
   "STT": 853,
   "Name": "Quầy thuốc Xuân Trang",
   "address": "Ấp Thượng 1, TT.Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5993244,
   "Latitude": 105.3542924
 },
 {
   "STT": 854,
   "Name": "Quầy thuốc Vạn Hòa",
   "address": "Ấp Trung Hòa, xã Tân Trung, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.5770729,
   "Latitude": 105.357164
 },
 {
   "STT": 855,
   "Name": "Quầy thuốc Tỷ Đô",
   "address": "Ấp Hòa Hưng 1, xã Hòa Lạc, huyện Phú Tân, AnGiang",
   "Longtitude": 10.6707297,
   "Latitude": 105.2246475
 },
 {
   "STT": 856,
   "Name": "Quầy thuốc Long Ẩn",
   "address": "Ấp Hòa Bình 3, xã HòaLạc, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6666154,
   "Latitude": 105.2897042
 },
 {
   "STT": 857,
   "Name": "Quầy thuốc Minh Thư",
   "address": "Ấp Phú Vinh, TT.Chợ Vàm, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.703117,
   "Latitude": 105.3432655
 },
 {
   "STT": 858,
   "Name": "Quầy thuốc Thúy Kiều",
   "address": "Tổ 2, Mỹ An 1, xã Mỹ Hòa Hưng, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.4066274,
   "Latitude": 105.427371
 },
 {
   "STT": 859,
   "Name": "Quầy thuốc Hòa Hưng",
   "address": "Số 328/4, khóm Hòa Hưng, thị trấn Nhà Bàng, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.624402,
   "Latitude": 105.007528
 },
 {
   "STT": 860,
   "Name": "Quầy thuốc Vũ Hiền",
   "address": "Tổ 22, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 861,
   "Name": "Quầy thuốc Triều Huệ",
   "address": "Tổ 01, ấp Đông Hưng, xã Nhơn Hưng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6306164,
   "Latitude": 104.995961
 },
 {
   "STT": 862,
   "Name": "Quầy thuốc Thu Tâm",
   "address": "Số 8/1, ấp Mằng Rò, xãVăn Giáo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5859787,
   "Latitude": 105.0382076
 },
 {
   "STT": 863,
   "Name": "Quầy thuốc Thanh Phong",
   "address": "Tổ 12, ấp Tân Long, xã Tân Lợi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5088656,
   "Latitude": 105.0498966
 },
 {
   "STT": 864,
   "Name": "Quầy thuốc Thanh Điền",
   "address": "Tổ 8, ấp Tân Long, xã Tân Lợi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5088656,
   "Latitude": 105.0498966
 },
 {
   "STT": 865,
   "Name": "Quầy thuốc Phương Trinh",
   "address": "Số 179/5, khóm Hòa Thuận, thị trấn Nhà Bàng, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 866,
   "Name": "Quầy thuốc Như Linh",
   "address": "Số 056/2, ấp Mằng Rò, xã Văn Giáo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5859787,
   "Latitude": 105.0382076
 },
 {
   "STT": 867,
   "Name": "Quầy thuốc Ngọc Hùng",
   "address": "Tổ 1, khóm I, thị trấn Chi Lăng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5309361,
   "Latitude": 105.023794
 },
 {
   "STT": 868,
   "Name": "Quầy thuốc Như Bình",
   "address": "Số 31/10, ấp Tân Thành, xã Tân Lập, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.4634686,
   "Latitude": 105.0849687
 },
 {
   "STT": 869,
   "Name": "Quầy thuốc Ngọc Điệp",
   "address": "Số 194/01, ấp An Hòa, xã An Hảo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.4869761,
   "Latitude": 105.0221367
 },
 {
   "STT": 870,
   "Name": "Quầy thuốc Minh Triết",
   "address": "Số 29/10, khóm I, thị trấn Chi Lăng, huyệnTịnh Biên, AN GIANG",
   "Longtitude": 10.5318577,
   "Latitude": 105.0265195
 },
 {
   "STT": 871,
   "Name": "Quầy thuốc Kim Hà",
   "address": "Tổ 8, ấp Mằng Rò, xã Văn Giáo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5859787,
   "Latitude": 105.0382076
 },
 {
   "STT": 872,
   "Name": "Quầy thuốc Bảo Trân",
   "address": "Số 637/6, ấp An Hòa, xã An Hảo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.4869761,
   "Latitude": 105.0221367
 },
 {
   "STT": 873,
   "Name": "Quầy thuốc Túy Phượng",
   "address": "Ấp Long Tân, xã LongĐiền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5111111,
   "Latitude": 105.4477778
 },
 {
   "STT": 874,
   "Name": "Quầy thuốc Trúc Linh 2",
   "address": "Tổ 15, ấp Tấn Phước, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 875,
   "Name": "Quầy thuốc Tố Anh",
   "address": "Số 179, ấp Long Tân, xã Long Điền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5104005,
   "Latitude": 105.4320465
 },
 {
   "STT": 876,
   "Name": "Quầy thuốc Tấn Hậu",
   "address": "Số 808, ấp An Lương, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 877,
   "Name": "Quầy thuốc Phú Thạnh",
   "address": "Tổ 04, ấp An Bình, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4330203,
   "Latitude": 105.5508873
 },
 {
   "STT": 878,
   "Name": "Quầy thuốc Lộc Sang",
   "address": "Số 337, tổ 16, ấp Tấn Quới, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 879,
   "Name": "Quầy thuốc Khiêm Trang",
   "address": "Số 248, ấp Long Tân, xã Long Điền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5104005,
   "Latitude": 105.4320465
 },
 {
   "STT": 880,
   "Name": "Quầy thuốc Cẩm Tuyết",
   "address": "Tổ 8, ấp Tấn Lợi, xãTấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4825247,
   "Latitude": 105.4771084
 },
 {
   "STT": 881,
   "Name": "Quầy thuốc Thiên Chương",
   "address": "Số 300, tổ 12, ấp Long Hòa I, xã Long Kiến, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4664526,
   "Latitude": 105.471249
 },
 {
   "STT": 882,
   "Name": "Quầy thuốc Đại Phát",
   "address": "Số 161, ấp Phú Hạ II, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 883,
   "Name": "Quầy thuốc Triệu Tín II",
   "address": "Số 25, ấp Long Thạnh II, xã Long Giang, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4957171,
   "Latitude": 105.4511467
 },
 {
   "STT": 884,
   "Name": "Quầy thuốc Thúy Loan",
   "address": "Kios số 2, đường Nguyễn Thái Học, ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 885,
   "Name": "Quầy thuốc Thế Cường",
   "address": "Ấp Long Hòa, xã Long Giang, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4658333,
   "Latitude": 105.4683333
 },
 {
   "STT": 886,
   "Name": "Quầy thuốc Phước Thọ",
   "address": "Số 306, ấp Mỹ Hòa A, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 887,
   "Name": "Quầy thuốc Ngọc Uyển",
   "address": "Tổ 11, ấp Long Phú, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 888,
   "Name": "Quầy thuốc Phi Yến",
   "address": "Số 192, ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4755556,
   "Latitude": 105.395
 },
 {
   "STT": 889,
   "Name": "Quầy thuốc Ngọc Em",
   "address": "Ấp Kiến Hưng I, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4825247,
   "Latitude": 105.4771084
 },
 {
   "STT": 890,
   "Name": "Quầy thuốc Huy Chinh",
   "address": "Số 132, tổ 5, ấp Nhơn Lợi, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4589745,
   "Latitude": 105.41181
 },
 {
   "STT": 891,
   "Name": "Quầy thuốc Chí Nhân",
   "address": "Ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4755556,
   "Latitude": 105.395
 },
 {
   "STT": 892,
   "Name": "Quầy thuốc Mỹ Ly",
   "address": "Số 172A, tổ 16, ấp Sơn Tân, xã Vọng Đông, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2678029,
   "Latitude": 105.1843801
 },
 {
   "STT": 893,
   "Name": "Quầy thuốc Nguyễn Sang",
   "address": "Số 138, tổ 7, ấp Tân Mỹ, xã Mỹ Phú Đông, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3253502,
   "Latitude": 105.2114696
 },
 {
   "STT": 894,
   "Name": "Quầy thuốc Mỹ Duyên",
   "address": "Số 187, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2760432,
   "Latitude": 105.3508203
 },
 {
   "STT": 895,
   "Name": "Quầy thuốc Thanh Liêm",
   "address": "Số 339, tổ 14, ấp Vĩnh Thắng, xã Vĩnh Khánh, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2597222,
   "Latitude": 105.3480556
 },
 {
   "STT": 896,
   "Name": "Quầy thuốc Trung Chi",
   "address": "Tổ 11, ấp Mỹ Thới, xãĐịnh Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3300097,
   "Latitude": 105.2932799
 },
 {
   "STT": 897,
   "Name": "Quầy thuốc Xuân Liên",
   "address": "Số 220, tổ 28, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3642987,
   "Latitude": 105.3306814
 },
 {
   "STT": 898,
   "Name": "Quầy thuốc Thủy Ngân",
   "address": "Ấp An Ninh, xã Lương Phi, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.4474043,
   "Latitude": 104.9248194
 },
 {
   "STT": 899,
   "Name": "Quầy thuốc Ngà Chi",
   "address": "Chợ Tri Tôn, thị trấn Tri Tôn, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.420975,
   "Latitude": 105.0026937
 },
 {
   "STT": 900,
   "Name": "Quầy thuốc Phước Hưng 2",
   "address": "Số 113/5, ấp Sóc Triết, xã Cô Tô, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3501846,
   "Latitude": 105.0384458
 },
 {
   "STT": 901,
   "Name": "Quầy thuốc Bửu Quang",
   "address": "Số 11, Chợ Tri Tôn, khóm II, thị trấn Tri Tôn, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.4219434,
   "Latitude": 105.0013906
 },
 {
   "STT": 902,
   "Name": "Quầy thuốc Mỹ Linh",
   "address": "Ấp Ninh Thuận, xã An Tức, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.3972321,
   "Latitude": 104.9856176
 },
 {
   "STT": 903,
   "Name": "Quầy thuốc Hải Ngọc",
   "address": "Ấp Giồng Cát, xãLương An Trà, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3883607,
   "Latitude": 104.889923
 },
 {
   "STT": 904,
   "Name": "Quầy thuốc Lê Hồng Hiếu",
   "address": "Ấp Vĩnh Hiệp, xã Vĩnh Gia, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.5073367,
   "Latitude": 104.7958532
 },
 {
   "STT": 905,
   "Name": "Quầy thuốc Phước Thọ",
   "address": "Ấp Vĩnh Hiệp, xã Vĩnh Gia, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.5073367,
   "Latitude": 104.7958532
 },
 {
   "STT": 906,
   "Name": "Quầy thuốc Thu Trang",
   "address": "Tổ 12, ấp Tân Lợi, xã Tân Phú, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3836773,
   "Latitude": 105.1551352
 },
 {
   "STT": 907,
   "Name": "Quầy thuốc Duy Phương",
   "address": "Tổ 12, ấp Tân Lợi, xã Tân Phú, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3836773,
   "Latitude": 105.1551352
 },
 {
   "STT": 908,
   "Name": "Quầy thuốc Kiều Lợi",
   "address": "Tổ 9, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 909,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Số 916, tổ 32, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 910,
   "Name": "Quầy thuốc số 0309",
   "address": "Tổ 22, ấp Hòa Phú 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 911,
   "Name": "Quầy thuốc Đức Lan",
   "address": "Tổ 8, ấp Thạnh Hòa, xã Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4693515,
   "Latitude": 105.357029
 },
 {
   "STT": 912,
   "Name": "Quầy thuốc Duy Minh",
   "address": "Tổ 12, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4506751,
   "Latitude": 105.2981186
 },
 {
   "STT": 913,
   "Name": "Quầy thuốc Hoàng Hải",
   "address": "Chợ Mỹ Đức, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AnGiang",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 914,
   "Name": "Quầy thuốc Mỹ Dung",
   "address": "Tổ 05, Chợ Hưng Thới, ấp Hưng Lợi, xã Đào Hữu Cảnh, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5122415,
   "Latitude": 105.1209811
 },
 {
   "STT": 915,
   "Name": "Quầy thuốc Hoàng Dũng",
   "address": "Tổ 03, ấp Mỹ Lợi, xã Mỹ Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 916,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Tổ 3, ấp Mỹ Lợi, xã Mỹ Phú, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 917,
   "Name": "Quầy thuốc Diễm Kiều",
   "address": "Số 481/19, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 919,
   "Name": "Quầy thuốc Thúy Oanh",
   "address": "Số 691, ấp Bình Hòa, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3861185,
   "Latitude": 105.386309
 },
 {
   "STT": 920,
   "Name": "Quầy thuốc Phú Cường",
   "address": "Số 340/18, Bình Khánh, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3953269,
   "Latitude": 105.4208821
 },
 {
   "STT": 921,
   "Name": "Quầy thuốc Bé Bảy",
   "address": "Ấp Bình Phú 2, xã Phú Bình, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.623492,
   "Latitude": 105.2253342
 },
 {
   "STT": 922,
   "Name": "Quầy thuốc Quyên",
   "address": "Ấp Bình Tây 1, xã PhúBình, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5796614,
   "Latitude": 105.2325225
 },
 {
   "STT": 923,
   "Name": "Quầy thuốc Phú Cường",
   "address": "Ấp Phú Trung, xã Phú Thành, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6675563,
   "Latitude": 105.2931113
 },
 {
   "STT": 924,
   "Name": "Quầy thuốc Ngọc Huyền",
   "address": "Ấp An Hưng, thị trấn An Phú, huyện An Phú,AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 925,
   "Name": "Quầy thuốc Bệnh viện đa khoa huyện PhúTân",
   "address": "Ấp Thượng 2, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5982252,
   "Latitude": 105.3532427
 },
 {
   "STT": 926,
   "Name": "Quầy thuốc Phong Phú",
   "address": "Ấp Hưng Tân, xã Phú Hưng, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.5965525,
   "Latitude": 105.3378651
 },
 {
   "STT": 927,
   "Name": "Quầy thuốc Tô Dư Ban",
   "address": "Ấp Phú Đông, xã Phú Xuân, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6334424,
   "Latitude": 105.281642
 },
 {
   "STT": 928,
   "Name": "Quầy thuốc Hồng Nghị",
   "address": "Ấp Phú Xương, thị trấn Chợ Vàm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.3413773
 },
 {
   "STT": 929,
   "Name": "Quầy thuốc Lan Hương",
   "address": "Số 704 Chu Văn An, ấp Trung Thạnh, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 930,
   "Name": "Quầy thuốc Bệnh viện đa khoa huyện ChợMới",
   "address": "Ấp Thị II, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 931,
   "Name": "Quầy thuốc Trúc Linh",
   "address": "Ấp Mỹ Tân, thị trấn MỹLuông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4956501,
   "Latitude": 105.4937686
 },
 {
   "STT": 932,
   "Name": "Quầy thuốc Hồng Huê",
   "address": "Số 456, tổ 1, ấp Mỹ Hòa, thị trấn Mỹ Luông, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 933,
   "Name": "Quầy thuốc Nguyên Cường",
   "address": "Ấp Kiến Hưng II, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 934,
   "Name": "Quầy thuốc Thiện Hậu",
   "address": "Tổ 2, ấp Long Thuận, xã Long Điền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5325595,
   "Latitude": 105.4589934
 },
 {
   "STT": 935,
   "Name": "Quầy thuốc Việt Thái",
   "address": "Ấp Long Hòa, thị trấnChợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.548909,
   "Latitude": 105.408669
 },
 {
   "STT": 936,
   "Name": "Quầy thuốc Sáu Nhân",
   "address": "Ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 937,
   "Name": "Quầy thuốc Minh Minh",
   "address": "Ấp Long Hòa, thị trấn Chợ Mới, huyện ChợMới, AN GIANG",
   "Longtitude": 10.548909,
   "Latitude": 105.408669
 },
 {
   "STT": 938,
   "Name": "Quầy thuốc Mỹ Phượng",
   "address": "Ấp Thị I, thị trấn Mỹ Luông, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4956501,
   "Latitude": 105.4937686
 },
 {
   "STT": 939,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Tổ 2, ấp Bình Chánh 2, xã Bình Mỹ, huyệnChâu Phú, AN GIANG",
   "Longtitude": 10.5816487,
   "Latitude": 105.2320603
 },
 {
   "STT": 940,
   "Name": "Quầy thuốc An Thịnh",
   "address": "Số 312, tổ 7, ấp Vĩnh Tường 2, xã Châu Phong, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 941,
   "Name": "Quầy thuốc Minh Khoa",
   "address": "Tổ 3, ấp Bình Hưng 2,xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 942,
   "Name": "Quầy thuốc Ngọc Tuyền",
   "address": "Tổ 5, ấp Vĩnh Tường 2, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 943,
   "Name": "Quầy thuốc Thu Thảo",
   "address": "Tổ 15, ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5164544,
   "Latitude": 105.3214531
 },
 {
   "STT": 944,
   "Name": "Quầy thuốc Hoàng Thảo",
   "address": "Ấp Bình Tây 1, xã Phú Bình, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.5810471,
   "Latitude": 105.2222065
 },
 {
   "STT": 945,
   "Name": "Quầy thuốc Hiền Phúc",
   "address": "Đường số 2, khu dân cư ấp Hòa Phú, xã Định Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 946,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Ấp Bình Hòa 2, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3861185,
   "Latitude": 105.386309
 },
 {
   "STT": 947,
   "Name": "Quầy thuốc Cẩm Tú",
   "address": "Tổ 15, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 948,
   "Name": "Quầy thuốc Minh Kiệp",
   "address": "Tổ 6, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 949,
   "Name": "Quầy thuốc Ngọc Đặng",
   "address": "Số 257, tổ 11, ấp Nam Huề, xã Bình Thành, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 950,
   "Name": "Quầy thuốc Sơn Hương",
   "address": "Số 473 Trà Sư, khóm Thới Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.6194704,
   "Latitude": 105.0006289
 },
 {
   "STT": 951,
   "Name": "Quầy thuốc Hồng Điệp",
   "address": "Đường Võ Văn Kiệt, tổ 3, ấp Tây Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 952,
   "Name": "Quầy thuốc Hồng Ngự",
   "address": "Kios số 4, nhà lồng chợ Thoại Sơn, ấp Bắc Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2692042,
   "Latitude": 105.2690986
 },
 {
   "STT": 953,
   "Name": "Quầy thuốc Xuân Nghi",
   "address": "Số 310, tổ 6, ấp Bình Thành, xã Bình Thành, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 954,
   "Name": "Quầy thuốc Lê Thụy",
   "address": "Số 15, ấp Trung Bình, xã Thoại Giang, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2595103,
   "Latitude": 105.2608002
 },
 {
   "STT": 955,
   "Name": "Quầy thuốc Diễm Đẹp",
   "address": "Số 211/8, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 956,
   "Name": "Quầy thuốc Minh Lý",
   "address": "Số 197, ấp An Hòa, xã An Hảo, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.4869761,
   "Latitude": 105.0221367
 },
 {
   "STT": 957,
   "Name": "Quầy thuốc Kim Chi",
   "address": "Tổ 1, khóm Xuân Hiệp, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 958,
   "Name": "Quầy thuốc Phạm Thị Bích",
   "address": "Số 12/8, khóm 3, thị trấn Chi Lăng, huyệnTịnh Biên, AN GIANG",
   "Longtitude": 10.5318577,
   "Latitude": 105.0265195
 },
 {
   "STT": 959,
   "Name": "Quầy thuốc Thu Vân",
   "address": "Tổ 6, khóm Hòa Hưng, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6223215,
   "Latitude": 105.0016485
 },
 {
   "STT": 961,
   "Name": "Quầy thuốc Hữu Vĩnh",
   "address": "Tổ 11, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 962,
   "Name": "Quầy thuốc Y Sa",
   "address": "Tổ 14, ấp Vĩnh Phúc, xã Vĩnh Hanh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.445273,
   "Latitude": 105.2301407
 },
 {
   "STT": 963,
   "Name": "Quầy thuốc Ngọc Mỹ",
   "address": "Tổ 23, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4426246,
   "Latitude": 105.3888335
 },
 {
   "STT": 965,
   "Name": "Quầy thuốc Ngọc Dung",
   "address": "Ấp Bình Tây 1, xã Phú Bình, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.5810471,
   "Latitude": 105.2222065
 },
 {
   "STT": 966,
   "Name": "Quầy thuốc Phước Lai",
   "address": "Tổ 2, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 967,
   "Name": "Quầy thuốc Mỹ Chi",
   "address": "Tổ 5, ấp Long Châu, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5833273,
   "Latitude": 105.181027
 },
 {
   "STT": 968,
   "Name": "Quầy thuốc Cẩm Loan",
   "address": "Tổ 1, ấp Bình Quới 2, xã Bình Thạnh Đông, huyện Phú Tân, AnGiang",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 969,
   "Name": "Quầy thuốc Minh Nguyệt",
   "address": "Số 209/4, Mỹ An 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4245638,
   "Latitude": 105.4360963
 },
 {
   "STT": 970,
   "Name": "Quầy thuốc Minh Phú",
   "address": "Ấp Phước Hòa, xã Phước Hưng, huyện An Phú, AN GIANG",
   "Longtitude": 10.8689233,
   "Latitude": 105.0840356
 },
 {
   "STT": 971,
   "Name": "Quầy thuốc Thanh Tùng",
   "address": "Số 820, tổ 29, ấp Tắc Trúc, xã Nhơn Hội, huyện An Phú, AN GIANG",
   "Longtitude": 10.8922487,
   "Latitude": 105.0498966
 },
 {
   "STT": 972,
   "Name": "Quầy thuốc Hiệp Hưng",
   "address": "Số 28 Lê Lợi, ấp Thị 1, thị trấn Mỹ Luông, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5076542,
   "Latitude": 105.4909388
 },
 {
   "STT": 973,
   "Name": "Quầy thuốc Ba Dũng",
   "address": "Số 43, tổ 1, ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756645,
   "Latitude": 105.3950939
 },
 {
   "STT": 974,
   "Name": "Quầy thuốc Sáu Huệ",
   "address": "Ấp Long Phú 2, xã Long Điền B, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.5104005,
   "Latitude": 105.4320465
 },
 {
   "STT": 975,
   "Name": "Quầy thuốc Hải Hồ",
   "address": "Số 257, tổ 10, ấp Đông Châu, xã Mỹ Hiệp, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 976,
   "Name": "Quầy thuốc Kim Tiến",
   "address": "Số 126, ấp Bình Trung, xã Bình Phước Xuân, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.453799,
   "Latitude": 105.552789
 },
 {
   "STT": 977,
   "Name": "Quầy thuốc Võ Thị Thước",
   "address": "Ấp Bình Thạnh 2, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3671932,
   "Latitude": 105.494688
 },
 {
   "STT": 978,
   "Name": "Quầy thuốc Hồng Châu",
   "address": "Số 402, ấp Bình Quới,xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3671932,
   "Latitude": 105.494688
 },
 {
   "STT": 979,
   "Name": "Quầy thuốc Gia Nguyên",
   "address": "Ấp Bình Trung, xã Bình Phước Xuân, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4541667,
   "Latitude": 105.5527778
 },
 {
   "STT": 980,
   "Name": "Quầy thuốc Tâm Như",
   "address": "Số 631, tổ 19, ấp An Mỹ, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3671932,
   "Latitude": 105.494688
 },
 {
   "STT": 981,
   "Name": "Quầy thuốc Trần Thị Út",
   "address": "Số 97, ấp An Thái, xãHòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 982,
   "Name": "Quầy thuốc Bảy Bằng",
   "address": "Tổ 5, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4630615,
   "Latitude": 105.3421605
 },
 {
   "STT": 983,
   "Name": "Quầy thuốc Chí Khang",
   "address": "Tổ 18, ấp Sơn Hiệp, xã An Bình, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3125096,
   "Latitude": 105.1609838
 },
 {
   "STT": 984,
   "Name": "Quầy thuốc Ngọc Quý",
   "address": "Số 323 Nguyễn Huệ, ấp Bắc Sơn, thị trấn Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 985,
   "Name": "Quầy thuốc Thúy Oanh",
   "address": "Số 49, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2760432,
   "Latitude": 105.3508203
 },
 {
   "STT": 986,
   "Name": "Quầy thuốc Thúy Diễm",
   "address": "Tổ 1, ấp Tây Bình B, xã Vĩnh Chánh, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 987,
   "Name": "Quầy thuốc Quốc Khánh 1",
   "address": "Tổ 9, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 988,
   "Name": "Quầy thuốc Minh Điền",
   "address": "Chợ Mỹ Đức, số 13 Lô A, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.6601075,
   "Latitude": 105.1884501
 },
 {
   "STT": 989,
   "Name": "QUẦY THUỐC HUỲNH TRÂN",
   "address": "Chợ Long Bình, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9548292,
   "Latitude": 105.0832331
 },
 {
   "STT": 990,
   "Name": "Quầy thuốc Kim Loan",
   "address": "Tổ 15, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 991,
   "Name": "Quầy thuốc Thùy Dung",
   "address": "Ấp An Thịnh, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 992,
   "Name": "Quầy thuốc Diễm Thúy",
   "address": "Ấp Bình Phước, xã Bình Phước Xuân, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4514826,
   "Latitude": 105.5532993
 },
 {
   "STT": 993,
   "Name": "Quầy thuốc Kinh Luân",
   "address": "Số 44, tổ 2, ấp Long Hòa 2, xã Long Kiến, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4664526,
   "Latitude": 105.471249
 },
 {
   "STT": 994,
   "Name": "QUẦY THUỐC KIM BA",
   "address": "Số 204, tổ 8, ấp Tây Thượng, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 995,
   "Name": "Quầy thuốc Thu Hiền",
   "address": "Số 425, ấp Thị 2, thị trấn Chợ Mới, huyệnChợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 996,
   "Name": "Quầy thuốc Kim Chi",
   "address": "Ấp Thị 1, thị trấn ChợMới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5505995,
   "Latitude": 105.3969764
 },
 {
   "STT": 998,
   "Name": "Quầy thuốc Bảo Linh",
   "address": "Số 179, tổ 3, ấp Long Thạnh 2, xã Long Hòa, huyện Phú Tân, AnGiang",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 999,
   "Name": "Quầy thuốc Ánh Loan",
   "address": "Số 193, tổ 4, ấp Châu Giang, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7340814,
   "Latitude": 105.1434387
 },
 {
   "STT": 1000,
   "Name": "Quầy thuốc Thái Vân",
   "address": "Tổ 4, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5682157,
   "Latitude": 105.2341082
 },
 {
   "STT": 1001,
   "Name": "Quầy thuốc Ngọc Thu",
   "address": "Tổ 19, ấp Vĩnh Hưng, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1002,
   "Name": "Quầy thuốc Hoàng Mai",
   "address": "Ấp An Hưng, thị trấn An Phú, huyện An Phú,AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 1003,
   "Name": "Quầy thuốc Trung Nghĩa",
   "address": "Tổ 3, ấp Vĩnh Hưng, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1004,
   "Name": "Quầy thuốc Thu Hằng",
   "address": "Ấp Vĩnh Thạnh, xã Vĩnh Lộc, huyện An Phú, AN GIANG",
   "Longtitude": 10.8512167,
   "Latitude": 105.1025075
 },
 {
   "STT": 1005,
   "Name": "Quầy thuốc Nguyên Khang",
   "address": "Tổ 10, ấp Tân Thành, xã Tân Lập, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.866667,
   "Latitude": 105.183333
 },
 {
   "STT": 1006,
   "Name": "Quầy thuốc Song Hương",
   "address": "Ấp Tân Trung, xã TàĐảnh, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.401378,
   "Latitude": 105.0963975
 },
 {
   "STT": 1007,
   "Name": "Quầy thuốc Thái Bình 1",
   "address": "Tổ 6, ấp Bình An 2, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4067213,
   "Latitude": 105.2428853
 },
 {
   "STT": 1008,
   "Name": "Quầy thuốc Nguyệt Hương",
   "address": "Tổ 43, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4435719,
   "Latitude": 105.3898099
 },
 {
   "STT": 1009,
   "Name": "Quầy thuốc Thanh Hùng 2",
   "address": "Ấp Trung 1, thị trấn PhúMỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5846855,
   "Latitude": 105.360363
 },
 {
   "STT": 1010,
   "Name": "Quầy thuốc Thu Thúy",
   "address": "Tổ 6, ấp Hòa Bình 3, xã Hòa Lạc, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.667617,
   "Latitude": 105.219466
 },
 {
   "STT": 1011,
   "Name": "Quầy thuốc Kim Oanh",
   "address": "Số 200/8 Đặng Huy Trứ, ấp Phú Hữu, thị trấn Phú Hòa, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3589268,
   "Latitude": 105.3763655
 },
 {
   "STT": 1012,
   "Name": "Quầy thuốc Phan Kỳ",
   "address": "Tổ 9, ấp Bình Chánh, xã Bình Long, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5671715,
   "Latitude": 105.249289
 },
 {
   "STT": 1013,
   "Name": "Quầy thuốc Thanh Tuấn",
   "address": "Số 313/15, ấp Vĩnh Trung, xã Vĩnh Trạch, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3310367,
   "Latitude": 105.3423909
 },
 {
   "STT": 1014,
   "Name": "Quầy thuốc Lệ Hằng",
   "address": "Lô 9B chợ Trà Mơn, ấp Mỹ An 1, xã Mỹ Hòa Hưng, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.4105908,
   "Latitude": 105.4303183
 },
 {
   "STT": 1015,
   "Name": "Quầy thuốc Ngọc Minh",
   "address": "Ấp Thượng 3, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5982252,
   "Latitude": 105.3532427
 },
 {
   "STT": 1017,
   "Name": "Quầy thuốc Giang Diễm",
   "address": "Ấp An Hưng, thị trấn An Phú, huyện An Phú,AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 1018,
   "Name": "Quầy thuốc Đại Thanh",
   "address": "Tổ 12, ấp Trung Bình 2, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 1019,
   "Name": "Quầy thuốc Minh Chi",
   "address": "Số 53, tổ 3, ấp Hòa Phú 2, thị trấn An Châu, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1020,
   "Name": "Quầy thuốc Kim Hoàng 1",
   "address": "Tổ 17, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 1021,
   "Name": "Quầy thuốc Mai Thơ",
   "address": "Chợ Vĩnh Thành, ấp Tân Thành, xã Vĩnh Thành, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3582121,
   "Latitude": 105.3190371
 },
 {
   "STT": 1022,
   "Name": "Quầy thuốc Nhã Hương",
   "address": "Ấp An Bình, xã Hòa Bình, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.3897222,
   "Latitude": 105.4704245
 },
 {
   "STT": 1023,
   "Name": "Quầy thuốc Quốc Nam",
   "address": "Số 686, ấp An Quới, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 1024,
   "Name": "Quầy thuốc Minh Đức",
   "address": "Ấp An Thuận, xã HòaBình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3846614,
   "Latitude": 105.4685588
 },
 {
   "STT": 1025,
   "Name": "Quầy thuốc Thúy Nga",
   "address": "Số 512, ấp Long Mỹ 1, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 1026,
   "Name": "Quầy thuốc Vinh Oanh",
   "address": "Số 374, tổ 21, ấp Hòa Phú, xã Định Thành, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 1027,
   "Name": "Quầy thuốc Đông Hồ",
   "address": "Tổ 23, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4426246,
   "Latitude": 105.3888335
 },
 {
   "STT": 1028,
   "Name": "Quầy thuốc Kim Thanh",
   "address": "Số 203, tổ 9, ấp Hòa Long 3, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1030,
   "Name": "Quầy thuốc Nhật Huy",
   "address": "Số 311/11, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 1031,
   "Name": "Quầy thuốc Mai Xuân",
   "address": "Số 182, ấp Long Hòa, xã Long Giang, huyệnChợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 1032,
   "Name": "Quầy thuốc Ngọc Liễu",
   "address": "Ấp Mỹ Tân, thị trấn Mỹ Luông, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4956501,
   "Latitude": 105.4937686
 },
 {
   "STT": 1033,
   "Name": "Quầy thuốc Nguyễn Huỳnh",
   "address": "Số 437, tổ 19, ấp Tây Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 1034,
   "Name": "Quầy thuốc Minh Phúc",
   "address": "Tổ 15, ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5164544,
   "Latitude": 105.3214531
 },
 {
   "STT": 1035,
   "Name": "Quầy thuốc Nhật Hùng",
   "address": "Số 25, tổ 15, đường Phan Bội Châu, ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5285357,
   "Latitude": 105.3189726
 },
 {
   "STT": 1036,
   "Name": "Quầy thuốc Ngọc",
   "address": "Tổ 2, ấp Bình Hưng 2, xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 1037,
   "Name": "Quầy Thuốc Huy Đạt",
   "address": "Tổ 13, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1038,
   "Name": "Quầy thuốc Thiên Hưng",
   "address": "Tổ 7, ấp Mỹ Long, xã Mỹ An, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 1039,
   "Name": "Quầy thuốc Mai Loan",
   "address": "Số 130, tổ 5, ấp Long Thuận, xã Long Giang, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 1041,
   "Name": "Quầy thuốc Diệp Quang",
   "address": "Số 236A, ấp Thị 2, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5508498,
   "Latitude": 105.4040296
 },
 {
   "STT": 1042,
   "Name": "Quầy thuốc Trung Quân",
   "address": "Chợ Vĩnh Bình, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 1043,
   "Name": "Quầy thuốc Khánh Tiên",
   "address": "Số 131, tổ 8, ấp Hòa Lợi 2, xã Vĩnh Lợi, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3929765,
   "Latitude": 105.3014109
 },
 {
   "STT": 1044,
   "Name": "Quầy thuốc Hồng Linh",
   "address": "Số 256, tổ 11, ấp Thị 2,xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 1045,
   "Name": "Quầy thuốc Ngọc Hiếu",
   "address": "Số 78, ấp Thị 2, xã Hội An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 1046,
   "Name": "Quầy thuốc Nghĩa Đạt",
   "address": "Số 659, tổ 19, ấp An Lương, xã Hòa Bình, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1047,
   "Name": "Quầy thuốc Mỹ Lan",
   "address": "Số 247, tổ 10, ấp Tấn Long, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 1048,
   "Name": "Quầy thuốc Trung Tín",
   "address": "Tổ 13, ấp Bình Minh, xãBình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5630265,
   "Latitude": 105.1726816
 },
 {
   "STT": 1050,
   "Name": "Quầy thuốc Mỹ Tiên",
   "address": "Tổ 23, ấp An Phú, xã An Hòa, huyện ChâuThành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 1051,
   "Name": "Quầy thuốc Vương Quí",
   "address": "Ấp Phước Thọ, xã Đa Phước, huyện An Phú,AN GIANG",
   "Longtitude": 10.7173664,
   "Latitude": 105.12151
 },
 {
   "STT": 1052,
   "Name": "Quầy thuốc Thùy Lam",
   "address": "Tổ 1, ấp Hòa Thạnh, xãChâu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 1053,
   "Name": "Quầy thuốc Minh Quang",
   "address": "Số 194, tổ 08, ấp Tân Phú, xã Tân Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8129361,
   "Latitude": 105.191593
 },
 {
   "STT": 1054,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "Tổ 09, ấp Vĩnh Khánh 2, xã Vĩnh Tế, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.668943,
   "Latitude": 105.0623171
 },
 {
   "STT": 1055,
   "Name": "Quầy thuốc Phong Ngọc",
   "address": "Tổ 14, ấp Mỹ An 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4017004,
   "Latitude": 105.4447566
 },
 {
   "STT": 1056,
   "Name": "Quầy thuốc An Khang",
   "address": "Tổ 35, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 1057,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "Tổ 22, ấp Phú An 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 1059,
   "Name": "Quầy thuốc Thoại An",
   "address": "Số 229, ấp Nam Huề, xã Bình Thành, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 1060,
   "Name": "Quầy thuốc Vương Lình",
   "address": "Số 272/13, ấp Vĩnh Trung, xã Vĩnh Trạch, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3310367,
   "Latitude": 105.3423909
 },
 {
   "STT": 1061,
   "Name": "Quầy thuốc Giáo Linh",
   "address": "Tổ 27, ấp Bình An 1, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4830556,
   "Latitude": 105.3055556
 },
 {
   "STT": 1062,
   "Name": "Quầy thuốc Trương Quốc",
   "address": "Số 8A/19, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 1063,
   "Name": "Quầy thuốc Hòa Việt",
   "address": "Tổ 10, ấp Tân Long, xã Tân Lợi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.49433,
   "Latitude": 105.0396687
 },
 {
   "STT": 1064,
   "Name": "Quầy thuốc Lan Anh",
   "address": "Tổ 08, khóm Thới Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 1065,
   "Name": "Quầy thuốc Trương Ngọc",
   "address": "Số 07/19, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 1066,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Kios số 54-55, chợ Núi Voi, xã Núi Voi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5385053,
   "Latitude": 105.0586639
 },
 {
   "STT": 1067,
   "Name": "Quầy thuốc Hoàng Vũ",
   "address": "Ấp Tân Thạnh, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9470064,
   "Latitude": 105.0881767
 },
 {
   "STT": 1068,
   "Name": "Quầy thuốc Nhựt Hồng",
   "address": "Tổ 09, ấp Hòa Long 3, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1070,
   "Name": "Quầy thuốc Dược Sĩ",
   "address": "Đường Phan Thị Ràng, khóm An Hòa B, thị trấn Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4942823,
   "Latitude": 104.9095979
 },
 {
   "STT": 1071,
   "Name": "Quầy thuốc Oanh",
   "address": "Tổ 20, ấp Bình Phú 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4617892,
   "Latitude": 105.3433121
 },
 {
   "STT": 1072,
   "Name": "Quầy thuốc Thúy Hằng",
   "address": "Ấp Bình Phú 2, xã Phú Bình, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.623492,
   "Latitude": 105.2253342
 },
 {
   "STT": 1073,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "Ấp Hậu Giang 1, xã Tân Hòa, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5690667,
   "Latitude": 105.3277541
 },
 {
   "STT": 1074,
   "Name": "Quầy thuốc Tuyết Vân",
   "address": "Ấp Long Phú 2, xã Long Điền B, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.5104005,
   "Latitude": 105.4320465
 },
 {
   "STT": 1075,
   "Name": "Quầy thuốc Vương Đình Hạnh",
   "address": "Số 105, tổ 1, ấp Tân Hiệp A, thị trấn Óc Eo, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1076,
   "Name": "Quầy thuốc Phước Thiện",
   "address": "Số 291, tổ 10, ấp Trung Phú 4, xã Vĩnh Phú, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3624284,
   "Latitude": 105.2166549
 },
 {
   "STT": 1077,
   "Name": "Quầy thuốc Bích Vân",
   "address": "Ấp Tắc Trúc, xã Nhơn Hội, huyện An Phú, AnGiang",
   "Longtitude": 10.9105865,
   "Latitude": 105.0538921
 },
 {
   "STT": 1078,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Số 385, ấp Mỹ Phú, xãMỹ An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 1079,
   "Name": "Quầy thuốc Quế Hương",
   "address": "Số 124, tổ 3, ấp Mỹ An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 1080,
   "Name": "Quầy thuốc Minh Thành",
   "address": "Số 379 Nguyễn Huệ, ấp Bắc Sơn, thị trấn Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 1081,
   "Name": "Quầy thuốc Bệnh viện đa khoa huyện TriTôn",
   "address": "Số 72 Nguyễn Trãi, thị trấn Tri Tôn, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4209044,
   "Latitude": 104.9988913
 },
 {
   "STT": 1082,
   "Name": "Quầy thuốc Thúy Hằng",
   "address": "Tổ 18, ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 1083,
   "Name": "Quầy thuốc Hoàng Oanh",
   "address": "Tổ 11, ấp Vĩnh Nhuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3730355,
   "Latitude": 105.2210788
 },
 {
   "STT": 1084,
   "Name": "Quầy thuốc Hà Thanh Tùng",
   "address": "Tổ 7, ấp Vĩnh Thuận, xã Vĩnh Hanh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4446287,
   "Latitude": 105.248737
 },
 {
   "STT": 1085,
   "Name": "Quầy thuốc Ngọc Phụng",
   "address": "Tổ 19, ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 1086,
   "Name": "Quầy thuốc Lan Vi",
   "address": "Lô 19, chợ An Châu, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4443713,
   "Latitude": 105.3901523
 },
 {
   "STT": 1087,
   "Name": "Quầy thuốc Tường Hưng",
   "address": "Tổ 15, ấp 1, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 1088,
   "Name": "Quầy thuốc Quyên Thủy",
   "address": "Tổ 4, ấp Long Hiệp, xã Long An, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.7865361,
   "Latitude": 105.1902297
 },
 {
   "STT": 1089,
   "Name": "Quầy thuốc Thu Tuyền",
   "address": "Số 96, tổ 3, ấp Phú Bình, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 1090,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Tổ 11, ấp Kiến Thuận 2, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1091,
   "Name": "Quầy thuốc Bảo Vy",
   "address": "Tổ 7, ấp Mỹ Long 2, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4100841,
   "Latitude": 105.4434738
 },
 {
   "STT": 1092,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "Tổ 11, ấp Tân Hòa C, xã Tân An, Tx. Tân Châu,AN GIANG",
   "Longtitude": 10.8423542,
   "Latitude": 105.1843501
 },
 {
   "STT": 1093,
   "Name": "Quầy thuốc Trung Hậu",
   "address": "Số 414, ấp Thuận An, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 1094,
   "Name": "Quầy thuốc Kim Cương",
   "address": "Số 424, tổ 12, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 1095,
   "Name": "Quầy thuốc Hải Ngọc",
   "address": "Số 828, tổ 4, khóm Long Hưng 2, phường Long Sơn, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7958634,
   "Latitude": 105.2308219
 },
 {
   "STT": 1097,
   "Name": "Quầy thuốc Khánh Huyền",
   "address": "Tổ 7, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1098,
   "Name": "Quầy thuốc Hùng Trang",
   "address": "Số 1017, ấp Bình Thạnh 1, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3852216,
   "Latitude": 105.5262355
 },
 {
   "STT": 1099,
   "Name": "Quầy thuốc Trúc Cang",
   "address": "Ấp Bình Phú 1, xã Phú Bình, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6200037,
   "Latitude": 105.2370339
 },
 {
   "STT": 1100,
   "Name": "Quầy thuốc Diễm Kiều",
   "address": "Số 24, ấp Hòa Bình 3, xã Hòa Lạc, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.667617,
   "Latitude": 105.219466
 },
 {
   "STT": 1101,
   "Name": "Quầy thuốc Bích Ngân",
   "address": "Số 672, tổ 9 , ấp Long Hòa 1, xã Long Hòa, huyện Phú Tân, AnGiang",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 1102,
   "Name": "Quầy thuốc Lý Nho",
   "address": "Số 628, ấp Mỹ Hội, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 1103,
   "Name": "Quầy thuốc Hưng Thảo",
   "address": "Số 448, ấp Mỹ Hòa, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 1104,
   "Name": "Quầy thuốc Phúc Nguyên",
   "address": "Tổ 11, ấp Long Quới 2, xã Long Điền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5112548,
   "Latitude": 105.4478131
 },
 {
   "STT": 1105,
   "Name": "Quầy thuốc Lê Văn Sến",
   "address": "Tổ 23, ấp Kinh Đào, xãPhú Thuận, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.304159,
   "Latitude": 105.4057097
 },
 {
   "STT": 1106,
   "Name": "Quầy thuốc Thu Thanh",
   "address": "Số 432 Lâm Thanh Hồng, tổ 5, ấp Trung Sơn, thị trấn Óc Eo, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2294719,
   "Latitude": 105.1592462
 },
 {
   "STT": 1107,
   "Name": "Quầy thuốc Hoàng Phi",
   "address": "Số 91 Lâm Thanh Hồng, tổ 5, ấp Trung Sơn, thị trấn Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2294719,
   "Latitude": 105.1592462
 },
 {
   "STT": 1108,
   "Name": "Quầy thuốc Thành Nhân",
   "address": "Số 1066, tổ 2, ấp Vĩnh Thạnh 1, xã Lê Chánh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.732922,
   "Latitude": 105.1668326
 },
 {
   "STT": 1109,
   "Name": "Quầy thuốc Ngọc Giàu",
   "address": "Số 014, tổ 1, ấp Mỹ Thới, xã Định Mỹ, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3562421,
   "Latitude": 105.4582046
 },
 {
   "STT": 1110,
   "Name": "Quầy thuốc Lan Thanh",
   "address": "Tổ 24, ấp Kinh Đào, xã Phú Thuận, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.304159,
   "Latitude": 105.4057097
 },
 {
   "STT": 1111,
   "Name": "Quầy thuốc Nhật Nam",
   "address": "Tổ 1, ấp Mỹ Phú, xã Mỹ An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 1112,
   "Name": "Quầy thuốc Hữu Tâm",
   "address": "Tổ 14, ấp Bình Phú, xã Bình Phước Xuân, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4704369,
   "Latitude": 105.5624355
 },
 {
   "STT": 1113,
   "Name": "Quầy thuốc Đức Trí",
   "address": "Số 419, tổ 14, ấp Thị, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 1114,
   "Name": "Quầy thuốc Minh Lý",
   "address": "Số 548, ấp Bình Thành, xã Bình Thành, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 1115,
   "Name": "Quầy thuốc Hồng Quyên",
   "address": "Ấp Hà Bao 1, xã Đa Phước, huyện An Phú,AN GIANG",
   "Longtitude": 10.7465743,
   "Latitude": 105.1199409
 },
 {
   "STT": 1116,
   "Name": "Quầy thuốc Mai Liên",
   "address": "Ấp Sa Tô, xã KhánhBình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9317017,
   "Latitude": 105.0786652
 },
 {
   "STT": 1117,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Tổ 1, ấp Tân Thạnh, xãTà Đảnh, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.866667,
   "Latitude": 105.183333
 },
 {
   "STT": 1118,
   "Name": "Quầy thuốc Danh Thuận",
   "address": "Tổ 18, ấp Tà On, xãChâu Lăng, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4430515,
   "Latitude": 104.9695519
 },
 {
   "STT": 1119,
   "Name": "Quầy thuốc Thành Bổn",
   "address": "Số 190/15, khóm Xuân Phú, thị trấn Tịnh Biên",
   "Longtitude": 10.6047746,
   "Latitude": 104.9607894
 },
 {
   "STT": 1120,
   "Name": "Quầy thuốc Sơn Thủy",
   "address": "Tổ 7, ấp Tân Phú B, xã Tân An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8136609,
   "Latitude": 105.1970174
 },
 {
   "STT": 1121,
   "Name": "Quầy thuốc Tuyền",
   "address": "Tổ 55, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4572188,
   "Latitude": 105.3324998
 },
 {
   "STT": 1122,
   "Name": "Quầy thuốc Thanh Nhàn",
   "address": "Tổ 3, ấp Vĩnh Thành, xã Vĩnh An, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.441856,
   "Latitude": 105.171806
 },
 {
   "STT": 1123,
   "Name": "Quầy thuốc Huỳnh Duy",
   "address": "Tổ 12, ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 1124,
   "Name": "Quầy thuốc Minh Phúc",
   "address": "Tổ 15, ấp Ninh Phước, xã Lương An Trà, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3912866,
   "Latitude": 104.8804893
 },
 {
   "STT": 1125,
   "Name": "Quầy thuốc Võ Đăng Quang",
   "address": "Tổ 17, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 1126,
   "Name": "Quầy thuốc Mỹ Châu",
   "address": "Số 477, tổ 16, ấp Hòa Trung, xã Kiến An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5423179,
   "Latitude": 105.3716684
 },
 {
   "STT": 1127,
   "Name": "Quầy thuốc số 195",
   "address": "Số 01 Nguyễn Huệ, ấp Tây Sơn, thị trấn Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 1128,
   "Name": "Quầy thuốc Phú Cường",
   "address": "Tổ 4, ấp 4, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8972222,
   "Latitude": 105.1666667
 },
 {
   "STT": 1129,
   "Name": "Quầy thuốc Khả Ái",
   "address": "Ấp Tắc Trúc, xã NhơnHội, huyện An Phú, AN GIANG",
   "Longtitude": 10.9105865,
   "Latitude": 105.0538921
 },
 {
   "STT": 1130,
   "Name": "Quầy thuốc Ên",
   "address": "Ấp Quốc Hưng, xãQuốc Thái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8943447,
   "Latitude": 105.077809
 },
 {
   "STT": 1131,
   "Name": "Quầy thuốc Bệnh viện  huyện Tịnh Biên",
   "address": "Tổ 3, ấp Sơn Đông, thị trấn Nhà Bàng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6476108,
   "Latitude": 105.0367466
 },
 {
   "STT": 1132,
   "Name": "Quầy thuốc Thu Hồng",
   "address": "Tổ 19, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1133,
   "Name": "Quầy thuốc Hùng Phước",
   "address": "Đường Nguyễn Văn Muôn, tổ 6, ấp Tân Hiệp B, thị trấn Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1134,
   "Name": "Quầy thuốc Hoài Thành",
   "address": "Số 21/A, Lộ nông thôn, ấp 1, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 1135,
   "Name": "Quầy thuốc Húa Tài",
   "address": "Tổ 2, ấp 1, xã Vĩnh Xương, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 1136,
   "Name": "Quầy thuốc Song Ngọc",
   "address": "Tổ 40, ấp Bình Phú 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 1137,
   "Name": "Quầy thuốc Phúc Tuyền",
   "address": "Tổ 20, ấp Mỹ Thuận, xã Mỹ Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 1138,
   "Name": "Quầy thuốc Thanh Tùng",
   "address": "Số 43 Lê Hồng Phong, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7708956,
   "Latitude": 105.2838511
 },
 {
   "STT": 1139,
   "Name": "Quầy thuốc Minh Châu",
   "address": "Số 227, tổ 4, ấp Hòa Long, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7340814,
   "Latitude": 105.1434387
 },
 {
   "STT": 1140,
   "Name": "Quầy thuốc Mỹ Hoa",
   "address": "Số 727, tổ 10, ấp Long Hòa 2, xã Long Hòa, huyện Phú Tân, AnGiang",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 1141,
   "Name": "Quầy thuốc Linh Xuyên",
   "address": "Tổ 6, ấp Tân Phú B, xã Tân An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8136609,
   "Latitude": 105.1970174
 },
 {
   "STT": 1142,
   "Name": "Quầy thuốc Phước Đặng",
   "address": "Số 472, tổ 11, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, AnGiang",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1143,
   "Name": "Quầy thuốc Kim Hoa",
   "address": "Số 10, tổ 1, ấp Hòa Trung, xã Kiến An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5423179,
   "Latitude": 105.3716684
 },
 {
   "STT": 1144,
   "Name": "Quầy thuốc số 999",
   "address": "Số 999, ấp An Thuận,xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 1145,
   "Name": "Quầy thuốc Út Hằng",
   "address": "Số 596, tổ 23, ấp Kiến Bình 1, xã Kiến An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5459241,
   "Latitude": 105.3601822
 },
 {
   "STT": 1146,
   "Name": "Quầy thuốc Tuấn Huy",
   "address": "Tổ 9, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 1147,
   "Name": "Quầy thuốc Xuân Thẳm",
   "address": "Tổ 10, ấp Hòa Lợi 1, xã Vĩnh Lợi, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3747313,
   "Latitude": 105.3132033
 },
 {
   "STT": 1148,
   "Name": "Quầy thuốc Cẩm Huệ",
   "address": "Tổ 19, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1149,
   "Name": "Quầy thuốc Long Ánh",
   "address": "Chợ Vĩnh Nhuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 1150,
   "Name": "Quầy thuốc Thái Nhân",
   "address": "Đường số 2, Chợ An Châu, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4443713,
   "Latitude": 105.3901523
 },
 {
   "STT": 1151,
   "Name": "Quầy thuốc Mai Linh",
   "address": "Tổ 12, ấp Long Bình, xã Ô Long Vỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5883214,
   "Latitude": 105.1025075
 },
 {
   "STT": 1152,
   "Name": "Quầy thuốc Bảo Duy",
   "address": "Tổ 6, ấp Hưng Lợi, xã Đào Hữu Cảnh, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4855694,
   "Latitude": 105.1245632
 },
 {
   "STT": 1153,
   "Name": "Quầy thuốc Sơn Trang",
   "address": "Tổ 11, ấp Bình Thạnh, xã Bình Chánh, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4997871,
   "Latitude": 105.2370339
 },
 {
   "STT": 1154,
   "Name": "Quầy thuốc Trung Việt",
   "address": "Số 154, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5690597,
   "Latitude": 105.2405097
 },
 {
   "STT": 1156,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "Tổ 14, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6717518,
   "Latitude": 105.173975
 },
 {
   "STT": 1157,
   "Name": "Quầy thuốc Thái Toàn",
   "address": "Tổ 6, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5764843,
   "Latitude": 105.2162554
 },
 {
   "STT": 1158,
   "Name": "Quầy thuốc Quang Oanh",
   "address": "Ấp Đồng Ky, xã QuốcThái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1159,
   "Name": "Quầy thuốc Phước Thành",
   "address": "Tổ 4, ấp Hòa Hưng, xã Hòa Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4245638,
   "Latitude": 105.4360963
 },
 {
   "STT": 1161,
   "Name": "Quầy thuốc Lan Chi",
   "address": "Ấp An Hòa, xã Khánh An, huyện An Phú, AnGiang",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 1162,
   "Name": "Quầy thuốc Thúy Kiều",
   "address": "Tổ 9, ấp Long Quới 1, xã Long Điền B, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5104005,
   "Latitude": 105.4320465
 },
 {
   "STT": 1163,
   "Name": "Quầy thuốc Thanh Tiến",
   "address": "Tổ 6, ấp Mỹ Lợi, xã Mỹ Phú, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 1164,
   "Name": "Quầy thuốc Khả Doanh",
   "address": "Ấp Ninh Thạnh, xã An Tức, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.4156719,
   "Latitude": 104.9165968
 },
 {
   "STT": 1165,
   "Name": "Quầy thuốc Hòa Nhứt",
   "address": "Ấp An Hòa, xã An Hảo,huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.4855337,
   "Latitude": 105.0148772
 },
 {
   "STT": 1166,
   "Name": "Quầy thuốc Hạnh Long",
   "address": "Tổ 2, ấp Vĩnh Tâm, xã Vĩnh Trung, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.550372,
   "Latitude": 105.021915
 },
 {
   "STT": 1167,
   "Name": "Quầy thuốc Chín Kết",
   "address": "Tổ 6, ấp Phú Tâm, xã An Phú, huyện TịnhBiên, AN GIANG",
   "Longtitude": 10.6216438,
   "Latitude": 104.9797753
 },
 {
   "STT": 1168,
   "Name": "Quầy thuốc Thùy Dương",
   "address": "Tổ 8, khóm 1, thị trấn Chi Lăng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.536548,
   "Latitude": 105.0303494
 },
 {
   "STT": 1169,
   "Name": "Quầy thuốc Ngọc Trâm",
   "address": "Số 516 Nguyễn Huệ, ấp Đông Sơn 2, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 1170,
   "Name": "Quầy thuốc Cẩm Tú",
   "address": "Tổ 14, ấp Trung Phú 2, xã Vĩnh Phú, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3525,
   "Latitude": 105.2311111
 },
 {
   "STT": 1171,
   "Name": "Quầy thuốc Tuyết Nhành",
   "address": "Tổ 9, ấp Mỹ Thới, xã Định Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3300097,
   "Latitude": 105.2932799
 },
 {
   "STT": 1172,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Số 522/A, tổ 17, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 1173,
   "Name": "Quầy thuốc Ngọc Hân",
   "address": "Tổ 11, ấp Vĩnh Tường 2, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 1174,
   "Name": "Quầy thuốc Hoàng Yến",
   "address": "Tổ 10, ấp Mỹ Thới, xãĐịnh Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3300097,
   "Latitude": 105.2932799
 },
 {
   "STT": 1175,
   "Name": "Quầy thuốc Minh Dung",
   "address": "Số 595, ấp Mỹ Phước, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 1177,
   "Name": "Quầy thuốc Ngọc Minh",
   "address": "Số 581, tổ 23, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1178,
   "Name": "Quầy thuốc Phước Hòa",
   "address": "Ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4359535,
   "Latitude": 105.384625
 },
 {
   "STT": 1179,
   "Name": "Quầy thuốc Thiên Phương",
   "address": "Số 268, tổ 6, ấp Hòa Hưng 1, xã Hòa Lạc, huyện Phú Tân, AnGiang",
   "Longtitude": 10.667617,
   "Latitude": 105.219466
 },
 {
   "STT": 1180,
   "Name": "Quầy thuốc Khang Trân",
   "address": "Ấp Hòa Bình 3, xã HòaLạc, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6666154,
   "Latitude": 105.2897042
 },
 {
   "STT": 1181,
   "Name": "Quầy thuốc Quốc Vinh",
   "address": "Ấp Tân Phú, xã PhúLâm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.7348122,
   "Latitude": 105.2747469
 },
 {
   "STT": 1182,
   "Name": "Quầy thuốc Tâm Phúc",
   "address": "Số 849, ấp Trung Thạnh, thị trấn Phú Mỹ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 1183,
   "Name": "Quầy thuốc Huỳnh Quốc Khánh",
   "address": "Tổ 18, ấp Phú An 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4633638,
   "Latitude": 105.3417831
 },
 {
   "STT": 1184,
   "Name": "Quầy thuốc Thúy Linh",
   "address": "Tổ 3, ấp Tân Trung, xã Tà Đảnh, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4031312,
   "Latitude": 105.0908147
 },
 {
   "STT": 1185,
   "Name": "Quầy thuốc Kim Phụng",
   "address": "Tổ 1, ấp Mỹ An 1, xã Mỹ Hòa Hưng, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.4066274,
   "Latitude": 105.427371
 },
 {
   "STT": 1186,
   "Name": "Quầy thuốc Bích Hạnh",
   "address": "Tổ 6, ấp Hưng Tân, xã Phú Hưng, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5940885,
   "Latitude": 105.3189726
 },
 {
   "STT": 1188,
   "Name": "Quầy thuốc Tâm Y",
   "address": "Số 778, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 1189,
   "Name": "Quầy thuốc Kim Phụng",
   "address": "Tổ 6, ấp An Lạc, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 1190,
   "Name": "Quầy thuốc Tuyết Minh",
   "address": "Số 675, tổ 8, ấp Phú Hữu 1, xã Lê Chánh,  Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.732922,
   "Latitude": 105.1668326
 },
 {
   "STT": 1191,
   "Name": "Quầy thuốc Hoàng Thảo 1",
   "address": "Số 589, ấp Phú Mỹ Thượng, xã Phú Thọ, huyện Phú Tân, AnGiang",
   "Longtitude": 10.633333,
   "Latitude": 105.333333
 },
 {
   "STT": 1192,
   "Name": "Quầy thuốc Trần Quý",
   "address": "Tổ 13, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1193,
   "Name": "Quầy thuốc Nhật Duy",
   "address": "Tổ 8, ấp Mỹ Thuận, xãVĩnh Châu, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6531969,
   "Latitude": 105.125336
 },
 {
   "STT": 1194,
   "Name": "Quầy thuốc Tuyết Minh",
   "address": "Tổ 12, ấp Bình An 2, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.5215836,
   "Latitude": 105.1258955
 },
 {
   "STT": 1195,
   "Name": "Quầy thuốc Minh Thi",
   "address": "Tổ 01, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 1196,
   "Name": "Quầy thuốc Ngọc An",
   "address": "Tổ 8, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1197,
   "Name": "Quầy thuốc Tuyết Nhi",
   "address": "Số 448/14, tổ 5, ấp Núi Voi, xã Núi Voi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5385053,
   "Latitude": 105.0586639
 },
 {
   "STT": 1198,
   "Name": "Quầy thuốc Kim Loan",
   "address": "Tổ 10, ấp Bình Phú, xã Bình Phước Xuân, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4704369,
   "Latitude": 105.5624355
 },
 {
   "STT": 1199,
   "Name": "Quầy thuốc Lam Nhi",
   "address": "Số 187, ấp Mỹ Tân, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4896962,
   "Latitude": 105.499324
 },
 {
   "STT": 1200,
   "Name": "Quầy thuốc Phương Loan",
   "address": "Ấp Mỹ An, xã Mỹ An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4756606,
   "Latitude": 105.5057221
 },
 {
   "STT": 1201,
   "Name": "Quầy thuốc Vĩnh Khang",
   "address": "Số 1011, ấp Bình Thạnh 1, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3852216,
   "Latitude": 105.5262355
 },
 {
   "STT": 1202,
   "Name": "Quầy thuốc Mila",
   "address": "Số 35 Nguyễn Hữu Cảnh, ấp An Hưng, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.8137455,
   "Latitude": 105.0909286
 },
 {
   "STT": 1203,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Số 197, tổ 27, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.2599612,
   "Latitude": 105.348246
 },
 {
   "STT": 1204,
   "Name": "Quầy thuốc Nguyễn Di Phúc",
   "address": "Kios D1, D2, ấp Phú Thuận, xã Tây Phú, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3570986,
   "Latitude": 105.1530449
 },
 {
   "STT": 1205,
   "Name": "Quầy thuốc Lệ Thu",
   "address": "Tổ 11, ấp Phú Thuận, xã Tây Phú, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.37394,
   "Latitude": 105.155577
 },
 {
   "STT": 1206,
   "Name": "Quầy thuốc Hoàng Nguyên",
   "address": "Tổ 20, ấp Hòa Long 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1207,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "Tổ 17, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1208,
   "Name": "Quầy thuốc Bích Liên",
   "address": "Ấp Thị 1, thị trấn Mỹ Luông, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.5049979,
   "Latitude": 105.4901978
 },
 {
   "STT": 1209,
   "Name": "Quầy thuốc Minh Quân",
   "address": "Số 712, tổ 4, ấp Phú Hữu 1, xã Lê Chánh,  Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.732922,
   "Latitude": 105.1668326
 },
 {
   "STT": 1210,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Tổ 21, ấp Kiến Bình 1, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5459241,
   "Latitude": 105.3601822
 },
 {
   "STT": 1212,
   "Name": "Quầy thuốc An Kiên",
   "address": "Số 8, ấp Thị 2, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4993584,
   "Latitude": 105.4830539
 },
 {
   "STT": 1213,
   "Name": "Quầy thuốc Ngọc Yến",
   "address": "Tổ 3, ấp Hiệp Thuận, xã Hiệp Xương, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5932012,
   "Latitude": 105.2799187
 },
 {
   "STT": 1214,
   "Name": "Quầy thuốc Hữu Tài",
   "address": "Số 236, tổ 7, ấp Bình Đông 2, xã Bình Thạnh Đông, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.5678113,
   "Latitude": 105.2612184
 },
 {
   "STT": 1215,
   "Name": "Quầy thuốc Hoàng Nam",
   "address": "Tổ 6, ấp Hòa Long 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1217,
   "Name": "Quầy thuốc Thanh Bình",
   "address": "Tổ 13, ấp Tân Thuận, xã Tân Lợi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.49433,
   "Latitude": 105.0396687
 },
 {
   "STT": 1218,
   "Name": "Quầy thuốc số 248 Võ ThịHồng Yến",
   "address": "Tổ 2, ấp Tân Thành, xã Vọng Thê, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.275753,
   "Latitude": 105.1317431
 },
 {
   "STT": 1219,
   "Name": "Quầy thuốc Huỳnh Mỹ",
   "address": "Ấp Vĩnh Quới, xã LạcQuới, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.5324229,
   "Latitude": 104.8992634
 },
 {
   "STT": 1220,
   "Name": "Quầy thuốc Thanh Dũng",
   "address": "Tổ 2, ấp Khánh Bình, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6770184,
   "Latitude": 105.1902297
 },
 {
   "STT": 1221,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Tổ 10, ấp Tân Thuận, xã Tân Lợi, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.49433,
   "Latitude": 105.0396687
 },
 {
   "STT": 1222,
   "Name": "Quầy thuốc Xuân Dũng",
   "address": "Tổ 6, ấp Sơn Tân, xã Vọng Đông, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.2629004,
   "Latitude": 105.1995933
 },
 {
   "STT": 1223,
   "Name": "Quầy thuốc Lư Huỳnh",
   "address": "Số 109, tổ 4, ấp Trung Phú 3, xã Vĩnh Phú, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3759416,
   "Latitude": 105.4185406
 },
 {
   "STT": 1224,
   "Name": "Quầy thuốc Trung Kiên",
   "address": "Tổ 1, ấp Phú Hữu, xã Định Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3371899,
   "Latitude": 105.2985487
 },
 {
   "STT": 1225,
   "Name": "Quầy thuốc 252",
   "address": "Số 142, tổ 4, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 1226,
   "Name": "Quầy thuốc Mỹ Ngọc",
   "address": "Tổ 4, ấp Bình Chánh 2, xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5316269,
   "Latitude": 105.3029658
 },
 {
   "STT": 1227,
   "Name": "Quầy thuốc Hồ Văn Dũng",
   "address": "Tổ 8, ấp Tân Hiệp B, thị trấn Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1228,
   "Name": "Quầy thuốc Mai Thật",
   "address": "Tổ 3, Tỉnh lộ 953, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8004009,
   "Latitude": 105.242378
 },
 {
   "STT": 1229,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Số 49, ấp An Long, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 1230,
   "Name": "Quầy thuốc Kỳ Phương",
   "address": "Số 76, tổ 2, ấp Tân Hậu A2, xã Tân An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7986632,
   "Latitude": 105.1668326
 },
 {
   "STT": 1231,
   "Name": "Quầy thuốc Tú Xương",
   "address": "Tổ 3, ấp Bình Thới, xãBình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5285357,
   "Latitude": 105.3189726
 },
 {
   "STT": 1232,
   "Name": "Quầy thuốc Thanh Vy",
   "address": "Số 350, ấp Mỹ Đức, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 1233,
   "Name": "Quầy thuốc Duy Hồng",
   "address": "Tổ 2, ấp Kiến Bình 1, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5459241,
   "Latitude": 105.3601822
 },
 {
   "STT": 1234,
   "Name": "Quầy thuốc Hồng Phương",
   "address": "Tổ 2, ấp An Bình, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3897222,
   "Latitude": 105.4704245
 },
 {
   "STT": 1235,
   "Name": "Quầy thuốc 116- Thạnh Tâm",
   "address": "Tổ 16, ấp Trung Bình 1, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 1236,
   "Name": "Quầy thuốc Phi Oanh",
   "address": "Số 546, ấp Hòa Hạ, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5563083,
   "Latitude": 105.3950728
 },
 {
   "STT": 1238,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Ấp An Hòa, xã Khánh An, huyện An Phú, AnGiang",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 1239,
   "Name": "Quầy thuốc Hùng Cường",
   "address": "Tổ 13, ấp Mỹ Thuận, xãVĩnh Châu, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6531969,
   "Latitude": 105.125336
 },
 {
   "STT": 1240,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Số 316/01, khóm Xuân Bình, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 1241,
   "Name": "Quầy thuốc Bình Trị",
   "address": "Tổ 6, ấp Mỹ Thuận, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4245638,
   "Latitude": 105.4360963
 },
 {
   "STT": 1242,
   "Name": "Quầy thuốc Xuân Nghi",
   "address": "Tổ 10, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1243,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "Tổ 7, ấp Vĩnh Quới, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.6152888,
   "Latitude": 105.199733
 },
 {
   "STT": 1244,
   "Name": "Quầy thuốc Long Liên",
   "address": "Tổ 11, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 1245,
   "Name": "Quầy thuốc Tư Vân",
   "address": "Số 413, ấp Mỹ Hội, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 1246,
   "Name": "Quầy thuốc Ngọc Son",
   "address": "Số 421, tổ 19, ấp Mỹ Hội, xã Mỹ Hội Đông, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4915985,
   "Latitude": 105.354221
 },
 {
   "STT": 1247,
   "Name": "Quầy thuốc Nhơn An",
   "address": "Tổ 10, ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.488611,
   "Latitude": 105.40336
 },
 {
   "STT": 1248,
   "Name": "Quầy thuốc Phượng",
   "address": "Số 10 Nguyễn Hữu Cảnh, ấp An Thịnh, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.8137455,
   "Latitude": 105.0909286
 },
 {
   "STT": 1249,
   "Name": "Quầy thuốc Nguyễn Khôi",
   "address": "Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, AnGiang",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1250,
   "Name": "Quầy thuốc Sơn Ca",
   "address": "Ấp An Quới, xã An Thạnh Trung, huyệnChợ Mới, AN GIANG",
   "Longtitude": 10.4193305,
   "Latitude": 105.464109
 },
 {
   "STT": 1251,
   "Name": "Quầy thuốc Quang Thái",
   "address": "Tổ 18, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1252,
   "Name": "Quầy thuốc Đặng Hòa An",
   "address": "Tổ 7, ấp An Thị, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 1253,
   "Name": "Quầy thuốc Thơm Hạnh 2",
   "address": "KDC An Châu, ấp Hòa Long 1, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1254,
   "Name": "Quầy thuốc Kim Hương",
   "address": "Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, AnGiang",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1255,
   "Name": "Quầy thuốc Bình Tuyền",
   "address": "Ấp Hòa Lợi, xã Phú Hiệp, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6571683,
   "Latitude": 105.2855843
 },
 {
   "STT": 1256,
   "Name": "Quầy thuốc Thanh Hiền",
   "address": "Chợ Vĩnh Nhuận, ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3730355,
   "Latitude": 105.2210788
 },
 {
   "STT": 1257,
   "Name": "Quầy thuốc Ngọc Giàu",
   "address": "Số 12, ấp Trung 1, thịtrấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5825573,
   "Latitude": 105.3567219
 },
 {
   "STT": 1258,
   "Name": "Quầy thuốc Vĩnh Hảo",
   "address": "Tổ 18, ấp Hưng Tân, xã Phú Hưng, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5940885,
   "Latitude": 105.3189726
 },
 {
   "STT": 1259,
   "Name": "Quầy thuốc Quang Tùng",
   "address": "Số 590/6 Mặc Cần Dện, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 1260,
   "Name": "Quầy thuốc Thanh Tú",
   "address": "Tổ 19, ấp Phú An 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 1262,
   "Name": "Quầy thuốc Mỹ Linh",
   "address": "Chợ Tân An, ấp Tân Phú B, xã Tân An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8136609,
   "Latitude": 105.1970174
 },
 {
   "STT": 1263,
   "Name": "Quầy thuốc Ngô Thuận",
   "address": "Số 10, tổ 01, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 1264,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Chợ Tân Tuyến, xã Tân Tuyến, huyện Tri Tôn,AN GIANG",
   "Longtitude": 10.3376626,
   "Latitude": 105.0908147
 },
 {
   "STT": 1265,
   "Name": "Quầy thuốc Tô Thị Cẩm Giàu",
   "address": "Số 273, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1266,
   "Name": "Quầy thuốc Phương Minh",
   "address": "Số 144, tổ 3, ấp Mỹ An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 1267,
   "Name": "Quầy thuốc Thái Giàu",
   "address": "Số 537, ấp Hòa Hạ, xã Kiến An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5563083,
   "Latitude": 105.3950728
 },
 {
   "STT": 1268,
   "Name": "Quầy thuốc Tiến Minh",
   "address": "Tổ 5, ấp An Thái, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4330203,
   "Latitude": 105.5508873
 },
 {
   "STT": 1269,
   "Name": "Quầy thuốc Huỳnh Hiền",
   "address": "Tổ 30, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 1270,
   "Name": "Quầy thuốc Thuận Sanh",
   "address": "Tổ 29, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 1271,
   "Name": "Quầy thuốc Phạm Gia I",
   "address": "Tổ 17, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 1272,
   "Name": "Quầy thuốc Quỳnh Vy",
   "address": "Tổ 9, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 1273,
   "Name": "Quầy thuốc Phạm Quốc",
   "address": "Tổ 16, ấp Long An, xã Ô Long Vỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5883214,
   "Latitude": 105.1025075
 },
 {
   "STT": 1274,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "Tổ 8, ấp Mỹ Lợi, xã Mỹ Phú, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 1275,
   "Name": "Quầy thuốc Ngọc Trân",
   "address": "Kios số 5, Khu bách hóa tổng hợp, Chợ Ô Long Vỹ, ấp Long An, xã Ô Long Vỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6001749,
   "Latitude": 105.1356512
 },
 {
   "STT": 1276,
   "Name": "Quầy thuốc Huyền Trân",
   "address": "Tổ 6, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 1277,
   "Name": "Quầy thuốc Thanh Mai",
   "address": "Tổ 15, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1278,
   "Name": "Quầy thuốc Khải Minh",
   "address": "Tổ 01, ấp Vĩnh Thành, thị trấn Cái Dầu, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5664597,
   "Latitude": 105.240612
 },
 {
   "STT": 1279,
   "Name": "Quầy thuốc Hồng Dự",
   "address": "Kios J07, J08, Chợ Long Châu, ấp Bờ Dâu, xã Thạnh Mỹ Tây, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5418448,
   "Latitude": 105.1458704
 },
 {
   "STT": 1280,
   "Name": "Quầy thuốc Minh Triết",
   "address": "Tổ 12, ấp Hưng Thạnh, xã Đào Hữu Cảnh, huyện Châu Phú, AnGiang",
   "Longtitude": 10.4893691,
   "Latitude": 105.1142011
 },
 {
   "STT": 1281,
   "Name": "Quầy thuốc Thu Thủy",
   "address": "Tổ 3, ấp Vĩnh Thạnh B, xã Vĩnh Hòa, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8528973,
   "Latitude": 105.1785307
 },
 {
   "STT": 1282,
   "Name": "Quầy thuốc Bảo Châu",
   "address": "Tổ 30, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 1283,
   "Name": "Quầy thuốc Vinh",
   "address": "Tổ 5, ấp Vĩnh Hòa, xã Lạc Quới, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.5074693,
   "Latitude": 104.8703043
 },
 {
   "STT": 1284,
   "Name": "Quầy thuốc Ngọc Vân",
   "address": "Ấp Phước Lộc, xã Ô Lâm, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.355998,
   "Latitude": 104.977193
 },
 {
   "STT": 1285,
   "Name": "Quầy thuốc Việt Mỹ",
   "address": "Số 126, ấp Thượng 2, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5997513,
   "Latitude": 105.351377
 },
 {
   "STT": 1286,
   "Name": "Quầy thuốc Ngọc Khánh",
   "address": "Tổ 1, ấp Mỹ Long 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.360347,
   "Latitude": 105.412569
 },
 {
   "STT": 1287,
   "Name": "Quầy thuốc Khánh Tiên",
   "address": "Số 36, tổ 01, ấp Đông Châu, xã Mỹ Hiệp, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 1288,
   "Name": "Quầy thuốc Đắc Chiến",
   "address": "Tổ 12, ấp Tân Lợi, xã Tân Phú, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3836773,
   "Latitude": 105.1551352
 },
 {
   "STT": 1289,
   "Name": "Quầy thuốc Trúc Ly",
   "address": "Tổ 11, ấp Vĩnh Thọ, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4204614,
   "Latitude": 105.1843801
 },
 {
   "STT": 1290,
   "Name": "Quầy thuốc Diệp Mi",
   "address": "Ấp Tân Thạnh, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9470064,
   "Latitude": 105.0881767
 },
 {
   "STT": 1291,
   "Name": "Quầy thuốc số 58",
   "address": "Ấp Sa Tô, xã KhánhBình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9317017,
   "Latitude": 105.0786652
 },
 {
   "STT": 1292,
   "Name": "Quầy thuốc Gia Hân",
   "address": "Số 70, tổ 03, ấp An Long, xã An Thạnh Trung, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 1293,
   "Name": "Quầy thuốc Ngọc Nhi",
   "address": "Tổ 01, ấp Thị 2, thị trấn Chợ Mới, huyện ChợMới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 1294,
   "Name": "Quầy thuốc Tư Long",
   "address": "Số 527, ấp Mỹ Hòa, xãNhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756645,
   "Latitude": 105.3950939
 },
 {
   "STT": 1295,
   "Name": "Quầy thuốc Khoa Minh",
   "address": "Tổ 7, ấp Khánh Thuận, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6442088,
   "Latitude": 105.2125434
 },
 {
   "STT": 1296,
   "Name": "Quầy thuốc Phụng Hiển",
   "address": "Tổ 17, ấp Tây Bình, xã Vĩnh Trạch, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 1297,
   "Name": "Quầy thuốc Tuyết Nhi",
   "address": "Quốc lộ 91, tổ 42, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4632258,
   "Latitude": 105.342672
 },
 {
   "STT": 1298,
   "Name": "Quầy thuốc Trần Lộc",
   "address": "Số 258, tổ 10, ấp Đông Châu, xã Mỹ Hiệp, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 1299,
   "Name": "Quầy thuốc Khoa Nguyên",
   "address": "Số 778, ấp Mỹ Tân, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4896962,
   "Latitude": 105.499324
 },
 {
   "STT": 1300,
   "Name": "Quầy thuốc Tấn Dũng",
   "address": "Kios số 02, ấp Bình Thạnh 1, xã Hòa An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.3852216,
   "Latitude": 105.5262355
 },
 {
   "STT": 1301,
   "Name": "Quầy thuốc Tuyết Nghĩa",
   "address": "Tổ 19, ấp An Thái, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1302,
   "Name": "Quầy thuốc Trang Thanh",
   "address": "Số 066/4, ấp Phú Hòa, xã An Phú, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6170431,
   "Latitude": 104.9899995
 },
 {
   "STT": 1303,
   "Name": "Quầy thuốc Minh Phúc",
   "address": "Tổ 7, khóm Sơn Đông, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6476108,
   "Latitude": 105.0367466
 },
 {
   "STT": 1304,
   "Name": "Quầy thuốc Như Quỳnh",
   "address": "Tổ 7, ấp Ba Xoài, xã An Cư, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.5223894,
   "Latitude": 104.956956
 },
 {
   "STT": 1305,
   "Name": "Quầy thuốc Thanh Phương",
   "address": "Số 165/5, khóm Hòa Thuận, thị trấn Nhà Bàng, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 1306,
   "Name": "Quầy thuốc Tuấn Trang",
   "address": "Tổ 4, ấp Hòa Tân, xã Định Thành, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3057775,
   "Latitude": 105.3014109
 },
 {
   "STT": 1307,
   "Name": "Quầy thuốc Bích Loan",
   "address": "Tổ 1, ấp Bình An, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 1308,
   "Name": "Quầy thuốc Hải Đăng",
   "address": "Số 269, tổ 10, ấp Phú Quí, xã Phú An, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6706279,
   "Latitude": 105.3189726
 },
 {
   "STT": 1309,
   "Name": "Quầy thuốc Năm Phố",
   "address": "Tổ 2, ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5164544,
   "Latitude": 105.3214531
 },
 {
   "STT": 1310,
   "Name": "Quầy thuốc Minh Luật",
   "address": "KDC Chợ Mỹ Đức, số 6, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 1311,
   "Name": "Quầy thuốc Ngọc Dung 2",
   "address": "Số 258, tổ 11, ấp Bình Trung 1, xã Bình Thạnh Đông, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.5796614,
   "Latitude": 105.2325225
 },
 {
   "STT": 1312,
   "Name": "Quầy thuốc Minh Ngọc",
   "address": "Số 70, tổ 3, ấp Bình Đông, xã Bình Thạnh Đông, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 1313,
   "Name": "Quầy thuốc Hoàng Quân",
   "address": "Số 316, tổ 7, ấp Hòa An, xã Hòa Lạc, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6752813,
   "Latitude": 105.2253316
 },
 {
   "STT": 1314,
   "Name": "Quầy thuốc số 280",
   "address": "Ấp Hòa Hưng 1, xã Hòa Lạc, huyện Phú Tân, AnGiang",
   "Longtitude": 10.6707297,
   "Latitude": 105.2246475
 },
 {
   "STT": 1315,
   "Name": "Quầy thuốc Cẩm Tiên",
   "address": "Tổ 10, ấp Hòa Thành, xã Định Thành, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3119397,
   "Latitude": 105.3167945
 },
 {
   "STT": 1316,
   "Name": "Quầy thuốc Phan Thành Phong",
   "address": "Số 328, ấp Tân Hiệp A, thị trấn Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1317,
   "Name": "Quầy thuốc Út Toại",
   "address": "Kios 3B, chợ Tân Thành, xã Vĩnh Thành, huyện Châu Thành, AnGiang",
   "Longtitude": 10.3582121,
   "Latitude": 105.3190371
 },
 {
   "STT": 1318,
   "Name": "Quầy thuốc Hồng Anh",
   "address": "Số 711, ấp Vĩnh Lợi 1, xã Châu Phong, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7233552,
   "Latitude": 105.1311105
 },
 {
   "STT": 1320,
   "Name": "Quầy thuốc Huỳnh Tha",
   "address": "Tổ 9, ấp Vĩnh Lập, xã Vĩnh Trung, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5531636,
   "Latitude": 105.0382076
 },
 {
   "STT": 1321,
   "Name": "Quầy thuốc Phúc Điền",
   "address": "Tổ 01, ấp Tân An, xã Tân Tuyến, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.3376626,
   "Latitude": 105.0908147
 },
 {
   "STT": 1322,
   "Name": "Quầy thuốc Vĩnh Lộc",
   "address": "Số 75A, tổ 7, đường liên xã, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3792302,
   "Latitude": 105.3872573
 },
 {
   "STT": 1323,
   "Name": "Quầy thuốc Trúc Ly",
   "address": "Tổ 2, ấp Phú Nhứt, xã An Phú, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6216438,
   "Latitude": 104.9797753
 },
 {
   "STT": 1325,
   "Name": "Quầy thuốc Nhựt Thành",
   "address": "Tổ 55, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4572188,
   "Latitude": 105.3324998
 },
 {
   "STT": 1326,
   "Name": "Quầy thuốc Hải Thuận",
   "address": "Tổ 14, ấp Vĩnh Thạnh B, xã Vĩnh Hòa, Tx.Tân Châu, AN GIANG",
   "Longtitude": 10.8528973,
   "Latitude": 105.1785307
 },
 {
   "STT": 1327,
   "Name": "Quầy thuốc Khoa",
   "address": "Số 122, tổ 4, ấp Phú An A, xã Phú Vĩnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 1329,
   "Name": "Quầy thuốc Thùy Trân",
   "address": "Tổ 4, ấp Phú Hiệp, thị trấn Chợ Vàm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.702865,
   "Latitude": 105.3306814
 },
 {
   "STT": 1330,
   "Name": "Quầy thuốc Minh Hòa",
   "address": "Số 390, tổ 8, ấp Phú Mỹ Hạ, xã Phú Thọ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5968965,
   "Latitude": 105.3513766
 },
 {
   "STT": 1331,
   "Name": "Quầy thuốc Hậu Hữu",
   "address": "Số 511, tổ 17, ấp Thị, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 1332,
   "Name": "Quầy thuốc Thanh Hưng",
   "address": "Tổ 12, ấp Thị, xã MỹHiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5083333,
   "Latitude": 105.5413889
 },
 {
   "STT": 1333,
   "Name": "Quầy thuốc Trần Tâm",
   "address": "Số 43, tổ 2, ấp Long Thạnh 2, xã Long Giang, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.4957171,
   "Latitude": 105.4511467
 },
 {
   "STT": 1334,
   "Name": "Quầy thuốc Thảo Nguyên 1",
   "address": "Số 556, tổ 21, ấp Long Mỹ 1, xã Long Giang, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 1335,
   "Name": "Quầy thuốc Bằng Phi",
   "address": "Số 112, tổ 3, ấp Mỹ An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 1336,
   "Name": "Quầy thuốc Linh Nguyễn",
   "address": "Chợ Long An, tổ 4, xãLong An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7888375,
   "Latitude": 105.1990045
 },
 {
   "STT": 1337,
   "Name": "Quầy thuốc Phi Hùng",
   "address": "Ấp Hà Bao 2, xã ĐaPhước, huyện An Phú, AN GIANG",
   "Longtitude": 10.7464634,
   "Latitude": 105.1200414
 },
 {
   "STT": 1338,
   "Name": "Quầy thuốc Phước Đức",
   "address": "Tổ 10, ấp Phú Hữu, xã Định Mỹ, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.8835468,
   "Latitude": 105.1142011
 },
 {
   "STT": 1339,
   "Name": "Quầy thuốc Mỹ Long",
   "address": "Tổ 4, ấp Mỹ Long 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4245638,
   "Latitude": 105.4360963
 },
 {
   "STT": 1340,
   "Name": "Quầy thuốc Khải Uyên",
   "address": "Ấp An Thịnh, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 1341,
   "Name": "Quầy thuốc Thắm",
   "address": "Số 184, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2760432,
   "Latitude": 105.3508203
 },
 {
   "STT": 1342,
   "Name": "Quầy thuốc Thiện Nhân",
   "address": "Số 2115, tổ 12, ấp Bình An 1, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4830556,
   "Latitude": 105.3055556
 },
 {
   "STT": 1343,
   "Name": "Quầy thuốc Huỳnh Phượng",
   "address": "Tổ 35, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 1344,
   "Name": "Quầy thuốc Thúy An",
   "address": "Tổ 14, ấp Mỹ Hiệp, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4122842,
   "Latitude": 105.430691
 },
 {
   "STT": 1345,
   "Name": "Quầy thuốc Việt Quang",
   "address": "Tổ 2, ấp Mỹ Long 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4066274,
   "Latitude": 105.427371
 },
 {
   "STT": 1346,
   "Name": "Quầy thuốc Pholly",
   "address": "Ấp Phnôm Pi, xã Châu Lăng, huyện Tri Tôn,AN GIANG",
   "Longtitude": 10.4512468,
   "Latitude": 104.9949026
 },
 {
   "STT": 1347,
   "Name": "Quầy thuốc Huy Hoàng 1",
   "address": "Số 344, tổ 17, ấp Tấn Quới, xã Tấn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 1348,
   "Name": "Quầy thuốc Lê Hoa",
   "address": "Số 22, ấp An Khương, xã An Thạnh Trung, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 1349,
   "Name": "Quầy thuốc Khoa Trinh",
   "address": "Số 89, tổ 3, ấp Mỹ Trung, xã Mỹ An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 1350,
   "Name": "Quầy thuốc Kim Yến",
   "address": "Khu TTTM Mỹ Luông, ấp Thị 2, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4993584,
   "Latitude": 105.4830539
 },
 {
   "STT": 1351,
   "Name": "Quầy thuốc Lê Đặng Ngân",
   "address": "Số 66, ấp Thị 2, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.505,
   "Latitude": 105.49
 },
 {
   "STT": 1352,
   "Name": "Quầy thuốc Tiền Hồ 7",
   "address": "Tổ 24, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 1353,
   "Name": "Quầy thuốc Đại Linh",
   "address": "Số 240, ấp Bình Thạnh 1, xã Hòa An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3852216,
   "Latitude": 105.5262355
 },
 {
   "STT": 1354,
   "Name": "Quầy thuốc Diễm Phương",
   "address": "Số 203, tổ 9, ấp Bình Tấn, xã Bình Phước Xuân, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.447669,
   "Latitude": 105.544701
 },
 {
   "STT": 1355,
   "Name": "Quầy thuốc Văn Chơn",
   "address": "Ấp An Hòa, xã Khánh An, huyện An Phú, AnGiang",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 1356,
   "Name": "Quầy thuốc Bình Yên",
   "address": "Ấp Phú Hòa, xã Phú Hữu, huyện An Phú, AnGiang",
   "Longtitude": 10.8835468,
   "Latitude": 105.1142011
 },
 {
   "STT": 1357,
   "Name": "Quầy thuốc 194",
   "address": "Số 194/8 Đặng Huy Trứ, thị trấn Phú Hòa, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.35874,
   "Latitude": 105.3765476
 },
 {
   "STT": 1358,
   "Name": "Quầy thuốc số 701",
   "address": "Tổ 11, ấp Tây Bình A, xã Vĩnh Chánh, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 1359,
   "Name": "Quầy thuốc Quốc Huy",
   "address": "Số 43, ấp Phú Bình, xã Phú An, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6596897,
   "Latitude": 105.3189726
 },
 {
   "STT": 1360,
   "Name": "Quầy thuốc Bé Tư",
   "address": "Số 359, tổ 9, ấp Phú Trung, xã Phú Thành, huyện Phú Tân, AnGiang",
   "Longtitude": 10.6574189,
   "Latitude": 105.2545888
 },
 {
   "STT": 1361,
   "Name": "Quầy thuốc Hoàng Thảo 2",
   "address": "Số 577, tổ 10, ấp Hậu Giang 1, xã Tân Hòa, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5690667,
   "Latitude": 105.3277541
 },
 {
   "STT": 1362,
   "Name": "Quầy thuốc Hồng Trang",
   "address": "Số 314, tổ 10, ấp Hòa Hiệp, xã Phú Hiệp, huyện Phú Tân, AnGiang",
   "Longtitude": 10.7098598,
   "Latitude": 105.1902297
 },
 {
   "STT": 1363,
   "Name": "Quầy thuốc Khải Trọng",
   "address": "Tổ 20, ấp Sơn Tân, xãVọng Đông, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.259588,
   "Latitude": 105.200768
 },
 {
   "STT": 1364,
   "Name": "Quầy thuốc Trung Dung",
   "address": "Đường Nguyễn Huệ, ấp Tây Sơn, thị trấn Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 1365,
   "Name": "Quầy thuốc Hoàng Huy",
   "address": "Tổ 8, khóm Xuân Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 1366,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Tổ 20, ấp Tây Hưng, xãNhơn Hưng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6223746,
   "Latitude": 104.9929208
 },
 {
   "STT": 1367,
   "Name": "Quầy thuốc Hoàng Diệu",
   "address": "Số 333, tổ 7, ấp Kiến Bình 2, xã Kiến An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5459241,
   "Latitude": 105.3601822
 },
 {
   "STT": 1368,
   "Name": "Quầy thuốc Tiết",
   "address": "Số 245, ấp An Quới, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 1369,
   "Name": "Quầy thuốc Nhật Tiến",
   "address": "Tổ 24, ấp Mỹ Hòa, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4663644,
   "Latitude": 105.3788738
 },
 {
   "STT": 1370,
   "Name": "Quầy thuốc Nhã Uyên",
   "address": "Số 328, tổ 11, ấp Long Thành, xã Long Điền B, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4957171,
   "Latitude": 105.4511467
 },
 {
   "STT": 1371,
   "Name": "Quầy thuốc Duy Tân",
   "address": "Số 1539, ấp Hòa Tây B, xã Phú Thuận, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.278342,
   "Latitude": 105.4185227
 },
 {
   "STT": 1372,
   "Name": "Quầy thuốc Minh Triết 1",
   "address": "Tổ 21, ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4755556,
   "Latitude": 105.395
 },
 {
   "STT": 1373,
   "Name": "Quầy thuốc Thanh Thủy",
   "address": "Tổ 16, ấp Phú An 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 1374,
   "Name": "Quầy thuốc Thơm Hạnh",
   "address": "Tổ 24, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1375,
   "Name": "Quầy thuốc Huỳnh Ngọc",
   "address": "Tổ B1, ấp Tân Thành,xã Vĩnh Thành, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3582121,
   "Latitude": 105.3190371
 },
 {
   "STT": 1376,
   "Name": "Quầy thuốc Tùng Bách",
   "address": "Chợ Tân Thành, xã Vĩnh Thành, huyệnChâu Thành, AN GIANG",
   "Longtitude": 10.3582121,
   "Latitude": 105.3190371
 },
 {
   "STT": 1377,
   "Name": "Quầy thuốc Ngọc Thành",
   "address": "Tổ 11, ấp Bình An 1, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4830556,
   "Latitude": 105.3055556
 },
 {
   "STT": 1378,
   "Name": "Quầy thuốc Trúc Linh",
   "address": "Tổ 12, ấp Tân Lợi, xã Tân Phú, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3836773,
   "Latitude": 105.1551352
 },
 {
   "STT": 1379,
   "Name": "Quầy thuốc Quỳnh Lê",
   "address": "Tổ 40, ấp Bình An 1, xã An Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4830556,
   "Latitude": 105.3055556
 },
 {
   "STT": 1381,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "Tổ 8, ấp Mỹ Phó, xã Mỹ Đức, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1382,
   "Name": "Quầy thuốc Thanh Trúc",
   "address": "Tổ 12, ấp Vĩnh Phước, xã Vĩnh Bình, huyệnChâu Thành, AN GIANG",
   "Longtitude": 10.4371958,
   "Latitude": 104.8337879
 },
 {
   "STT": 1383,
   "Name": "Quầy thuốc Năm Huỳnh",
   "address": "Tổ 22, ấp Tây Thượng, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 1384,
   "Name": "Quầy thuốc Minh Trung",
   "address": "Số 146A, tổ 3, ấp An Bình, xã Hòa Bình, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1385,
   "Name": "Quầy thuốc Minh Tâm",
   "address": "Số 164 Trần Hưng Đạo, ấp Thị 1, thị trấn Chợ Mới, huyện Chợ Mới,AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 1386,
   "Name": "Quầy thuốc Thu Ba",
   "address": "Tổ 21, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1387,
   "Name": "Quầy thuốc Nguyễn Tuyền",
   "address": "Ấp Ninh Thạnh, xã An Tức, huyện Tri Tôn, AnGiang",
   "Longtitude": 10.4156719,
   "Latitude": 104.9165968
 },
 {
   "STT": 1388,
   "Name": "Quầy thuốc Ngọc Tuyền",
   "address": "Ấp Giồng Cát, xã Lương An Trà, huyệnTri Tôn, AN GIANG",
   "Longtitude": 10.3883607,
   "Latitude": 104.889923
 },
 {
   "STT": 1389,
   "Name": "Quầy thuốc Thành Danh",
   "address": "Tổ 6, ấp Vĩnh Hiệp, xã Vĩnh Gia, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.5073367,
   "Latitude": 104.7958532
 },
 {
   "STT": 1390,
   "Name": "Quầy thuốc Lê Phát Đạt",
   "address": "Số 156, tổ 6, ấp Tấn Hưng, xã Tấn Mỹ, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 1392,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "Tổ 12, ấp Tây Bình B, xã Vĩnh Chánh, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 1393,
   "Name": "Quầy thuốc Huỳnh Ngân",
   "address": "Tổ 11, ấp Mỹ Thuận, xã Vĩnh Châu, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6531969,
   "Latitude": 105.125336
 },
 {
   "STT": 1394,
   "Name": "Quầy thuốc Nhân Nghĩa",
   "address": "Số 662 Nguyễn Huệ, ấp Nam Sơn, thị trấn Núi Sập, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.267279,
   "Latitude": 105.26676
 },
 {
   "STT": 1395,
   "Name": "Quầy thuốc Thanh Điền",
   "address": "Số 54 Lê Thánh Tôn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2625127,
   "Latitude": 105.2648662
 },
 {
   "STT": 1396,
   "Name": "Quầy thuốc Hoa Phượng 1",
   "address": "Số 97 Lê Lợi, ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 1398,
   "Name": "Quầy thuốc Phương Thảo 9",
   "address": "Tổ 36, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 1400,
   "Name": "Quầy thuốc 439",
   "address": "KDC mới ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AnGiang",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 1401,
   "Name": "Quầy thuốc Hiếu",
   "address": "Tổ 1, ấp Vĩnh Hòa 2, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3730355,
   "Latitude": 105.2210788
 },
 {
   "STT": 1402,
   "Name": "Quầy thuốc Quốc Cường",
   "address": "Số 507 Trà Sư, tổ 1, khóm Thới Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6194704,
   "Latitude": 105.0006289
 },
 {
   "STT": 1403,
   "Name": "Quầy thuốc Ty Phô",
   "address": "Số 177/5, ấp Ba Xoài, xã An Cư, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.5223894,
   "Latitude": 104.956956
 },
 {
   "STT": 1404,
   "Name": "Quầy thuốc Thành Trung",
   "address": "Tổ 9, ấp Núi Két, xã Thới Sơn, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6091506,
   "Latitude": 105.0015736
 },
 {
   "STT": 1405,
   "Name": "Quầy thuốc Văn Dương",
   "address": "Số 303, tổ 7, ấp Hiệp Hòa, xã Hiệp Xương, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5932012,
   "Latitude": 105.2799187
 },
 {
   "STT": 1406,
   "Name": "Quầy thuốc Diễm Thơ",
   "address": "Số 238, tổ 9, ấp Trung Hòa, xã Tân Trung, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5627357,
   "Latitude": 105.3453184
 },
 {
   "STT": 1407,
   "Name": "Quầy thuốc Ngọc Quyên",
   "address": "Số 602, tổ 8, ấp Long Hòa 2, xã Long Hòa, huyện Phú Tân, AnGiang",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 1408,
   "Name": "Quầy thuốc Bùi Thị Kim Hương",
   "address": "Tổ 10, ấp Bình Hòa 2, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3829665,
   "Latitude": 105.376542
 },
 {
   "STT": 1409,
   "Name": "Quầy thuốc Hiền Phước",
   "address": "Tổ 18, ấp Hòa Long 3, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.437845,
   "Latitude": 105.3928757
 },
 {
   "STT": 1410,
   "Name": "Quầy thuốc Ngọc Khanh",
   "address": "Tổ 6, ấp Hòa Tân, xãTân Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8423542,
   "Latitude": 105.1843501
 },
 {
   "STT": 1411,
   "Name": "Quầy thuốc Kim Y",
   "address": "Tổ 2, ấp Vĩnh Thạnh 1, xã Lê Chánh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.733889,
   "Latitude": 105.143889
 },
 {
   "STT": 1412,
   "Name": "Quầy thuốc Thúy Diễm",
   "address": "Tổ 4, ấp Hòa Thạnh, xã Tân Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7997101,
   "Latitude": 105.2405477
 },
 {
   "STT": 1413,
   "Name": "Quầy thuốc Ngọc Loan",
   "address": "Số 310/11, ấp Ba Xưa, xã Thạnh Mỹ Tây, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 1415,
   "Name": "Quầy thuốc Hồng Xuân",
   "address": "Tổ 17, ấp Khánh Phát, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6442088,
   "Latitude": 105.2125434
 },
 {
   "STT": 1416,
   "Name": "Quầy thuốc Nguyễn Xa Tô",
   "address": "Số 258, ấp Kiến Thuận 2, xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1417,
   "Name": "Quầy thuốc Nguyễn Tiền",
   "address": "Số 317, tổ 14, ấp Kiến Hưng 2, xã Kiến Thành, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1418,
   "Name": "Quầy thuốc Tấn Tới",
   "address": "Tổ 3, ấp Hòa Phú 3, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4475014,
   "Latitude": 105.3846359
 },
 {
   "STT": 1419,
   "Name": "Quầy thuốc Phương Anh",
   "address": "Tổ 10, ấp Đông Bình Trạch, xã Vĩnh Thành, huyện Châu Thành, AnGiang",
   "Longtitude": 10.35,
   "Latitude": 105.35
 },
 {
   "STT": 1420,
   "Name": "Quầy thuốc Thanh Gương",
   "address": "Tỉnh lộ 941, tổ 36, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4432477,
   "Latitude": 105.1756391
 },
 {
   "STT": 1422,
   "Name": "Quầy thuốc Uyên Nhi",
   "address": "Tổ 7, khóm 1, thị trấnChi Lăng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.536548,
   "Latitude": 105.0303494
 },
 {
   "STT": 1423,
   "Name": "Quầy thuốc Nhân Mỹ",
   "address": "Tổ 14, ấp Tây Huề, xã Bình Thành, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 1424,
   "Name": "Quầy thuốc Trần Văn Thum",
   "address": "Tổ 1A, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.6162274,
   "Latitude": 105.2095892
 },
 {
   "STT": 1425,
   "Name": "Quầy thuốc An Bình",
   "address": "Tổ 6, ấp Thị 1, xã Hội An, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 1426,
   "Name": "Quầy thuốc Hoàng Oanh 1",
   "address": "Tổ 12, ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 1427,
   "Name": "Quầy thuốc Thanh Nam",
   "address": "Số 66, tổ 3, ấp Long Hòa 1, xã Long Kiến, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4664526,
   "Latitude": 105.471249
 },
 {
   "STT": 1428,
   "Name": "Quầy thuốc Tuyết Hồng",
   "address": "Số 936, tổ 16, ấp An Lương, xã Hòa Bình, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1429,
   "Name": "Quầy thuốc Nguyễn ThịCẩm Tú",
   "address": "Ấp Phước Hòa, xã Phước Hưng, huyện An Phú, AN GIANG",
   "Longtitude": 10.8689233,
   "Latitude": 105.0840356
 },
 {
   "STT": 1430,
   "Name": "Quầy thuốc Lê Ngọc",
   "address": "Ấp Đồng Ky, xã QuốcThái, huyện An Phú, AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1431,
   "Name": "Quầy thuốc Toàn Thắng",
   "address": "Tổ 15, ấp Bình Trung, xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5796614,
   "Latitude": 105.2325225
 },
 {
   "STT": 1432,
   "Name": "Quầy thuốc Hữu Tình",
   "address": "Số 296, ấp Mỹ Hóa 1, xã Tân Trung, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5627357,
   "Latitude": 105.3453184
 },
 {
   "STT": 1433,
   "Name": "Quầy thuốc Bảo An",
   "address": "Đường Ngô Tự Lợi, khóm An Hòa B, thị trấn Ba Chúc, huyện Tri Tôn, AN GIANG",
   "Longtitude": 10.4942823,
   "Latitude": 104.9095979
 },
 {
   "STT": 1434,
   "Name": "Quầy thuốc Kiều Hương",
   "address": "Tổ 10, ấp Vĩnh Thọ, xãVĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4067213,
   "Latitude": 105.2428853
 },
 {
   "STT": 1435,
   "Name": "Quầy thuốc Thanh Son",
   "address": "Tổ 24, ấp Vĩnh Thành, xã Vĩnh An, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4405883,
   "Latitude": 105.3933397
 },
 {
   "STT": 1436,
   "Name": "Quầy thuốc Trung Thành",
   "address": "Số 275, tổ 6, ấp Bình Thành, xã Phú Bình, huyện Phú Tân, AnGiang",
   "Longtitude": 10.5933054,
   "Latitude": 105.2493604
 },
 {
   "STT": 1437,
   "Name": "Quầy thuốc Hữu Chí",
   "address": "Số 18, tổ 4, ấp Phú Hạ, xã Phú Xuân, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6286249,
   "Latitude": 105.2838511
 },
 {
   "STT": 1438,
   "Name": "Quầy thuốc Kim Tiền",
   "address": "Tổ 10, ấp Bình Hòa 2, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3829665,
   "Latitude": 105.376542
 },
 {
   "STT": 1439,
   "Name": "Quầy thuốc Tâm Hà",
   "address": "Thửa 122, tổ 3, ấp Bình Hòa, xã Mỹ Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3861185,
   "Latitude": 105.386309
 },
 {
   "STT": 1440,
   "Name": "Quầy thuốc Thanh Sang",
   "address": "Tổ 20, ấp Sơn Tân, xã Vọng Đông, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.259588,
   "Latitude": 105.200768
 },
 {
   "STT": 1441,
   "Name": "Quầy thuốc Mỹ Lệ",
   "address": "KDC, tổ 1, ấp Vĩnh Lợi, xã Vĩnh Khánh, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2599612,
   "Latitude": 105.348246
 },
 {
   "STT": 1442,
   "Name": "Quầy thuốc Thanh Trúc",
   "address": "Số 247, ấp Mỹ Thuận, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 1443,
   "Name": "Quầy thuốc Tiền Hồ 9",
   "address": "Số 301, tổ 7, ấp Long Hòa 1, xã Long Kiến, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.4664526,
   "Latitude": 105.471249
 },
 {
   "STT": 1444,
   "Name": "Quầy thuốc Thu Phương",
   "address": "Tổ 7, ấp Bình An, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4770834,
   "Latitude": 105.1957789
 },
 {
   "STT": 1445,
   "Name": "Quầy thuốc Bích Ngân",
   "address": "Số 393/01, ấp Khánh Hòa, xã Khánh Hòa, huyện Châu Phú, AnGiang",
   "Longtitude": 10.6770184,
   "Latitude": 105.1902297
 },
 {
   "STT": 1446,
   "Name": "Quầy thuốc Phương Oanh",
   "address": "Tổ 15, ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5164544,
   "Latitude": 105.3214531
 },
 {
   "STT": 1447,
   "Name": "Quầy thuốc Liên Thảo",
   "address": "Số 334/10, ấp Bình Đức, xã Bình Phú, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 1449,
   "Name": "Quầy thuốc Trọng Nghĩa",
   "address": "Số 14/13, khóm Xuân Hòa, thị trấn Tịnh Biên, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 1450,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "Số 096, tổ 7, ấp Tây Bình C, xã Vĩnh Chánh, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 1451,
   "Name": "Quầy thuốc Ngọc Huệ",
   "address": "Tổ 7, ấp Tân Phú B, xã Tân An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.8136609,
   "Latitude": 105.1970174
 },
 {
   "STT": 1452,
   "Name": "Quầy thuốc Dương Tuyền",
   "address": "Tổ 14, ấp Long Hiệp, xãLong An, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7865361,
   "Latitude": 105.1902297
 },
 {
   "STT": 1453,
   "Name": "Quầy thuốc Cường Dung",
   "address": "Số 285, ấp Hưng Mỹ, xãPhú Hưng, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6666154,
   "Latitude": 105.2897042
 },
 {
   "STT": 1454,
   "Name": "Quầy thuốc Diễm Phương",
   "address": "Số 334, tổ 7, ấp Hiệp Thuận, xã Hiệp Xương, huyện Phú Tân, AnGiang",
   "Longtitude": 10.6073356,
   "Latitude": 105.2721456
 },
 {
   "STT": 1455,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "Tổ 2, ấp Phú Hữu 2, xã Lê Chánh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.732922,
   "Latitude": 105.1668326
 },
 {
   "STT": 1456,
   "Name": "Quầy thuốc Trọng Nghĩa",
   "address": "Tổ 12, ấp Vĩnh Bình, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1457,
   "Name": "Quầy thuốc Triều Phát",
   "address": "Tổ 11, ấp Ba Xưa, xã Thạnh Mỹ Tây, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 1458,
   "Name": "Quầy thuốc Lê Giang",
   "address": "Tổ 6, ấp Thạnh An, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.576247,
   "Latitude": 105.2140903
 },
 {
   "STT": 1459,
   "Name": "Quầy thuốc Hòa Ngọc",
   "address": "Chợ Ô Long Vĩ, kios số 05, tổ 8, ấp Long An, xã Ô Long Vỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6001749,
   "Latitude": 105.1356512
 },
 {
   "STT": 1460,
   "Name": "Quầy thuốc Thành Tuấn",
   "address": "Tổ 10, ấp Mỹ Hòa, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1461,
   "Name": "Quầy thuốc Như Ngọc 1",
   "address": "Ấp Đồng Ky, xã Quốc Thái, huyện An Phú, AnGiang",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1462,
   "Name": "Quầy thuốc Triều An",
   "address": "Ấp Phước Hòa, xã Phước Hưng, huyện An Phú, AN GIANG",
   "Longtitude": 10.8689233,
   "Latitude": 105.0840356
 },
 {
   "STT": 1463,
   "Name": "Quầy thuốc Hoàng Kim 1",
   "address": "Kios số 36, lô D TTTM, ấp Thị 2, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 1464,
   "Name": "Quầy thuốc Hữu Thọ 1",
   "address": "Số 272, ấp Long Hòa 1, xã Long Kiến, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.7573526,
   "Latitude": 105.2809247
 },
 {
   "STT": 1465,
   "Name": "Quầy thuốc Khánh Nhi",
   "address": "Số 7, tổ 1, ấp Đông, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 1467,
   "Name": "Quầy thuốc Lê Quang Vinh",
   "address": "Số 12/1A Đinh Tiên Hoàng, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1468,
   "Name": "Quầy thuốc Tấn Luôn",
   "address": "Số 163/05, ấp Thạnh An, xã Vĩnh Thạnh Trung, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.5833273,
   "Latitude": 105.181027
 },
 {
   "STT": 1469,
   "Name": "Quầy thuốc Vạn Lợi",
   "address": "Tổ 12, ấp Long Bình, xã Ô Long Vỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5883214,
   "Latitude": 105.1025075
 },
 {
   "STT": 1470,
   "Name": "Quầy thuốc Kim Là",
   "address": "Số 213/08, ấp Hưng Lợi, xã Đào Hữu Cảnh, huyện Châu Phú, AnGiang",
   "Longtitude": 10.4893691,
   "Latitude": 105.1142011
 },
 {
   "STT": 1471,
   "Name": "Quầy thuốc Hải Nghi",
   "address": "Số 492/10, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AnGiang",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1472,
   "Name": "Quầy thuốc Phước Nam",
   "address": "Tổ 4, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 1473,
   "Name": "Quầy thuốc Thanh Long",
   "address": "Ấp Bình Phú, xã Bình Thủy, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.5164544,
   "Latitude": 105.3214531
 },
 {
   "STT": 1475,
   "Name": "Quầy thuốc Nguyệt Lệ",
   "address": "Tổ 9, ấp Bình Hưng 1, xã Bình Mỹ, huyệnChâu Phú, AN GIANG",
   "Longtitude": 10.5213483,
   "Latitude": 105.3071753
 },
 {
   "STT": 1476,
   "Name": "Quầy thuốc 463",
   "address": "Số 1536, tổ 2, ấp Hòa Tây B, xã Phú Thuận, huyện Thoại Sơn, AnGiang",
   "Longtitude": 10.278342,
   "Latitude": 105.4185227
 },
 {
   "STT": 1477,
   "Name": "Quầy thuốc Thái Lan",
   "address": "Tổ 11, ấp Phú Đông, xã Phú Xuân, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6286249,
   "Latitude": 105.2838511
 },
 {
   "STT": 1478,
   "Name": "Quầy thuốc Quốc Thanh",
   "address": "Ấp Phú Đông, xã Phú Xuân, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.6334424,
   "Latitude": 105.281642
 },
 {
   "STT": 1479,
   "Name": "Quầy thuốc Hồng Phượng",
   "address": "Tổ 4, đường Hùng Vương, ấp Bắc Sơn, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2693393,
   "Latitude": 105.2682625
 },
 {
   "STT": 1480,
   "Name": "Quầy thuốc Kim Nguyên",
   "address": "Tổ 8, ấp Khánh Bình, xã Khánh Hòa, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6442088,
   "Latitude": 105.2125434
 },
 {
   "STT": 1481,
   "Name": "Quầy thuốcNguyễn Thị Thắm",
   "address": "Ấp Tân Bình, thị trấnLong Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 1482,
   "Name": "Quầy thuốc Minh Ngọc",
   "address": "Tổ 12, ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4506751,
   "Latitude": 105.2981186
 },
 {
   "STT": 1483,
   "Name": "Quầy thuốc Tiền Hồ 10",
   "address": "Kios 55, 56 chợ Mỹ Khánh, xã Mỹ Khánh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3792765,
   "Latitude": 105.3905147
 },
 {
   "STT": 1484,
   "Name": "Quầy thuốc Bá Phước",
   "address": "Tổ 14, ấp Trung Phú 3, xã Vĩnh Phú, huyệnThoại Sơn, AN GIANG",
   "Longtitude": 10.3624284,
   "Latitude": 105.2166549
 },
 {
   "STT": 1485,
   "Name": "Quầy thuốc Minh Trí",
   "address": "Số 373/18, ấp Tây Bình, xã Vĩnh Trạch, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 1486,
   "Name": "Quầy thuốc Hữu Lộc",
   "address": "Số 66, tổ 3, ấp Bình Đông 1, xã Bình Thạnh Đông, huyện Phú Tân,AN GIANG",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 1487,
   "Name": "Trung Kiên",
   "address": "Số 168/3, khóm Đông Thịnh 5, phường Mỹ Phước, thành phố  Long Xuyên,AN GIANG",
   "Longtitude": 10.375602,
   "Latitude": 105.44761
 },
 {
   "STT": 1488,
   "Name": "Kim Long",
   "address": "Số 280 Thủ Khoa Nghĩa, phường Châu Phú A, thành phố  Châu Đốc,AN GIANG",
   "Longtitude": 10.7106923,
   "Latitude": 105.1163426
 },
 {
   "STT": 1489,
   "Name": "Hồng Hạnh",
   "address": "Tổ 6, khóm Vĩnh Chánh 2, phường Vĩnh Ngươn, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7241412,
   "Latitude": 105.109085
 },
 {
   "STT": 1490,
   "Name": "Uy Long Đường",
   "address": "Số 290 Lý Thái Tổ, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.377888,
   "Latitude": 105.4422827
 },
 {
   "STT": 1491,
   "Name": "Thu Hà",
   "address": "Số 3/49, khóm Thới Hòa, phường Mỹ Thạnh, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 1492,
   "Name": "Song Thiện",
   "address": "Số 6B5 Nguyễn Thượng Hiền, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3913046,
   "Latitude": 105.4247617
 },
 {
   "STT": 1493,
   "Name": "Đức Minh",
   "address": "Số 36/1A Lê Chân, phường Mỹ Quý, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3636341,
   "Latitude": 105.4489039
 },
 {
   "STT": 1494,
   "Name": "Minh Đức Dược Đường",
   "address": "Tổ 21, ấp Bình Minh, xã Bình Mỹ, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 1495,
   "Name": "Bích Trâm",
   "address": "Tổ 1, khóm Mỹ Hòa, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.690276,
   "Latitude": 105.1434387
 },
 {
   "STT": 1496,
   "Name": "Phước An Đường",
   "address": "Số 14 Nguyễn Huệ A, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3817085,
   "Latitude": 105.4405332
 },
 {
   "STT": 1497,
   "Name": "Thanh Hưng",
   "address": "Số 28/8 Trần Quy Cáp, phường Mỹ Thới, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3461458,
   "Latitude": 105.4673068
 },
 {
   "STT": 1498,
   "Name": "Tân Xương Lợi",
   "address": "Số 164/5, khóm Hòa Thuận, thị trấn Nhà Bàng, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 1499,
   "Name": "Tuyết Nga",
   "address": "Số 31 Thủ Khoa Nghĩa, phường Châu Phú A,  thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.715356,
   "Latitude": 105.1120699
 },
 {
   "STT": 1500,
   "Name": "Phúc Hưng",
   "address": "Tổ 23, ấp Kiến Hưng,xã Kiến Thành, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1501,
   "Name": "Trúc Phương",
   "address": "Số 58 Võ Văn Hoài, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3883695,
   "Latitude": 105.4206111
 },
 {
   "STT": 1502,
   "Name": "Vạn Tường",
   "address": "Số 15/5B Bùi Thị Xuân, phường Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3796141,
   "Latitude": 105.4373756
 },
 {
   "STT": 1503,
   "Name": "Cơ Sở 0188",
   "address": "Số 590/30, khóm Bình Khánh 4, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3953269,
   "Latitude": 105.4208821
 },
 {
   "STT": 1504,
   "Name": "Nguyệt Nga",
   "address": "Số 1325B/67 Trần Hưng Đạo, phường Bình Đức, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3893975,
   "Latitude": 105.4306305
 },
 {
   "STT": 1505,
   "Name": "Kim Thanh",
   "address": "Số 535 Tôn Đức Thắng, phường Vĩnh Mỹ, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.6998614,
   "Latitude": 105.1354499
 },
 {
   "STT": 1506,
   "Name": "Vạn Phong Nguyên",
   "address": "Tổ 17, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1507,
   "Name": "Thăng Long",
   "address": "Chợ Cái Dầu, tổ 29, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú,AN GIANG",
   "Longtitude": 10.5702578,
   "Latitude": 105.2387827
 },
 {
   "STT": 1508,
   "Name": "Cơ Sở Bảo Châu",
   "address": "Số 395/10A, tổ 2, khóm Tây Khánh 5, phường Mỹ Hòa, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 1509,
   "Name": "Thịnh Hưng Đường",
   "address": "Số 157A Hà Hoàng Hổ, khóm Đông Phú,phường Đông Xuyên, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3778076,
   "Latitude": 105.4315196
 },
 {
   "STT": 1510,
   "Name": "Vạn Xuân",
   "address": "Số 243 Trần Phú, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn,AN GIANG",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 1511,
   "Name": "Quãng Đức Nguyên",
   "address": "Ấp Phú Xương, thị trấn Chợ Vàm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.3413773
 },
 {
   "STT": 1512,
   "Name": "Quãng Đức Nguyên",
   "address": "Ấp Phú Xương, thị trấn Chợ Vàm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.3413773
 },
 {
   "STT": 1513,
   "Name": "Quãng Đức Nguyên 02",
   "address": "Ấp Phú Xương, thị trấn Chợ Vàm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.3413773
 },
 {
   "STT": 1514,
   "Name": "Quãng Đức Nguyên 02",
   "address": "Ấp Phú Xương, thị trấn Chợ Vàm, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.6948518,
   "Latitude": 105.3413773
 },
 {
   "STT": 1515,
   "Name": "Trường An Đường 2",
   "address": "Số 61 Ngô Quyền, tổ 7, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7708956,
   "Latitude": 105.2838511
 },
 {
   "STT": 1516,
   "Name": "Tam Sanh Đường",
   "address": "Số 36 Chu Văn An, ấp Thượng 2, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5977151,
   "Latitude": 105.3554973
 },
 {
   "STT": 1517,
   "Name": "Vạn Tế Đường",
   "address": "Tổ 46, ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4572188,
   "Latitude": 105.3324998
 },
 {
   "STT": 1519,
   "Name": "Phương Hòa Đường",
   "address": "Số 37 Chi Lăng, khóm 5, phường Châu Phú A, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7103327,
   "Latitude": 105.1180893
 },
 {
   "STT": 1520,
   "Name": "Hiệp Hòa Đường",
   "address": "Số 17 Sương Nguyệt Anh, phường Châu Phú A, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7126656,
   "Latitude": 105.1188806
 },
 {
   "STT": 1521,
   "Name": "Chung Tài Xinh",
   "address": "Số 1/4, Hòa Thạnh, phường Mỹ Thạnh, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3495679,
   "Latitude": 105.4622698
 },
 {
   "STT": 1522,
   "Name": "Thanh Trí",
   "address": "Số 81 Lê Minh Ngươn, phường Mỹ Long, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3836964,
   "Latitude": 105.44202
 },
 {
   "STT": 1523,
   "Name": "Dũ Trung Long",
   "address": "Số 13 Chi Lăng, phường Châu Phú A, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7113275,
   "Latitude": 105.119648
 },
 {
   "STT": 1524,
   "Name": "Hưng Long",
   "address": "Số 82-84 Đống Đa, phường Châu Phú A, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7109307,
   "Latitude": 105.1176746
 },
 {
   "STT": 1525,
   "Name": "Phục An Đường",
   "address": "Số 232, ấp Thị, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4330203,
   "Latitude": 105.5508873
 },
 {
   "STT": 1526,
   "Name": "Phục Hưng Đường",
   "address": "Ấp An Lạc, xã An Thạnh Trung, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 1527,
   "Name": "Đông Hòa 1",
   "address": "Nhà lồng Chợ Cái Dầu, Kios số 96, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú, AnGiang",
   "Longtitude": 10.5679451,
   "Latitude": 105.2415481
 },
 {
   "STT": 1528,
   "Name": "Hùng Dũng",
   "address": "Số 15 Nguyễn Văn Cưng, phường Mỹ Long, thành phố Long Xuyên,AN GIANG",
   "Longtitude": 10.3827148,
   "Latitude": 105.4408926
 },
 {
   "STT": 1529,
   "Name": "Cơ sở sản xuất và kinh doanh dầu gió TânThập",
   "address": "Số 97/29 Trưng Nữ Vương, phường Châu Phú B, thành phố Châu Đốc,AN GIANG",
   "Longtitude": 10.7067385,
   "Latitude": 105.1182045
 },
 {
   "STT": 1530,
   "Name": "Lê Loan",
   "address": "Số 27/15, Trung Thạnh, phường Mỹ Thới, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3368079,
   "Latitude": 105.4478131
 },
 {
   "STT": 1531,
   "Name": "Phước Sanh Đường",
   "address": "Số 5 Nguyễn Huệ, tổ 4, khóm Long Thị C, phường Long Hưng,TX.Tân Châu, AN GIANG",
   "Longtitude": 10.7958634,
   "Latitude": 105.2308219
 },
 {
   "STT": 1532,
   "Name": "Bình An Tường",
   "address": "Tổ 8, ấp Tân Phú B, xã Tân An, TX.Tân Châu, AN GIANG",
   "Longtitude": 10.8136609,
   "Latitude": 105.1970174
 },
 {
   "STT": 1533,
   "Name": "Sanh Sanh Đường",
   "address": "Số 4 Nguyễn Huệ, khóm Long Thị C, phường Long Hưng, TX.Tân Châu, AN GIANG",
   "Longtitude": 10.7958634,
   "Latitude": 105.2308219
 },
 {
   "STT": 1534,
   "Name": "Lai Đức Đường",
   "address": "Ấp Mỹ Hòa, xã Nhơn Mỹ, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.47895,
   "Latitude": 105.3902148
 },
 {
   "STT": 1535,
   "Name": "Điền Thị",
   "address": "Ấp Long Mỹ 1, xã Long Giang, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 1536,
   "Name": "Kiến Thị Hạnh 7",
   "address": "Số 45 Đinh Tiên Hoàng, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 1537,
   "Name": "Phúc Nguyên Đường",
   "address": "Số 486, ấp Mỹ Hội, xã Mỹ Hội Đông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 1538,
   "Name": "Tinh Thần",
   "address": "Số 77 Nguyễn Huệ, phường Mỹ Long, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3817085,
   "Latitude": 105.4405332
 },
 {
   "STT": 1539,
   "Name": "Phương Hòa Sanh",
   "address": "Số 6 Nguyễn Hữu Cảnh, phường Châu Phú A, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7107917,
   "Latitude": 105.1195889
 },
 {
   "STT": 1540,
   "Name": "Quỳnh Như",
   "address": "Số 1020/2d, tổ 11, Tây Khánh 7, phường Mỹ Hòa, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 1541,
   "Name": "Tinh Thần",
   "address": "Số 77 Nguyễn Huệ, phường Mỹ Long, thành phố Long Xuyên, AnGiang",
   "Longtitude": 10.3817085,
   "Latitude": 105.4405332
 },
 {
   "STT": 1542,
   "Name": "Tập Phát",
   "address": "Số 86 Lê Thị Nhiên, phường Mỹ Long, thành phố Long Xuyên, AnGiang",
   "Longtitude": 10.3839883,
   "Latitude": 105.4425156
 },
 {
   "STT": 1543,
   "Name": "Phương Hòa Sanh",
   "address": "Số 06 Nguyễn Hữu Cảnh, phường Châu Phú A, thành phố Châu Đốc, AnGiang",
   "Longtitude": 10.7107917,
   "Latitude": 105.1195889
 },
 {
   "STT": 1544,
   "Name": "Năm Luông",
   "address": "Số 05 Phan Chu Trinh, phường Mỹ Long, thành phố Long Xuyên, AnGiang",
   "Longtitude": 10.3839363,
   "Latitude": 105.4422516
 },
 {
   "STT": 1545,
   "Name": "Hiệp Hòa Đường",
   "address": "Số 17 Sương Nguyệt Anh, phường Châu Phú A, thành phố Châu Đốc, AnGiang",
   "Longtitude": 10.7126656,
   "Latitude": 105.1188806
 },
 {
   "STT": 1546,
   "Name": "Xuân Tứ Hiếu",
   "address": "Tổ 04, ấp Vĩnh Thạnh B, xã Vĩnh Hòa, TX.Tân Châu, AN GIANG",
   "Longtitude": 10.8528973,
   "Latitude": 105.1785307
 },
 {
   "STT": 1547,
   "Name": "Hạnh Chương",
   "address": "Số 03 Phan Chu Trinh, phường Mỹ Long, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3839831,
   "Latitude": 105.4423099
 },
 {
   "STT": 1548,
   "Name": "Phương Lam",
   "address": "Số 13/9, khóm Hòa Thạnh, phường Mỹ Thạnh, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3314328,
   "Latitude": 105.4830461
 },
 {
   "STT": 1549,
   "Name": "Vạn Hòa Đường",
   "address": "Số 220 Trần Hưng Đạo, phường Châu Phú A, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7137522,
   "Latitude": 105.1176832
 },
 {
   "STT": 1550,
   "Name": "Vạn Hòa Đường",
   "address": "Số 220 Trần Hưng Đạo, phường Châu Phú A, thành phố Châu Đốc, AN GIANG",
   "Longtitude": 10.7137522,
   "Latitude": 105.1176832
 },
 {
   "STT": 1551,
   "Name": "Trường An Đường 2",
   "address": "Số 61 Ngô Quyền, tổ  07, khóm Long Thạnh A, phường Long Thạnh, TX.Tân Châu, AN GIANG",
   "Longtitude": 10.7708956,
   "Latitude": 105.2838511
 },
 {
   "STT": 1552,
   "Name": "Kim Ngân",
   "address": "Số 462A/22, khóm Bình Đức 5, phường Bình Đức, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.4256524,
   "Latitude": 105.4037336
 },
 {
   "STT": 1553,
   "Name": "Thái Sơn",
   "address": "Số 549, tổ 24, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1554,
   "Name": "Lộc Quí",
   "address": "Số 72/3, khóm Trà Sư, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6248148,
   "Latitude": 105.0067314
 },
 {
   "STT": 1555,
   "Name": "Thanh Danh",
   "address": "Số 73/3, khóm Trà Sư, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6248148,
   "Latitude": 105.0067314
 },
 {
   "STT": 1556,
   "Name": "Hoàng Minh",
   "address": "Tổ 2, khóm Thới Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 1557,
   "Name": "Trường An Đường",
   "address": "Số 94 Nguyễn Công Nhàn, khóm Long Thị B, phường Long Hưng, TX.Tân Châu, AN GIANG",
   "Longtitude": 10.7979027,
   "Latitude": 105.2387666
 },
 {
   "STT": 1558,
   "Name": "Trường An Đường",
   "address": "Số 94 Nguyễn Công Nhàn, khóm Long Thị B, phường Long Hưng, TX.Tân Châu, AN GIANG",
   "Longtitude": 10.7979027,
   "Latitude": 105.2387666
 },
 {
   "STT": 1559,
   "Name": "Vĩnh Chương",
   "address": "Số 2 Trương Định, phường Châu Phú B,thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7052044,
   "Latitude": 105.1290018
 },
 {
   "STT": 1560,
   "Name": "Phước Điền",
   "address": "Số 81 Hải Thượng Lãn Ông, ấp Mỹ Lương, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5974514,
   "Latitude": 105.3557475
 },
 {
   "STT": 1561,
   "Name": "Quách Tất",
   "address": "Ấp Thị 2, xã Hội An, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 1562,
   "Name": "Hạnh Sanh Đường",
   "address": "Ấp Thị, xã Mỹ Hiệp, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.493561,
   "Latitude": 105.556299
 },
 {
   "STT": 1563,
   "Name": "Trường Thọ",
   "address": "Ấp Tân Thạnh, thị trấn Long Bình, huyện An Phú, AN GIANG",
   "Longtitude": 10.9470064,
   "Latitude": 105.0881767
 },
 {
   "STT": 1564,
   "Name": "Sáu Xứng",
   "address": "Số 93/3, khóm Trà Sư, thị trấn Nhà Bàng, huyện Tịnh Biên, AnGiang",
   "Longtitude": 10.6248148,
   "Latitude": 105.0067314
 },
 {
   "STT": 1565,
   "Name": "Khoa Thi",
   "address": "Số 441 Trà Sư, khóm Thới Hòa, thị trấn Nhà Bàng, huyện Tịnh Biên,AN GIANG",
   "Longtitude": 10.6184409,
   "Latitude": 104.9985426
 },
 {
   "STT": 1566,
   "Name": "Cửa Hàng Dược Liệu",
   "address": "Số 256/23 Thoại Ngọc Hầu, phường Mỹ Phước, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3806719,
   "Latitude": 105.4440519
 },
 {
   "STT": 1568,
   "Name": "Tám Tỷ",
   "address": "Số 36 Phan Đình Phùng B, phường Mỹ Long, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3840778,
   "Latitude": 105.4418613
 },
 {
   "STT": 1569,
   "Name": "Vĩnh Xuân Hòa",
   "address": "Số 512, tổ 5, ấp Tân Hiệp A, thị trấn Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1570,
   "Name": "Khôn Hòa",
   "address": "Số 515 Nguyễn Huệ, ấp Đông Sơn 2, thị trấn Núi Sập, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2649225,
   "Latitude": 105.2656198
 },
 {
   "STT": 1571,
   "Name": "Vạn An Đường",
   "address": "Số 14 Phan Đình Phùng, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3840398,
   "Latitude": 105.4412622
 },
 {
   "STT": 1572,
   "Name": "Vĩnh An Đường",
   "address": "Số 2/2 Nguyễn Trãi, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3818267,
   "Latitude": 105.4438612
 },
 {
   "STT": 1573,
   "Name": "Bá Phước Đường",
   "address": "Số 22 Trường Chinh, khóm Long Thạnh A, phường Long Thạnh, Tx. Tân Châu, AN GIANG",
   "Longtitude": 10.7992409,
   "Latitude": 105.2496328
 },
 {
   "STT": 1574,
   "Name": "Minh Châu",
   "address": "Số 99/3, khóm Trà Sư, thị trấn Nhà Bàng, huyện Tịnh Biên, AN GIANG",
   "Longtitude": 10.6248148,
   "Latitude": 105.0067314
 },
 {
   "STT": 1575,
   "Name": "Nhơn Hòa Đường",
   "address": "Tổ 10, ấp Vĩnh Lộc, xã Vĩnh Bình, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4470354,
   "Latitude": 105.172579
 },
 {
   "STT": 1576,
   "Name": "Võ Kim Ánh",
   "address": "Số 17/8, khóm Hòa Thạnh, phường Mỹ Thạnh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3495679,
   "Latitude": 105.4622698
 },
 {
   "STT": 1577,
   "Name": "Vạn Tế Đường",
   "address": "Số 12, ấp Thị, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5474757,
   "Latitude": 105.4053436
 },
 {
   "STT": 1578,
   "Name": "Vạn Tế Đường",
   "address": "Số 30, ấp Thị 1, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4993584,
   "Latitude": 105.4830539
 },
 {
   "STT": 1579,
   "Name": "Nguyễn Văn Tấn Tước",
   "address": "Số 81, tổ 3, ấp Long Hòa, xã Long Điền B, huyện Chợ Mới, AnGiang",
   "Longtitude": 10.5112548,
   "Latitude": 105.4478131
 },
 {
   "STT": 1580,
   "Name": "Đại Nguyên Đường",
   "address": "Số 5 Phạm Hồng Thái, thị trấn Chợ Mới, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5508514,
   "Latitude": 105.4000049
 },
 {
   "STT": 1581,
   "Name": "Vạn Hưng Đường",
   "address": "Ấp Tắc Trúc, xã Nhơn Hội, huyện An Phú, AN GIANG",
   "Longtitude": 10.9105865,
   "Latitude": 105.0538921
 },
 {
   "STT": 1582,
   "Name": "Vĩnh Xuân Hòa 2",
   "address": "Số 808, ấp Tân Hiệp A, thị trấn Óc Eo, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1583,
   "Name": "Hải Anh 2",
   "address": "Ấp An Hưng, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 1584,
   "Name": "Phúc Huy",
   "address": "Số 95/3C Trần Hưng Đạo, phường Mỹ Phước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3788972,
   "Latitude": 105.4394416
 },
 {
   "STT": 1585,
   "Name": "Đức Thành",
   "address": "Tổ 9, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 1586,
   "Name": "Xuân Hoà Đường",
   "address": "Số 340 Thoại Ngọc Hầu, ấp An Hưng, thị trấn An Phú, huyện An Phú, AN GIANG",
   "Longtitude": 10.8147065,
   "Latitude": 105.0919613
 },
 {
   "STT": 1587,
   "Name": "Thái An Đường",
   "address": "Số 35, tổ 2, ấp Hòa Long 4, thị trấn An Châu, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 1588,
   "Name": "Hưng Hòa Đường",
   "address": "Lô 3, đường Lý Thái Tổ, phường Mỹ Long, thành phố  Long Xuyên, AnGiang",
   "Longtitude": 10.3794183,
   "Latitude": 105.4448971
 },
 {
   "STT": 1589,
   "Name": "Tuyết Hạnh",
   "address": "Số 23L3 Phạm Văn Đồng, phường MỹPhước, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3638144,
   "Latitude": 105.434756
 },
 {
   "STT": 1590,
   "Name": "Phương Hòa Đường",
   "address": "Số 21 Chi Lăng, phường Châu Phú A, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7103371,
   "Latitude": 105.1180956
 },
 {
   "STT": 1591,
   "Name": "Thái Hữu",
   "address": "Số 813 Chu Văn An, ấp Trung Thạnh, thị trấn Phú Mỹ, huyện Phú Tân, AN GIANG",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 1592,
   "Name": "Duy Khương",
   "address": "Tổ 9, Chợ Mỹ Đức, ấp Mỹ Thiện, xã Mỹ Đức, huyện Châu Phú, AN GIANG",
   "Longtitude": 10.6659713,
   "Latitude": 105.1798717
 },
 {
   "STT": 1593,
   "Name": "Vạn An Xuân 1",
   "address": "Số 20, tổ 8, ấp Thị 1, thị trấn Mỹ Luông, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 1594,
   "Name": "Minh Đức 2",
   "address": "Số 725 Hà Hoàng Hổ, phường Đông Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.375899,
   "Latitude": 105.418358
 },
 {
   "STT": 1595,
   "Name": "Nguyễn Thị Trúc Ly",
   "address": "Số 14 Nguyễn Huệ A, phường Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3817085,
   "Latitude": 105.4405332
 },
 {
   "STT": 1596,
   "Name": "Nguyễn Mai",
   "address": "Số 46/3 Phan Bội Châu, phường Bình Khánh, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.4004383,
   "Latitude": 105.4205636
 },
 {
   "STT": 1597,
   "Name": "Đại lý thuốc số 001",
   "address": "Tổ 6, ấp Hòa Thạnh, xã Hòa Bình Thạnh, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.4125083,
   "Latitude": 105.348246
 },
 {
   "STT": 1598,
   "Name": "Đại lý thuốc số 006",
   "address": "Số 553, ấp Bắc Thạnh, xã Thoại Giang, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2655716,
   "Latitude": 105.2311827
 },
 {
   "STT": 1599,
   "Name": "Đại lý thuốc số 61",
   "address": "Ấp Đông Bình Nhất, xã Vĩnh Thành, huyện Châu Thành, AN GIANG",
   "Longtitude": 10.358482,
   "Latitude": 105.368724
 },
 {
   "STT": 1600,
   "Name": "Nhà thuốc Thu Thảo",
   "address": "Thửa số 57, Trần Hưng Đạo, P. Bình Đức, thành phố Long Xuyên, tỉnh AnGiang",
   "Longtitude": 10.3895169,
   "Latitude": 105.430437
 },
 {
   "STT": 1601,
   "Name": "Nhà thuốc Thịnh Vượng",
   "address": "159, Hàm Nghi, P.Bình Khánh, thành phố  Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3932647,
   "Latitude": 105.4212525
 },
 {
   "STT": 1602,
   "Name": "Nhà thuốc Thái Bình 2",
   "address": "3/B3 Nguyễn Huệ, P.Mỹ Long, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3817085,
   "Latitude": 105.4405332
 },
 {
   "STT": 1603,
   "Name": "Nhà thuốc Gia Phú",
   "address": "2996/17, hẻm Trần Hưng Đạo, P. Mỹ Thạnh, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3372626,
   "Latitude": 105.4748212
 },
 {
   "STT": 1604,
   "Name": "Nhà thuốc Bình Minh",
   "address": "161, Hà Hoàng Hổ, P.Đông Xuyên,  thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3775082,
   "Latitude": 105.422901
 },
 {
   "STT": 1605,
   "Name": "Nhà thuốc Anh Tuấn",
   "address": "798, đường vòng Núi Sam, P.Núi Sam, thành phố Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.6812134,
   "Latitude": 105.0809523
 },
 {
   "STT": 1606,
   "Name": "Nhà thuốc số 91",
   "address": "91, Lê Lợi, P.Châu Phú B, thành phố Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.7046782,
   "Latitude": 105.1304708
 },
 {
   "STT": 1607,
   "Name": "Quầy thuốc Huệ Tâm",
   "address": "Tổ 12, Bình Hòa 2, xã Mỹ Khánh, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3792302,
   "Latitude": 105.3872573
 },
 {
   "STT": 1608,
   "Name": "Quầy thuốc Duy Khánh",
   "address": "Tổ 19, Mỹ Khánh 2, xã mỹ Hòa Hưng, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3864043,
   "Latitude": 105.3804526
 },
 {
   "STT": 1609,
   "Name": "Quầy thuốc Ngọc Phê",
   "address": "Tổ 14, Mỹ An 2, xã Mỹ Hòa Hưng, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3990635,
   "Latitude": 105.4426552
 },
 {
   "STT": 1611,
   "Name": "Cơ sở Minh Châu",
   "address": "Sạp số 016-018, lô C, trung tâm thương mại thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.8423542,
   "Latitude": 105.1843501
 },
 {
   "STT": 1612,
   "Name": "Quầy thuốc Thúy Hằng",
   "address": "81, tổ 2, ấp Hòa Thạnh, xã Châu Phong, TX.Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.7340814,
   "Latitude": 105.1434387
 },
 {
   "STT": 1613,
   "Name": "Quầy thuốc Minh Hải",
   "address": "Số 109, tổ 4, ấp Bờ Dâu, xã thạnh Mỹ Tây, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 1614,
   "Name": "Quầy thuốc Huỳnh Loan",
   "address": "Tổ 04, ấp Trung Phú 3, xã Vĩnh Phú, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3527541,
   "Latitude": 105.2311827
 },
 {
   "STT": 1615,
   "Name": "Quầy thuốc Ba Khanh",
   "address": "Số 216, ấp Tân Hiệp A, TT Óc Eo, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1616,
   "Name": "Quầy thuốc Hoàng Kim",
   "address": "Số 520, ấp Tân Hiệp A, TT Óc Eo, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1617,
   "Name": "Quầy thuốc Tiến Đạt",
   "address": "Tổ 13, ấp Tân Hiệp B, TT Óc Eo, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 1618,
   "Name": "Quầy thuốc Hiền Trang",
   "address": "Tổ 03, ấp Phú Thuận, xã Tây Phú, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3354326,
   "Latitude": 105.1375908
 },
 {
   "STT": 1619,
   "Name": "Quầy thuốc Phương Nhì",
   "address": "Số 234, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.2760432,
   "Latitude": 105.3508203
 },
 {
   "STT": 1620,
   "Name": "Quầy thuốc Tấn Hiệp",
   "address": "Số 42, ấp Kiến Hưng 2, xã Kiến Thành, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1621,
   "Name": "Quầy thuốc Nhân Bình",
   "address": "Số 251, quốc lộ 91, Hòa Phú 3, thị trấn An Châu, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.440019,
   "Latitude": 105.392736
 },
 {
   "STT": 1622,
   "Name": "Quầy thuốc Kim Chi",
   "address": "Tổ 39,ấp Phú Hòa 1, xã Bình Hòa, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.463034,
   "Latitude": 105.342453
 },
 {
   "STT": 1623,
   "Name": "Quầy thuốc Huỳnh Anh",
   "address": "Tổ 01, ấp Hòa Thạnh, xã Hòa Bình Thạnh, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.4125083,
   "Latitude": 105.348246
 },
 {
   "STT": 1624,
   "Name": "Quầy thuốc Hồng Vân",
   "address": "Tổ 6, ấp Thạnh Phú, xã Bình Thạnh, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.4693515,
   "Latitude": 105.357029
 },
 {
   "STT": 1625,
   "Name": "Quầy thuốc Bé Ba",
   "address": "179/9. ấp Vĩnh Trung, xã Vĩnh Trạch, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3310367,
   "Latitude": 105.3423909
 },
 {
   "STT": 1626,
   "Name": "Quầy thuốc An Khang",
   "address": "Tổ 3, ấp Sốc Triết, xã Cô Tô, huyện Tri Tôn, tỉnh AN GIANG",
   "Longtitude": 10.3501846,
   "Latitude": 105.0384458
 },
 {
   "STT": 1627,
   "Name": "Quầy thuốc Thùy Dương",
   "address": "Ấp Long Phú 2, xã Long Điền B, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5104664,
   "Latitude": 105.4448701
 },
 {
   "STT": 1628,
   "Name": "Quầy thuốc Diễm Châu",
   "address": "Số 88, ấp Voi 1, xã Núi Voi, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.5360404,
   "Latitude": 105.0460902
 },
 {
   "STT": 1629,
   "Name": "Quầy thuốc Huỳnh Hiền",
   "address": "Tổ 12, ấp Tây Bình, xã Vĩnh Trạch, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3517339,
   "Latitude": 105.3172747
 },
 {
   "STT": 1630,
   "Name": "Quầy thuốc Kim Liên",
   "address": "Tổ 14, ấp Trung Phú 2, xã Vĩnh Phú, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3525,
   "Latitude": 105.2311111
 },
 {
   "STT": 1631,
   "Name": "Quầy thuốc Hoài Nhân",
   "address": "347, tổ 2, ấp Tân Thành, xã Vọng Thuê, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.275753,
   "Latitude": 105.1317431
 },
 {
   "STT": 1632,
   "Name": "Quầy thuốc Hoa Sen",
   "address": "số 568, tổ 21, ấp An Thái, xã Hòa Bình, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1633,
   "Name": "Quầy thuốc Ngọc Huyên",
   "address": "số 46 ,tổ 1, ấp An Bình, xã Hòa Bình, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1634,
   "Name": "Quầy thuốc Huy Hà",
   "address": "số 220, tổ 10, ấp Long Hòa 1, xã Long Điền A, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 1635,
   "Name": "Quầy thuốc Bích Liên",
   "address": "ấp Long Bình, xã Kiến An, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5419444,
   "Latitude": 105.3733333
 },
 {
   "STT": 1636,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "511A, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.3935508,
   "Latitude": 105.4566962
 },
 {
   "STT": 1637,
   "Name": "Quầy thuốc Tú Trinh",
   "address": "ấp Vĩnh hội, xã Vĩnh Hội Đông, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.7923518,
   "Latitude": 105.0732771
 },
 {
   "STT": 1638,
   "Name": "Quầy thuốc Kim Nên",
   "address": "ấp Phú Nghĩa, xã Phú Hội, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.8142824,
   "Latitude": 105.0732771
 },
 {
   "STT": 1639,
   "Name": "Quầy thuốc Châu Nhiên",
   "address": "ấp An Hòa, xã Khánh An, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 1640,
   "Name": "Quầy thuốc Út Sang",
   "address": "70, ấp Bình Hòa, Quốc Lộ 91, thị trấn Cái Dầu, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5671715,
   "Latitude": 105.249289
 },
 {
   "STT": 1641,
   "Name": "Quầy thuốc Thủy Danh",
   "address": "949/15, tổ 8, ấp Bình Hòa 2, xã Mỹ Khánh, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3861185,
   "Latitude": 105.386309
 },
 {
   "STT": 1642,
   "Name": "Nhà thuốc Tiền Hồ 14",
   "address": "671, Trần Hưng Đạo, phường bình Đức 1, thành phố Long Xuyên,tỉnh AN GIANG",
   "Longtitude": 10.399945,
   "Latitude": 105.4202345
 },
 {
   "STT": 1643,
   "Name": "Quầy thuốc Kiều Trân",
   "address": "381, tổ 13, ấp Bình Khánh, xã Mỹ Khánh, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3953269,
   "Latitude": 105.4208821
 },
 {
   "STT": 1644,
   "Name": "Quầy thuốc Huỳnh Châu",
   "address": "tổ 19, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5796614,
   "Latitude": 105.2325225
 },
 {
   "STT": 1645,
   "Name": "Nhà thuốc Hồng Anh",
   "address": "3A2, Lý Thái Tổ,phường Mỹ Long, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3778032,
   "Latitude": 105.4420386
 },
 {
   "STT": 1646,
   "Name": "Quầy thuốc Nguyễn Phí",
   "address": "số 172,ấp Phú Lộc, xã Phú Thạnh, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.6574189,
   "Latitude": 105.2545888
 },
 {
   "STT": 1647,
   "Name": "Quầy thuốc Trúc Mai",
   "address": "số 65, ấp Phú Cường B, xã Phú Thạnh, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.7157434,
   "Latitude": 105.2900096
 },
 {
   "STT": 1648,
   "Name": "Quầy thuốc Thúy An",
   "address": "tổ 12, ấp Hòa Bình 3, xã Hòa Lạc, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.667617,
   "Latitude": 105.219466
 },
 {
   "STT": 1649,
   "Name": "Quầy thuốc Cẩm Tú",
   "address": "tổ 02, ấp Bình Phú 2, xã Phú Bình, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5933054,
   "Latitude": 105.2493604
 },
 {
   "STT": 1650,
   "Name": "Quầy thuốc Kim Anh",
   "address": "ấp Bình Phú 2, xã Phú Bình, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.623492,
   "Latitude": 105.2253342
 },
 {
   "STT": 1651,
   "Name": "Quầy thuốc Thúy Hằng",
   "address": "số 304, ấp Bình Trung 1, xã Bình Thạnh Đông, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 1652,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "ấp Hậu Giang 1, xã Tân Hòa, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5647783,
   "Latitude": 105.3206126
 },
 {
   "STT": 1653,
   "Name": "Quầy thuốc Trang Lộc",
   "address": "990, ấp Vĩnh Lợi 1, xã Châu Phong, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.7340814,
   "Latitude": 105.1434387
 },
 {
   "STT": 1654,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "tổ 11, ấp Tân Hòa C, xã Tân An, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.8423542,
   "Latitude": 105.1843501
 },
 {
   "STT": 1655,
   "Name": "Quầy thuốc Tuyết Loan",
   "address": "ấp Vĩnh Trường 2, xã Châu Phong, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.8031464,
   "Latitude": 105.194835
 },
 {
   "STT": 1656,
   "Name": "Quầy thuốc Diễm Tùng",
   "address": "tổ 4, ấp Hòa Long, xã Châu Phong, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.7340814,
   "Latitude": 105.1434387
 },
 {
   "STT": 1657,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "tổ 9, ấp Vĩnh Khánh II, xã Vĩnh Tế, thành phố Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.6597328,
   "Latitude": 105.0514519
 },
 {
   "STT": 1658,
   "Name": "Nhà thuốc Thái Phiên",
   "address": "số 18, đường số 1, KDC chợ phường Châu Phú B, thành phố Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.7025293,
   "Latitude": 105.1241706
 },
 {
   "STT": 1659,
   "Name": "Nhà thuốc Lê Mỹ",
   "address": "số 283, Tôn Đức Thắng, phường Long Thạnh, thị xã Tân Châu, tỉnh AnGiang",
   "Longtitude": 10.7991208,
   "Latitude": 105.2451032
 },
 {
   "STT": 1660,
   "Name": "Cơ sở Đại Quang",
   "address": "đường Ngô Quyền, khóm 3, thị trấn Tri Tôn, huyện Tri Tôn, tỉnh AN GIANG",
   "Longtitude": 10.4262393,
   "Latitude": 104.9989988
 },
 {
   "STT": 1661,
   "Name": "Quầy thuốc Hê Lam",
   "address": "ấp thị 2, thị trấn Chợ Mới, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5420745,
   "Latitude": 105.400087
 },
 {
   "STT": 1663,
   "Name": "Quầy thuốc Hữu Có",
   "address": "số 539, tổ 21, ấp Kiến Bình 1, xã Kiến An, huyện chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5459241,
   "Latitude": 105.3601822
 },
 {
   "STT": 1664,
   "Name": "Quầy thuốc Thái Thiện",
   "address": "số 1606, ấp Mỹ Hòa A, xã Mỹ Hội Đông, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 1665,
   "Name": "Quầy thuốc Kim Cương",
   "address": "ấp Cần Thạnh, xã Cần Đăng, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.4575621,
   "Latitude": 105.2938458
 },
 {
   "STT": 1666,
   "Name": "Quầy thuốc Cẩm Tú",
   "address": "Tổ 6, ấp Thạnh Phú, xã Bình Thạnh, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.4693515,
   "Latitude": 105.357029
 },
 {
   "STT": 1667,
   "Name": "Quầy thuốc Nguyên Phát",
   "address": "ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 1668,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "tổ 18, ấp 2, xã Vĩnh Xương, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 1669,
   "Name": "Quầy thuốc Phước Thành 1",
   "address": "số 843, ấp Trung Thạnh, thị trấn Phú Mỹ, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 1670,
   "Name": "Quầy thuốc Phú Triệu",
   "address": "số 225, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.2760432,
   "Latitude": 105.3508203
 },
 {
   "STT": 1671,
   "Name": "Quầy thuốc Thái Châu",
   "address": "tổ 11, ấp 4, xã Vĩnh Xương, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 1672,
   "Name": "Quầy thuốc Diệu Hiền",
   "address": "ấp Hiệp Hòa, xã Hiệp Xương, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5932012,
   "Latitude": 105.2799187
 },
 {
   "STT": 1673,
   "Name": "Quầy thuốc Lê Huỳnh",
   "address": "ấp Mỹ Hóa 2, xã Tân Hòa, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5690667,
   "Latitude": 105.3277541
 },
 {
   "STT": 1674,
   "Name": "Cơ Sở Vạn Tường",
   "address": "số 15/5B, Bùi Thị Xuân, phường Mỹ Xuyên, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3796141,
   "Latitude": 105.4373756
 },
 {
   "STT": 1675,
   "Name": "Cơ Sở Kim Đào",
   "address": "số 37 Bis, Đông An 5, phường Mỹ Xuyên, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3765234,
   "Latitude": 105.4405375
 },
 {
   "STT": 1676,
   "Name": "Nhà thuốc Phi Phụng",
   "address": "số 136-138, Lê Minh Ngươn, phường Mỹ Long, thành phố LongXuyên, tỉnh AN GIANG",
   "Longtitude": 10.3842493,
   "Latitude": 105.4414179
 },
 {
   "STT": 1677,
   "Name": "Quầy thuốc Mai Trang",
   "address": "số 150, ấp Long Thuận, xã Long Điền B, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5112548,
   "Latitude": 105.4478131
 },
 {
   "STT": 1678,
   "Name": "Nhà thuốc Hữu Phát",
   "address": "Tây Khánh 8, KDC Mỹ Hòa, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3603108,
   "Latitude": 105.4126652
 },
 {
   "STT": 1679,
   "Name": "Nhà thuốc Lam Quỳnh 52",
   "address": "số 780/30, Thoại Ngọc Hầu, khóm Mỹ Lộc, phường Mỹ Phước, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.38058,
   "Latitude": 105.4441121
 },
 {
   "STT": 1680,
   "Name": "Nhà thuốc Nhân Tín",
   "address": "Số 18, đường Nguyễn Văn Thoại, khóm 5, phường Châu Phú A, thị xã Châu Đốc, tỉnh AnGiang",
   "Longtitude": 10.7065221,
   "Latitude": 105.1144463
 },
 {
   "STT": 1681,
   "Name": "Nhà thuốc Hồng Trang",
   "address": "số 2A, đường Phan Văn Vàng, phường Châu Phú B, thị xã Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.7094212,
   "Latitude": 105.1184481
 },
 {
   "STT": 1682,
   "Name": "Quầy thuốc Đoàn Xuân",
   "address": "tổ 6, ấp Phú Hữu, thị trấn Phú Hòa, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3582024,
   "Latitude": 105.3796255
 },
 {
   "STT": 1683,
   "Name": "Quầy thuốc Siêm",
   "address": "ấp Đồng Ky, xã Quốc Thái, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1684,
   "Name": "Quầy thuốc Phú Sĩ 2",
   "address": "số 07, ấp Trung 1, thị trấn Phú Mỹ, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5825573,
   "Latitude": 105.3567219
 },
 {
   "STT": 1685,
   "Name": "Quầy thuốc Thành Đạt",
   "address": "ấp Phước Thọ, xã Đa Phước, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.7173664,
   "Latitude": 105.12151
 },
 {
   "STT": 1686,
   "Name": "Quầy thuốc Bế Dung 2",
   "address": "ấp Đồng Ky, xã Quốc Thái, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1687,
   "Name": "Quầy thuốc Huỳnh Tấn",
   "address": "tổ 09, Vĩnh Thuận, Vĩnh Châu, thành phố Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.6410073,
   "Latitude": 105.0878917
 },
 {
   "STT": 1688,
   "Name": "Quầy thuốc Huỳnh Duy",
   "address": "tổ 12, ấp Vĩnh Thuận, xã Vĩnh Nhuận, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 1689,
   "Name": "Quầy thuốc Kim Nguyên",
   "address": "tổ 8, ấp Khánh Bình, xã Khánh Hòa, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.6442088,
   "Latitude": 105.2125434
 },
 {
   "STT": 1690,
   "Name": "Quầy thuốc Minh Hiền",
   "address": "ấp Bình Thạnh, xã Bình Chánh, huyện Châu Phú, tình AN GIANG",
   "Longtitude": 10.5156732,
   "Latitude": 105.2907101
 },
 {
   "STT": 1691,
   "Name": "Quầy thuốc Ngọc",
   "address": "tổ 02, ấp Chánh Hưng, xã Bình Mỹ, huyện châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5613542,
   "Latitude": 105.2559563
 },
 {
   "STT": 1692,
   "Name": "Quầy thuốc Mỹ Ngọc",
   "address": "135/4, ấp Bình Chánh 2, xã Bình Mỹ, huyệnChâu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 1693,
   "Name": "Quầy thuốc Ngọc Hiền",
   "address": "tổ 01, ấp Hòa Lợi 2, xã Vĩnh Lợi, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.3747313,
   "Latitude": 105.3132033
 },
 {
   "STT": 1695,
   "Name": "Quầy thuốc Phước Thành",
   "address": "tổ 9, khóm Hòa Thuận, thị trấn Nhà Bàng, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 1696,
   "Name": "Quầy thuốc Kim Ngọc",
   "address": "282, tổ 5, khóm Xuân Phú, thị trấn Tịnh Biên, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 1697,
   "Name": "Quầy thuốc Trúc Ly",
   "address": "tổ 02, ấp Phú Nhất, xã An Phú, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.6400127,
   "Latitude": 104.9819662
 },
 {
   "STT": 1698,
   "Name": "Quầy thuốc Anh Tính",
   "address": "ấp Tân An, xã Tân Tuyến, huyện Tri Tôn, tỉnh AN GIANG",
   "Longtitude": 10.3375,
   "Latitude": 105.090556
 },
 {
   "STT": 1699,
   "Name": "Quầy thuốc Mỹ Lộc",
   "address": "Lô 22 L8, đường số 3, ấp Mỹ khánh, xã Mỹ Hòa Hưng, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3864043,
   "Latitude": 105.3804526
 },
 {
   "STT": 1700,
   "Name": "Quầy thuốc Hồng Ngọc",
   "address": "số 14, ấp Mỹ An 1, xã Mỹ Hòa Hưng, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3990635,
   "Latitude": 105.4426552
 },
 {
   "STT": 1701,
   "Name": "Nhà thuốc Phương Khanh",
   "address": "số 46, Ngô Gia Tự, phường Mỹ Long, thành phố Long Xuyên, tỉnhAN GIANG",
   "Longtitude": 10.383691,
   "Latitude": 105.4409032
 },
 {
   "STT": 1702,
   "Name": "Quầy thuốc Đình Đình",
   "address": "Tổ 01, ấp An Bình, xã Hòa Bình, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1703,
   "Name": "Quầy thuốc Hội Hiền",
   "address": "Đường dẫn cầu Ông Chưởng, ấp thị 2, thị trấn Chợ Mới, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5512096,
   "Latitude": 105.3934882
 },
 {
   "STT": 1704,
   "Name": "Quầy thuốc Phước Sang",
   "address": "ấp Trung Châu, xã Mỹ Hiệp, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 1705,
   "Name": "Quầy thuốc Trúc Phương",
   "address": "ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.389722,
   "Latitude": 105.471111
 },
 {
   "STT": 1706,
   "Name": "Quầy thuốc Phú Anh",
   "address": "số 39, tổ 01, ấp Nhơn Hiệp, xã Nhơn Mỹ, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.4756645,
   "Latitude": 105.3950939
 },
 {
   "STT": 1707,
   "Name": "Quầy thuốc Minh Tâm",
   "address": "Ấp Thị 1, thị trấn Chợ Mới, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5509424,
   "Latitude": 105.3975065
 },
 {
   "STT": 1708,
   "Name": "Nhà thuốc Kim Ánh",
   "address": "Số 09, Thủ Khoa Nghĩa, P. Châu Phú A, thành phố  Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.715356,
   "Latitude": 105.1120699
 },
 {
   "STT": 1709,
   "Name": "Quầy thuốc Ánh Ngọc",
   "address": "Ấp Hòa An, xã Hòa Lạc, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.6752813,
   "Latitude": 105.2253316
 },
 {
   "STT": 1710,
   "Name": "Quầy thuốc Nguyễn Tiền",
   "address": "Số 317, tổ 14, ấp Kiến Hưng 2, xã Kiến Thành, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1711,
   "Name": "Quầy thuốc Phương Nghi",
   "address": "Ấp Vĩnh Phú, xã Vĩnh Thạnh Trung, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.576247,
   "Latitude": 105.2140903
 },
 {
   "STT": 1712,
   "Name": "Quầy thuốc Ngọc Chuỗi",
   "address": "Ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5164544,
   "Latitude": 105.3214531
 },
 {
   "STT": 1713,
   "Name": "Quầy thuốc Phúc Tuyền",
   "address": "Tổ 20, ấp Mỹ Thuận, xã Mỹ Phú, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 1714,
   "Name": "Quầy thuốc Thành Duy",
   "address": "Số 423, tổ 19, ấp Vĩnh Lợi, xã Vĩnh Hanh, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.3929765,
   "Latitude": 105.3014109
 },
 {
   "STT": 1715,
   "Name": "Quầy thuốc Ánh Hồng",
   "address": "Tổ 10, ấp Cần Thới, xã Cần Đăng, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.4531117,
   "Latitude": 105.2952936
 },
 {
   "STT": 1716,
   "Name": "Quầy thuốc Trọng Huy",
   "address": "số 227, tổ 12, ấp Phú Xương, thị trấn Chợ Vàm, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 1717,
   "Name": "Quầy thuốc Hùng Thảo",
   "address": "Tổ 14, ấp Trung Phú 2, xã Vĩnh Phú, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3525,
   "Latitude": 105.2311111
 },
 {
   "STT": 1718,
   "Name": "Quầy thuốc Tâm Đức số 1",
   "address": "số 26, ấp Trung Bình, xã Thoại Giang, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.2655716,
   "Latitude": 105.2311827
 },
 {
   "STT": 1719,
   "Name": "Quầy thuốc Nguyễn Huỳnh 9",
   "address": "Tỉnh lộ 943, ấp Thanh Niên, thị trấn Phú Hòa, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3479739,
   "Latitude": 105.386309
 },
 {
   "STT": 1720,
   "Name": "Nhà thuốc Hiệp Thành",
   "address": "số 148, Lê Minh Ngươn, phường Mỹ Long, thành phố Long Xuyên, tỉnhAN GIANG",
   "Longtitude": 10.3837582,
   "Latitude": 105.4418036
 },
 {
   "STT": 1721,
   "Name": "Nhà thuốc Hải Dương",
   "address": "số 183, Lý Thái Tổ, phường Mỹ Bình, thành phố Long Xuyên, tỉnhAN GIANG",
   "Longtitude": 10.3806378,
   "Latitude": 105.4463392
 },
 {
   "STT": 1722,
   "Name": "Nhà thuốc Nhân Tâm",
   "address": "số 01, Phan Bá Vành, phường Mỹ Bình, thành phố Long Xuyên, tỉnhAN GIANG",
   "Longtitude": 10.3875482,
   "Latitude": 105.4352479
 },
 {
   "STT": 1723,
   "Name": "Nhà thuốc Huy Thông",
   "address": "16 D4, Hồ Nguyên Trừng, phường Mỹ Quý, thành phố Long Xuyên,tỉnh AN GIANG",
   "Longtitude": 10.36208,
   "Latitude": 105.4524494
 },
 {
   "STT": 1724,
   "Name": "Quầy thuốc Huy Khôi",
   "address": "số 137/4, ấp Vĩnh Lập, xã Vĩnh Trung, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.5531636,
   "Latitude": 105.0382076
 },
 {
   "STT": 1725,
   "Name": "Nhà thuốc AGIMEXPHAR M 4",
   "address": "Số 01, Đinh Tiên Hoàng, phường Mỹ Bình, thành phố LongXuyên, tỉnh AN GIANG",
   "Longtitude": 10.386202,
   "Latitude": 105.4392412
 },
 {
   "STT": 1726,
   "Name": "Nhà thuốc AGIMEXPHAR M 1",
   "address": "Số 27, Nguyễn Thái Học, phường Mỹ Bình, thành phố Long Xuyên,tỉnh AN GIANG",
   "Longtitude": 10.3877808,
   "Latitude": 105.4364323
 },
 {
   "STT": 1727,
   "Name": "Quầy thuốc Túy Lan",
   "address": "Tổ 02, ấp Hòa Tân, xã Hòa Bình Thạnh, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.407341,
   "Latitude": 105.3423909
 },
 {
   "STT": 1728,
   "Name": "Quầy thuốc Cẩm Yên",
   "address": "số 60/4, Trần Phú, thị Trấn Phú Hòa, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3613768,
   "Latitude": 105.3785304
 },
 {
   "STT": 1729,
   "Name": "Quầy thuốc Mỹ Tiên",
   "address": "tổ 02, ấp Tân Trung, xã Tà Đảnh, huyện Tri Tôn, tỉnh AN GIANG",
   "Longtitude": 10.5627357,
   "Latitude": 105.3453184
 },
 {
   "STT": 1730,
   "Name": "Quầy thuốc Bích Trang",
   "address": "tổ 04, ấp Trung Phú 1, xã Vĩnh Phú, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3527541,
   "Latitude": 105.2311827
 },
 {
   "STT": 1731,
   "Name": "Quầy thuốc Thuận Phát",
   "address": "tổ 8, ấp Mỹ Á, xã Núi Voi, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.5318577,
   "Latitude": 105.0265195
 },
 {
   "STT": 1732,
   "Name": "Quầy thuốc Huỳnh Hiền 2",
   "address": "Tổ 30, ấp Phú Hòa 2, xã Bình Hòa, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 1733,
   "Name": "Quầy thuốc Tuyết Châu",
   "address": "ấp Phước Lợi, xã Ô Lâm, huyện Tri Tôn, tỉnh AN GIANG",
   "Longtitude": 10.3432053,
   "Latitude": 104.9739333
 },
 {
   "STT": 1734,
   "Name": "Quầy thuốc Doanh Thuận",
   "address": "tổ 18, ấp Tà On, xã Châu Lăng, huyện Tri Tôn, tỉnh AN GIANG",
   "Longtitude": 10.4430515,
   "Latitude": 104.9695519
 },
 {
   "STT": 1735,
   "Name": "Quầy thuốc Sang Minh",
   "address": "tổ 27, ấp Chánh Hưng, xã Bình Long, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5671715,
   "Latitude": 105.249289
 },
 {
   "STT": 1736,
   "Name": "Nhà thuốc Lê Thái",
   "address": "số 11, Huỳnh Văn Hây, phường Mỹ Long, thành phố Long Xuyên, tỉnhAN GIANG",
   "Longtitude": 10.3838654,
   "Latitude": 105.4402846
 },
 {
   "STT": 1737,
   "Name": "Nhà thuốc Hoàng Đạt",
   "address": "số 29/9, Trần Hưng Đạo, khóm An Hưng, phường Mỹ Thới, thành phố Long Xuyên, tỉnhAN GIANG",
   "Longtitude": 10.3586949,
   "Latitude": 105.4552799
 },
 {
   "STT": 1738,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "316/01, khóm Xuân Bình, thị trấn Tịnh Biên, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 1739,
   "Name": "Nhà thuốc Lan Phương",
   "address": "số 1018-1020, Tôn Đức Thắng, phường Vĩnh Mỹ, thành phố Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.690769,
   "Latitude": 105.1448433
 },
 {
   "STT": 1740,
   "Name": "Quầy thuốc Ngọc Huyền",
   "address": "ấp Tắc Trúc,xã Nhơn Hội, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.9117665,
   "Latitude": 105.0504589
 },
 {
   "STT": 1741,
   "Name": "Nhà thuốc Minh Phát",
   "address": "số 50A, Thoại Ngọc Hầu, khóm Long Thị D, phường long Thạnh, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3804486,
   "Latitude": 105.4441981
 },
 {
   "STT": 1742,
   "Name": "Quầy thuốc Phú Cường",
   "address": "tổ 04, ấp 4, xã Vĩnh Xương, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 1743,
   "Name": "Quầy thuốc Ngọc Thặng",
   "address": "tổ 03,ấp Phú An A, xã Phú Vĩnh, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 1744,
   "Name": "Quầy thuốc Quốc Anh",
   "address": "tổ 19, ấp Vĩnh Lợi 2, xã Châu Phong, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.7340814,
   "Latitude": 105.1434387
 },
 {
   "STT": 1745,
   "Name": "Công ty CP XNK y tế Domesco-Chi nhánh AN GIANG",
   "address": "số 791A, Hà Hoàng Hổ, phường Đông Xuyên, thành phố Long Xuyên, AN GIANG",
   "Longtitude": 10.3777739,
   "Latitude": 105.4310741
 },
 {
   "STT": 1746,
   "Name": "Chi nhánh Công ty CPDP TV.PHARM tạiAN GIANG",
   "address": "167/6A, Nguyễn Văn Linh, khóm Đông Thịnh 8, phường Mỹ Phước, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.375602,
   "Latitude": 105.44761
 },
 {
   "STT": 1747,
   "Name": "Nhà thuốc Nhi Đồng Sài Gòn",
   "address": "số 1456, Trần Hưng Đạo, P, Mỹ Xuyên, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3760127,
   "Latitude": 105.4413064
 },
 {
   "STT": 1748,
   "Name": "Nhà thuốc Minh Trí",
   "address": "Tổ 10, Trưng Nữ Vương , khóm ChâuLong 4, P. Châu Phú B, thành phố  Châu Đốc, AN GIANG",
   "Longtitude": 10.7078557,
   "Latitude": 105.1193815
 },
 {
   "STT": 1750,
   "Name": "Quầy thuốc Thanh Hằng 1",
   "address": "tổ 18, ấp Trung Bình, xã Thoại Giang, huyện Thoại Sơn, AN GIANG",
   "Longtitude": 10.2655716,
   "Latitude": 105.2311827
 },
 {
   "STT": 1751,
   "Name": "Quầy thuốc Khả Di",
   "address": "tổ 22, ấp Mỹ Khánh 2, xã Mỹ Hòa Hưng, thành phố  Long Xuyên, AN GIANG",
   "Longtitude": 10.3864043,
   "Latitude": 105.3804526
 },
 {
   "STT": 1752,
   "Name": "Quầy thuốc Hiếu Ngọc",
   "address": "tổ 10, ấp An Thuận, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3846614,
   "Latitude": 105.4685588
 },
 {
   "STT": 1753,
   "Name": "Quầy thuốc Duy Nhất",
   "address": "ấp Đồng Ky, xã Quốc Thái, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.8933333,
   "Latitude": 105.0805556
 },
 {
   "STT": 1754,
   "Name": "Quầy thuốc Tân Bình",
   "address": "ấp Bình Phú 2, xã Phú Bình, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.623492,
   "Latitude": 105.2253342
 },
 {
   "STT": 1755,
   "Name": "Quầy thuốc Nguyễn Trúc",
   "address": "ấp Phú Thạnh, xã Phú Hữu, huyện An Phú, AN GIANG",
   "Longtitude": 10.8835468,
   "Latitude": 105.1142011
 },
 {
   "STT": 1756,
   "Name": "Quầy thuốc Hữu Tâm 2",
   "address": "tổ 14, ấp Bình Phú, xã Bình Phước Xuân, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 1757,
   "Name": "Quầy thuốc Lê Sang",
   "address": "số 81, tổ 3, ấp An Thạnh, xã Hòa Bình, huyện Chợ Mới, AN GIANG",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 1758,
   "Name": "Quầy thuốc Tiền Hồ 15",
   "address": "số 56, tổ 3, ấp Kiến Hưng 1, xã Kiến Thành, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1759,
   "Name": "Quầy thuốc Thanh Thoa",
   "address": "số 521, tổ 21, ấp Long Quới 1, xã Long Điền B, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.4957171,
   "Latitude": 105.4511467
 },
 {
   "STT": 1760,
   "Name": "Quầy thuốc Phước Huỳnh",
   "address": "ấp Phú Thạnh, xã Phú Hữu, huyện An Phú, AN GIANG",
   "Longtitude": 10.8835468,
   "Latitude": 105.1142011
 },
 {
   "STT": 1761,
   "Name": "Quầy thuốc Huỳnh Ngân",
   "address": "số 19, tổ 04, ấp Phú Hạ, xã Phú Xuân, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 1762,
   "Name": "Quầy thuốc Siêu",
   "address": "số 43,tổ 02, ấp Phú An A, xã Phú Vĩnh, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 1763,
   "Name": "Quầy thuốc Khánh Huyền",
   "address": "tổ 7, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1764,
   "Name": "Quầy thuốc Kim Ngọc",
   "address": "số 906/6, ấp Kinh Đào, xã Phú Thuận, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.304159,
   "Latitude": 105.4057097
 },
 {
   "STT": 1765,
   "Name": "Quầy thuốc Đông Hào",
   "address": "tổ 7, ấp Bình Hòa, thị trấn Cái Dầu, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5702578,
   "Latitude": 105.2387827
 },
 {
   "STT": 1766,
   "Name": "Quầy thuốc Lê Hoàng Tuấn",
   "address": "ấp Bình Chánh, xã Bình Long, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5441838,
   "Latitude": 105.2237892
 },
 {
   "STT": 1767,
   "Name": "Quầy thuốc Bé Năm",
   "address": "tổ 17, ấp Hòa Thạnh, xã Hòa Bình Thạnh, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.407341,
   "Latitude": 105.3423909
 },
 {
   "STT": 1768,
   "Name": "Nhà thuốc Tuyết Minh",
   "address": "số 11/03, Thoại Ngọc Hầu, phường Mỹ Phước, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3801849,
   "Latitude": 105.4446693
 },
 {
   "STT": 1770,
   "Name": "Quầy thuốc Trung Hưng",
   "address": "số 401, ấp Hòa Hưng 1, xã Hòa Lạc, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.667617,
   "Latitude": 105.219466
 },
 {
   "STT": 1771,
   "Name": "Cơ sở Thu Sương",
   "address": "tổ 05, khóm Sơn Đông, thị trấn Nhà Bàng, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.6476108,
   "Latitude": 105.0367466
 },
 {
   "STT": 1772,
   "Name": "Quầy thuốc Phong Phú",
   "address": "tổ 32, Huỳnh Thị Hưởng, ấp Thị 2, thịtrấn Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.4326748,
   "Latitude": 105.549742
 },
 {
   "STT": 1773,
   "Name": "Quầy thuốc Minh Châu 1",
   "address": "số 47, Nguyễn Văn Trỗi, ấp Đông Sơn 2, thị trấn Núi Sập, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.2681403,
   "Latitude": 105.27101
 },
 {
   "STT": 1774,
   "Name": "Quầy thuốc Tấn Bửu",
   "address": "tổ 7, ấp Phú Hạ 1, xã Kiến Thành, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 1775,
   "Name": "Quầy thuốc Phú Hằng",
   "address": "số 07, tổ 01, ấp Nhơn Lợi, xã Nhơn Mỹ, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.4589745,
   "Latitude": 105.41181
 },
 {
   "STT": 1776,
   "Name": "Quầy thuốc Hòa Bình",
   "address": "tổ 05, ấp Vĩnh Thành, xã Vĩnh Khánh, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.2599612,
   "Latitude": 105.348246
 },
 {
   "STT": 1777,
   "Name": "Quầy thuốc Minh Phúc",
   "address": "Chợ Định Thành, ấp Hòa Thành, xã Định Thành, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3058934,
   "Latitude": 105.3096286
 },
 {
   "STT": 1778,
   "Name": "Nhà thuốc Cẩm Tú",
   "address": "số 69A, Nguyễn Văn Linh, phường Mỹ Phước, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3633334,
   "Latitude": 105.427275
 },
 {
   "STT": 1779,
   "Name": "Nhà thuốc Huỳnh Trang",
   "address": "số 107, Phạm Cự Lượng, phường MỹPhước, thành phố Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3660122,
   "Latitude": 105.4471464
 },
 {
   "STT": 1780,
   "Name": "Quầy thuốc Song Thư",
   "address": "ấp 2, xã Vĩnh Xương, thị xã Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.909989,
   "Latitude": 105.1698875
 },
 {
   "STT": 1781,
   "Name": "Quầy thuốc Ba Dũng",
   "address": "số 41, ấp Nhơn An, xã Nhơn Mỹ, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.4756645,
   "Latitude": 105.3950939
 },
 {
   "STT": 1782,
   "Name": "Quầy thuốc Mỹ Tú",
   "address": "số 64, tổ 02, ấp Ba Xưa, xã Thạnh Mỹ Tây, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 1783,
   "Name": "Quầy thuốc Trinh Nữ",
   "address": "số 102B, đường Nam Vịnh Tre, ấp Vĩnh Thuận, xã Vĩnh Thạnh Trung, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.618398,
   "Latitude": 105.2096081
 },
 {
   "STT": 1784,
   "Name": "Quầy thuốc Bá Duy",
   "address": "tổ 12, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1786,
   "Name": "Nhà thuốc Thăng Long",
   "address": "số 1264, Trần Hưng Đạo, phường Mỹ Xuyên, TP Long Xuyên,AN GIANG",
   "Longtitude": 10.3809264,
   "Latitude": 105.4394148
 },
 {
   "STT": 1787,
   "Name": "Nhà thuốc Huỳnh Cẩm",
   "address": "số 04, đường số 7,phường Châu Phú B, TP Châu Đốc, tỉnh AN GIANG",
   "Longtitude": 10.696137,
   "Latitude": 105.124524
 },
 {
   "STT": 1788,
   "Name": "Quầy thuốc Kim Tuyến",
   "address": "số 21, tổ 1, ấp Voi 1, xã Núi Voi, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.5421786,
   "Latitude": 105.0372692
 },
 {
   "STT": 1789,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "tổ 15, ấp Tân Thuận, xã Tân Lợi, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.4935928,
   "Latitude": 105.0393777
 },
 {
   "STT": 1790,
   "Name": "Quầy thuốc Hạnh Long",
   "address": "tổ 2, ấp Vĩnh Tâm, xã Vĩnh Trung, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.550372,
   "Latitude": 105.021915
 },
 {
   "STT": 1791,
   "Name": "Quầy thuốc Hồng Mị",
   "address": "số 943, ấp Phú Quới, xã Phú An, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.6596897,
   "Latitude": 105.3189726
 },
 {
   "STT": 1792,
   "Name": "Quầy thuốc Hòa Đông",
   "address": "Kiot chợ Bắc Cái Đầm, ấp Hậu Giang 1, xã Tân Hòa, huyện Phú Tân, tỉnh AN GIANG",
   "Longtitude": 10.5591873,
   "Latitude": 105.3117465
 },
 {
   "STT": 1793,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "Ấp Long Phú 2, xã Long Điền B, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5104664,
   "Latitude": 105.4448701
 },
 {
   "STT": 1794,
   "Name": "Quầy thuốc Số 144",
   "address": "số 144, ấp Mỹ Quí, thị trấn Mỹ Luông, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.36208,
   "Latitude": 105.4524494
 },
 {
   "STT": 1795,
   "Name": "Quầy thuốc Thu Thảo",
   "address": "số 431, tổ 3, ấp Mỹ Hòa, thị trấn Mỹ Luông, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 1796,
   "Name": "Quầy thuốc Kim Hoa",
   "address": "tổ 11, ấp Hòa Hạ, xã Kiến An, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.5563083,
   "Latitude": 105.3950728
 },
 {
   "STT": 1797,
   "Name": "Quầy thuốc Cẩm Tiên 1",
   "address": "số 228, tổ 8, ấp Hòa Thành, xã Định Thành, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3057775,
   "Latitude": 105.3014109
 },
 {
   "STT": 1798,
   "Name": "Nhà thuốc Xuân",
   "address": "số 1357, đường Trần Hưng Đạo, P. Mỹ Long, thành phố  Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3765234,
   "Latitude": 105.4405375
 },
 {
   "STT": 1799,
   "Name": "Nhà thuốcTrung Tâm Y tế An Phú",
   "address": "ấp An Hưng, thị trấn AnPhú, huyện An Phú, tỉnh AN GIANG",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 1800,
   "Name": "Quầy thuốc Phụng Hiển",
   "address": "tổ 17, ấp Tây Bình, xã Vĩnh Trạch, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.25736,
   "Latitude": 105.268204
 },
 {
   "STT": 1801,
   "Name": "Quầy thuốc Thanh Nhã",
   "address": "tổ 01, ấp Tân Phú, xã Tân Thạnh, TX Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.7997101,
   "Latitude": 105.2405477
 },
 {
   "STT": 1802,
   "Name": "Quầy thuốc Xuân Thẳm",
   "address": "tổ 10, ấp Hòa Lợi 1, xã Vĩnh Lợi, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.3747313,
   "Latitude": 105.3132033
 },
 {
   "STT": 1803,
   "Name": "Quầy thuốc Kim Hiên",
   "address": "số 487, ấp Mỹ Chánh, xã Mỹ Đức, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 1804,
   "Name": "Quầy thuốc Bùi Thị Nhung",
   "address": "tổ 10, ấp Hòa Thạnh,  xã Hòa Bình Thạnh, huyện Châu Thành, tỉnh AN GIANG",
   "Longtitude": 10.4125083,
   "Latitude": 105.348246
 },
 {
   "STT": 1805,
   "Name": "Quầy thuốc Tường Hưng",
   "address": "tổ 15, ấp 01, xã Vĩnh Xương, TX.Tân Châu, tỉnh AN GIANG",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 1806,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "KDC Long Bình, ấp Long Bình, xã Long Kiến, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.4664526,
   "Latitude": 105.471249
 },
 {
   "STT": 1807,
   "Name": "Quầy thuốc Thanh Tú",
   "address": "389, Mỹ Tân, xã Mỹ Hội Đông, huyện Chợ Mới, tỉnh AN GIANG",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 1808,
   "Name": "Quầy thuốc Thanh Huy",
   "address": "số 3, Lê Thánh Tôn, ấp An Hưng, TT An Phú, tỉnh AN GIANG",
   "Longtitude": 10.8146533,
   "Latitude": 105.0919518
 },
 {
   "STT": 1809,
   "Name": "Quầy thuốc Phương Oanh",
   "address": "tổ 4, ấp Bình Phú, xã Bình Thủy, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5440566,
   "Latitude": 105.2253316
 },
 {
   "STT": 1810,
   "Name": "Quầy thuốc Phương Oanh 2",
   "address": "tổ 15, ấp Bình Hòa, xã Bình Thủy, huyện Châu Phú, tỉnh AN GIANG",
   "Longtitude": 10.5164544,
   "Latitude": 105.3214531
 },
 {
   "STT": 1811,
   "Name": "Nhà thuốc Phú Hiệp",
   "address": "số 543/5, đường Hà Hoàng Hổ, P. Mỹ Hòa, thành phố  Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3762334,
   "Latitude": 105.4190915
 },
 {
   "STT": 1812,
   "Name": "Nhà thuốc Trung Sơn AN GIANG",
   "address": "số 349/30, khóm Mỹ Quới, P. Mỹ Quý, thành phố  Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3720708,
   "Latitude": 105.4507424
 },
 {
   "STT": 1813,
   "Name": "Nhà thuốc Khoa Trí",
   "address": "số 257/2, Trần Nhật Duật, P. Mỹ Long, thành phố  Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3780848,
   "Latitude": 105.4411341
 },
 {
   "STT": 1814,
   "Name": "Quầy thuốc Trà My",
   "address": "đường số 4, ấp Phú Hữu, TT Phú Hòa, huyện Thoại Sơn, tỉnh AN GIANG",
   "Longtitude": 10.3573947,
   "Latitude": 105.3755025
 },
 {
   "STT": 1815,
   "Name": "Quầy  thuốc Khả Doanh",
   "address": "ấp Ninh Thạnh, xã An Tức, huyện Tri Tôn, tỉnh AN GIANG",
   "Longtitude": 10.3912467,
   "Latitude": 104.9547703
 },
 {
   "STT": 1816,
   "Name": "Quầy  thuốc Anh Thơ",
   "address": "số 212/2, khóm Xuân Hiệp, TT. Tịnh Biên, huyện Tịnh Biên, tỉnh AN GIANG",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 1817,
   "Name": "Nhà thuốc Vân Thư",
   "address": "số 1/23, Trần Hưng Đạo, P. Mỹ Thới, thành phố  Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.350578,
   "Latitude": 105.4616417
 },
 {
   "STT": 1818,
   "Name": "Nhà thuốc Hạnh Phúc",
   "address": "số 235, Ung Văn Khiêm, P. Mỹ Phước, thành phố  Long Xuyên, tỉnh AN GIANG",
   "Longtitude": 10.3671924,
   "Latitude": 105.4396794
 }
];